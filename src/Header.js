import React from 'react';

const _=({children})=><div className="jumbotron shadow-lg">
    {children}
</div>

export default _;