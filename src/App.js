import React from 'react';
import {HashRouter,Switch,Route} from 'react-router-dom';
import Home from './Home';
import Carl from './Carl/Carl';
import CarlBlogs from './Carl/Blog/Blogs';
import CarlBlog from './Carl/Blog/Blog';
import CarlInformation from './Carl/Information';

const _=()=> {

  return <HashRouter>
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route exact path="/carl/" component={Carl}/>
      <Route exact path="/carl/information" component={CarlInformation}/>
      <Route exact path="/carl/blogs/" component={CarlBlogs}/>
      <Route exact path="/carl/blogs/:blogfilename" component={CarlBlog}/>
    </Switch>
  </HashRouter>;
};
export default _;