import React from 'react';
import Helmet from 'react-helmet';
import CarlLogo from './Carl/CarlLogo';
import Header from './Header';

const _= () => <div className="App">
  <header className="App-header">
    <Helmet>
      <title>Home on BurksBrand</title>
      <meta name="description" content="Shared Launch Page for Burks Brand, Including Carl Burks" />
    </Helmet>
    <div className="container">
      <Header>
        <h1>BurksBrand</h1>
      </Header>
      <div className="container">
        <div className="row">
        <div className="col">

<div className="card" style={{ width: "20em" }}>
  <div className="card-body">
    <h3 className="card-title">
  <CarlLogo/><a href="#/carl" className="btn btn-primary">Carl Burks</a></h3>
    <p className="card-text">Software Engineer, Pen and Paper Game Arbiter, Electronics Tinkerer
    </p>
    </div>
</div>
</div>

        </div>
      </div>
      <br/>
      More Content Creators coming soon.

    </div>
  </header>
</div>
export default _;