import React from 'react';
const _ = (props)=>{
    return React.cloneElement(props.lookup[props.value]!==undefined ? props.lookup[props.value]: props.default,props);
}
export default _;
