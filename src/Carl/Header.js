import React from 'react';
import Header from '../Header';
import CarlLogo from './CarlLogo';

const _ =({ subTitle="Software Engineer, Pen and Paper Game Arbiter and Electronics Tinkerer",children }) => <Header>
    <CarlLogo/>
    <img style={{maxWidth: "8em", float:"right"}} src="./static/images/logo-filled.png" alt="Carl Burks"></img>
    <h1 className="carl-logo">Carl Burks </h1>
    <h3> {subTitle}</h3>
    {children}
</Header>

export default _;