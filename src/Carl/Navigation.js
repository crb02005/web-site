import React from 'react';

const _=({children})=><nav aria-label="breadcrumb">
    <ol className="breadcrumb">
        {children}
    </ol>
</nav>
export default _;
