import React from 'react';
import Helmet from 'react-helmet';
import CarlHeader from './Header';
import Navigation from './Navigation';
const _ =()=><>
<Helmet>
    <title>Carl Burks - Home</title>
    <meta name="description" content="Carl Burks blog and general information" />
</Helmet>
<div className="container">
    <CarlHeader>
    </CarlHeader>
    <Navigation>
        <li className="breadcrumb-item" aria-current="page"><a href="/#">BurksBrand</a></li>
        <li className="breadcrumb-item active" aria-current="page"><a href="/#">Carl Burks</a></li>
    </Navigation>
    <ul className="list-group">
        <li className="list-group-item link"><a href="/#/carl/information/">General Information</a></li>
        <li className="list-group-item link"><a href="/#/carl/blogs/">Blog</a></li>
    </ul>
</div>
</>
export default _;