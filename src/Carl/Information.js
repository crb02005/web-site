import React,{useState} from 'react';
import Helmet from 'react-helmet';
import Navigation from './Navigation';
import Header from './Header';

var ComputersInfo = ()=><div className="row">
<div className="col">

<h2>Computers</h2>
    <p>I fell in love with computers when my older brother got a Commodore 64. Everything after that is history. </p>

    <h3>C#</h3>
    <p>My latest efforts with C# include building RESTful apis using OpenApi, Swashbuckle, CQRS, and dynamic controllers.</p>
    <h3>JavaScript</h3>
    <p>I have been working with the latest public version of React, and have experience leading teams, and directly coding in JavaScript.</p>
    <h3>TypeScript</h3>
    <p>I have used TypeScript on several projects.</p>
    <h3>SQL</h3>
    <p>In college, I experienced the joys of Oracle databases. In the corporate world, I've used both MySQL and MsSQL. I have spoken about Sqlite at local tech conference.</p>
    <p>I have served on a SQL performance commitee with the goal of optimizing queries.</p>
    <h3>Python</h3>
    <p>For personal projects I have built several applications in Python. One of my projects utilized Docker, RabbitMQ, and several apis to categorize, and archive news articles to detect drift over time.</p>
    <p>Recently I've been working with Pandas for charting, built an API framework, and have been experimenting with machine learning for sentiment analysis.</p>
    <h4>Java/Android</h4>
    <p>I built my own dice roller app for Android.</p>
    <h4>Shells</h4>
    <ul>
        <li>Powershell</li>
        <li>DOS\cmd\command</li>
        <li>Bash</li>
    </ul>
    <h4>Linux</h4>
    <p>I've been a bit of a distro hopper. Arch is my current favorite for personal distro, but I like Alpine for containerization</p>
    <ul>
        <li>Redhat</li>
        <li>Fedora</li>
        <li>Debian</li>
        <li>Ubuntu</li>
        <li>Mint</li>
        <li>Arch</li>
        <li>Alpine</li>
        <li>Others</li>
    </ul>
    
</div>
</div>
var GameInfo=()=><div className="row"><div className="col">

    <h2>Pen &amp; Paper RPGS</h2>
    
    One time at church camp, I believe I played: <a href="https://en.wikipedia.org/wiki/DragonRaid"> Dragon Raid</a>, if it wasn't this one it was one like it. My parents didn't really like fantasy, "what if" questions, or magic so that limited my options.
    In college, I started with 3.0 D&amp;D. I still play with one of my friends from those days.

    <h2>Computer</h2>

    <p>I don't spend a lot of time playing computer games anymore. When I do I usually play Minecraft, Skyrim, or occasionally Ark.</p>
    
    <h2>Casual</h2>
    
    <p>If I'm stuck somewhere and I don't have an interesting article to read, I occasionally play a <a href="https://play.google.com/store/apps/details?id=com.jamcity.snoopypop">Snoopy Bubble Shooter</a> game. I used to play Words With Friends, but that kind of fell out of favor. </p>
    
    <h2>Board Games</h2>
    
    Machi Koro, King Domino, and King of Tokyo.
    </div></div>
var ElectronicsInfo=()=><div className="row"><div className="col">
<h2>
    Arduinos
</h2>
    <ul>
        <li>Weather station</li>
        <li>LED Menorah</li>
        <li>MIDI sender</li>
        <li>TV e-reader. (read from an SD card, and played messages through analog TV) </li>
    </ul>
<h2>
    Raspberry Pi
</h2>
    <ul>
        <li>Automated Lazy Susan, Camera. Spun the wheel around, took pictures and turned them into a gif</li>
        <li>Small two wheel &amp; one ball caster Robot</li>
        <li>See through desktop wallpaper. (pi camera behind the display, and a script to update the wallpaper)</li>
        <li>File server</li>
        <li>Squid cache</li>
        <li>PiHole</li>
    </ul>
<h2>
    Others
</h2>
<ul>
    <li>Amplifier</li>
    <li>PONG - from a set</li>
    <li>XBox One Controller Repair</li>
</ul>
</div></div>

var SideProjectsInfo=()=><><h4>Current Side Projects:</h4>
<ul>
    <li>Mazes</li>
    <li>Old School Revival of a BBS style game involving spaceships</li>
    <li>Running a modified Castles &amp; Crusades campaign with a few friends.</li>
</ul></>

var ContainerLi = ({select, topic, title, selectedTopic})=>{

    return <li className={(`list-group-item ${topic===selectedTopic ? "active":""}`)} onClick={()=>select(topic)} >
        {title}
    </li>
}

const _ = ()=>{ 
    const [topic, setTopic] = useState("computers");
    
    return <>
    <Helmet>
        <title>Carl Burks - Home</title>
        <meta name="description" content="Carl Burks blog and general information" />
    </Helmet>
    <div className="container">
        <Header/>
        <Navigation>
            <li className="breadcrumb-item" aria-current="page"><a href="/#">BurksBrand</a></li>
            <li className="breadcrumb-item" aria-current="page"><a href="/#/carl">Carl Burks</a></li>
            <li className="breadcrumb-item active" aria-current="page">Information</li>
        </Navigation>
        <div className="row">
            <div className="col">
                <ul className="list-group">
                    <ContainerLi title="Computers" topic="computers" select={(t)=>{setTopic(t)}} selectedTopic={topic}/>
                    <ContainerLi title="Electronics" topic="electronics" select={(t)=>{setTopic(t)}} selectedTopic={topic}/>
                    <ContainerLi title="Gaming" topic="gaming" select={(t)=>{setTopic(t)}} selectedTopic={topic}/>
                    <ContainerLi title="Side Projects" topic="sideproject" select={(t)=>{setTopic(t)}} selectedTopic={topic}/>
                </ul>
            </div>
            <div className="col-12">
            
            <div className=" shadow p-3">
    
            {topic === "computers" ?<ComputersInfo/>:<></>}
            {topic === "gaming" ?<GameInfo/>:<></>}
            {topic === "electronics" ?<ElectronicsInfo/>:<></>}
            {topic === "sideproject" ?<SideProjectsInfo/>:<></>}
            </div>
            </div>
        </div>
    </div>
    </>}

export default _;