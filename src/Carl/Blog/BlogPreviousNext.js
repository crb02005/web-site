import React from 'react';

const getIndex=(blogs,filename)=>{
    let index =0;
    for(index=0;index<blogs.length;index+=1){
        if(blogs[index].filename===filename){
            return index;
        }
    }
}
const _ =({blogs,filename})=>{

let index = getIndex(blogs,filename+".markdown"),
nextBlog        = index > 0 ? blogs[index-1]:null,
previousBlog    = index < blogs.length-1 ?blogs[index+1]:null;

return<div className="row">
    

    
    {nextBlog?<div className="col">
        <a  className="btn btn-outline-primary"
            href={"#/carl/blogs/"+nextBlog.filename.replace(".markdown","")} 
            key={nextBlog.filename}>
                Read Newer Posts
        </a></div>:null} 

        {previousBlog?<div className="col">
        <a className="btn btn-outline-primary"
            href={"#/carl/blogs/"+previousBlog.filename.replace(".markdown","")} 
            key={previousBlog.filename}>
                Read Older Posts
        </a>
    </div>:null} 
    
</div>
}
export default _;