import React from 'react';
import Switch from '../../Switch';
import {
    SiWindows95
    , SiUbuntu
    , SiPowershell
    , SiVisualstudiocode
    , SiVim
    , SiPython
    , SiJavascript
    , SiGnu
    , SiMarkdown
    , SiArduino
} from 'react-icons/si';
import { IoLogoTux } from 'react-icons/io';
// import _ from '../Navigation';
const linuxCopyPasta=`I'd just like to interject for a moment.  What you're referring to as Linux,
is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux.
Linux is not an operating system unto itself, but rather another free component
of a fully functioning GNU system made useful by the GNU corelibs, shell
utilities and vital system components comprising a full OS as defined by POSIX.

Many computer users run a modified version of the GNU system every day,
without realizing it.  Through a peculiar turn of events, the version of GNU
which is widely used today is often called "Linux", and many of its users are
not aware that it is basically the GNU system, developed by the GNU Project.

There really is a Linux, and these people are using it, but it is just a
part of the system they use.  Linux is the kernel: the program in the system
that allocates the machine's resources to the other programs that you run.
The kernel is an essential part of an operating system, but useless by itself;
it can only function in the context of a complete operating system.  Linux is
normally used in combination with the GNU operating system: the whole system
is basically GNU with Linux added, or GNU/Linux.  All the so-called "Linux"
distributions are really distributions of GNU/Linux.`;
const UNMAPPED = "unmapped";
const GAMING = "gaming";
const PROGRAMMING = "programming";
const ELECTRONICS = "electronics";

const lookup = { "meta": "important" };
[
    'bash', 'basic',
    'c', 'caj',
    'csharp', 'ddd',
    'docker', 'html',
    'javascript', 'linux',
    'mermaid',
    'netcat', 'npm',
    'python', 'react', 'sql',
    'tech-talk', 'ubuntu',
    'uml', 'vim',
    'vscode', "typescript", "sqlite", "sqlalchemy"
].forEach(x => lookup[x] = PROGRAMMING);

["minecraft"].forEach(x => lookup[x] = GAMING);
["arduino","pi","microcontroller"].forEach(x => lookup[x] = ELECTRONICS)

const lookupClass = {
    "important": "badge-danger",
    "unmapped": "badge-info",
    "programming": "badge-primary",
    "gaming": "badge-success",
    "electronics": "badge-warning"
}
const getClass = (name) => {
    if (lookupClass[name] !== undefined) {
        return lookupClass[name];
    }
    if (lookup[name] !== undefined) {
        return getClass(lookup[name]);
    }
    return getClass(UNMAPPED);
}
const GamingTag = ({ children }) => <span className="badge badge-success ml-1">{children}</span>
const ProgrammingTag = ({ children }) => <span className="badge badge-primary ml-1">{children}</span>
const ElectronicsTag = ({children})=><span className="badge badge-warning ml-1">{children}</span>
const ArduinoTag = ()=><ElectronicsTag><SiArduino/> arduino</ElectronicsTag>
const MarkdownTag = ()=><ProgrammingTag><SiMarkdown/> markdown</ProgrammingTag>
const SuperTuxTag = () => <GamingTag><IoLogoTux />supertux2</GamingTag>
const UbuntuTag = () => <ProgrammingTag><SiUbuntu /> ubuntu</ProgrammingTag>
const WindowsTag = () => <ProgrammingTag><SiWindows95 /> windows</ProgrammingTag>
const VimTag = () => <ProgrammingTag><SiVim /> vim</ProgrammingTag>
const PowerShellTag = () => <ProgrammingTag><SiPowershell /> powershell</ProgrammingTag>
const VisualStudioCodeTag = () => <ProgrammingTag><SiVisualstudiocode /> visual studio code</ProgrammingTag>
const PythonTag = () => <ProgrammingTag><SiPython /> python</ProgrammingTag>
const JavaScriptTag =()=><ProgrammingTag><SiJavascript/> javaScript</ProgrammingTag>
const CentralArkansasJavascriptTag =()=><ProgrammingTag><SiJavascript/> central arkansas javascript</ProgrammingTag>
const LinuxTag =()=><ProgrammingTag><span data-toggle="tooltip" title={linuxCopyPasta}><SiGnu/> linux</span> </ProgrammingTag>
const Tag = ({ name }) => <span className={`badge ${getClass(name.toLowerCase())} ml-1`}>{name}</span>

const _=({ name }) => <Switch lookup={{
    "windows": <WindowsTag />,
    "ubuntu": <UbuntuTag />,
    "supertux2": <SuperTuxTag />,
    "powershell": <PowerShellTag />,
    "vscode": <VisualStudioCodeTag />,
    "vim": <VimTag />,
    "python": <PythonTag />,
    "javascript": <JavaScriptTag/>,
    "linux": <LinuxTag/>,
    "markdown":<MarkdownTag/>,
    "caj": <CentralArkansasJavascriptTag/>,
    "arduino": <ArduinoTag/>
}} default={<Tag />} value={name} name={name.toLowerCase()} />

export default _;
