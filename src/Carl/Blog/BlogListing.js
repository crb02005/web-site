import React from 'react';
import Tag from './Tag';
const _ = ({title,date,filename,selected,categories})=>{
    var className="";
    if(selected){
        className+="bg-primary text-light";
    }
    if(date.toLocaleDateString().indexOf("Invalid")>-1){
        className="bg-danger  text-light";
    }
return <tr key={filename}>
    <td>
    {date.toLocaleDateString()} 
    </td>
    <td>
    <a className={ className} href={"/#/carl/blogs/"+filename.replace(".markdown","")}> 
    {title}
    </a>
    </td>
    <td>
    {categories.map(x=><Tag key={filename+"_"+x} name={x.trim()}/>)}
    </td>
</tr>
}
export default _;