import React from 'react';
import BlogListing from './BlogListing';
const _=({blogs,selectedBlog})=>{

return    <>
{blogs.map(x=><BlogListing categories={x.categories ? x.categories.split(","):[]} key={x.filename} date={new Date(x.date)} title={x.title} filename={x.filename} selected={x.filename===selectedBlog} />)}
</>

}
export default _;