import React, {useState,  Fragment} from 'react';
import DataSource from '../../DataSource';
import Helmet from 'react-helmet';
import yaml from 'js-yaml';
import CarlHeader from '../Header';
import ReactMarkdown from 'react-markdown';
import Navigation from '../Navigation';
import BlogListings from './BlogListings';
import BlogPreviousNext from './BlogPreviousNext';

const Blog=({filename,href})=>{
const [shouldGetBlog,setShouldGetBlog]=useState(true);
const [blog,setBlog]=useState("");
const [shouldGetIndex, setShouldGetIndex] = useState(true);
const [blogs, setBlogs] =useState([]);
const siteUrl = "./";
return <Fragment key={href}>
<DataSource 
        url={"carl/blog/"+filename+".markdown"}
        enabled={shouldGetBlog} 
        onResponse={(resp)=>{
            resp.text().then(text=>
                {
                    const pieces = text.split("---");
                    let blog = {};
                    try{
                        blog = yaml.safeLoad(pieces[1]);
                        blog.text=pieces[2].replaceAll("{{site.url}}",siteUrl).replaceAll("{{site.baseurl}}","");
                    } catch(ex){
                        blog = {
                            "date": new Date(),
                            "title": "invalid blog"
                        };
                        blog.text = "Could not load blog.";
                    }
                    setBlog(blog);
                    setShouldGetBlog(false);        
                })
        }}
    />
<DataSource
    url="./carl-blog-index.json"
    method="GET"
    enabled={shouldGetIndex} 
    required={false}
    onResponse={(resp)=>{
        resp.json().then(blogs=>{
            setShouldGetIndex(false);
            setBlogs(blogs);

        });
    }}
    onEmpty={()=>{
        setShouldGetIndex(false);
    }}
/>
<div className="container">
{shouldGetBlog ?
<CarlHeader />
:
<CarlHeader subTitle={blog.title+" - "+blog.date.toLocaleDateString('en-US')}/>
}
{shouldGetBlog?<>Loading</>:<>
<Helmet>
    <title>{"Carl Burks "+blog.title+" - "+blog.date.toLocaleDateString('en-US')}</title>
    <meta name="description" content={blog.categories} />
</Helmet>
    <Navigation>
            <li className="breadcrumb-item" aria-current="page"><a href="/#">BurksBrand</a></li>
            <li className="breadcrumb-item" aria-current="page"><a href="/#/carl">Carl Burks</a></li>
            <li className="breadcrumb-item" aria-current="page"><a href="/#/carl/blogs">Blogs</a></li>
            <li className="breadcrumb-item active" aria-current="page"> {blog.title}</li>
    </Navigation>

    <div className="row">
        <div className="col-8 blog">
            <BlogPreviousNext blogs={blogs} filename={filename}/>
            <div className="row">
            <div className="col-12">
            <ReactMarkdown children={blog.text}></ReactMarkdown> 
            </div>
            </div>
            <BlogPreviousNext blogs={blogs} filename={filename}/>
        </div>
        <div className="col-4">
            <table className="table table-striped table-sm">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Tags</th>
                    </tr>
                </thead>
                <tbody>
                <BlogListings blogs={blogs} selectedBlog={filename+".markdown"}/>
                </tbody>
            </table>
        </div>
    </div>
</>
}
</div>
</Fragment>
}

const _ = ({match})=><Blog key={window.location.href} match={match} href={window.location.href} filename={match.params.blogfilename}></Blog>
export default _;