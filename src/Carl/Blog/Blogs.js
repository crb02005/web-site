import React, {Fragment, useState} from 'react';
import Helmet from 'react-helmet';
import DataSource from '../../DataSource';
import Header from '../Header';
import Navigation from '../Navigation';
import BlogListings from './BlogListings';

const _=()=>{
const [shouldGetIndex, setShouldGetIndex] = useState(true);
const [blogs, setBlogs] =useState([]);
return<Fragment key={window.location.href}>
<Helmet>
    <title>Carl Burks - Home</title>
    <meta name="description" content="Carl Burks blog and general information" />
</Helmet>
<div className="container">
    <Header/>
    <Navigation>
        <li className="breadcrumb-item" aria-current="page"><a href="/#">BurksBrand</a></li>
        <li className="breadcrumb-item" aria-current="page"><a href="/#/carl">Carl Burks</a></li>
        <li className="breadcrumb-item active" aria-current="page">Blogs</li>
    </Navigation>
    <DataSource
        url="./carl-blog-index.json"
        method="GET"
        enabled={shouldGetIndex} 
        required={false}
        onResponse={(resp)=>{
            resp.json().then(blogs=>{
                setShouldGetIndex(false);
                setBlogs(blogs);
    
            });
            //resp[Object.keys(resp)[0]].data.title
            //date
            //title
            //layout
        }}
        onEmpty={()=>{
            setShouldGetIndex(false);
        }}
    />
    <table className="table table-bordered table-hover table-striped">
        <thead>
            <tr>
                <th>Date</th>
                <th>Title</th>
                <th>Tags</th>
            </tr>
        </thead>
        <tbody>
        <BlogListings blogs={blogs}/>

        </tbody>
    </table>
</div>
</Fragment>}
export default _;