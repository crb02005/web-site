import  { useEffect} from "react";
export const defaultHeaders ={
    "Content-Type": "application/text",
    accept: "application/text"
  }
export const defaultMapper =(res) =>{
    var jsonRes = res.json();
    return jsonRes;
};
const setError = (f)=>{
    // console.log(f);
};
const _ = ({enabled,url,onResponse}) => {

    useEffect(() => {
        if (enabled || enabled === undefined) {
            const abortController = new AbortController();
            var cleanedUp = false;
            const fetchCleanup = () => {
                cleanedUp = true;
            }
                fetch(url, {
                    headers: defaultHeaders,
                    method: "GET",
                    credentials: "same-origin",
                    mode: "cors",
                    referrerPolicy:"no-referrer",
                    signal: abortController.signal
                })
                .then((response) => {
                    try {
                        if (!cleanedUp) {
                            onResponse(response);
                        }

                    } catch (ex) {
                        !cleanedUp && setError(ex);
                    }
                })
                .catch((responseReject) => {
                        !cleanedUp && setError(responseReject);
                })
                .finally(() => {

                });
            return fetchCleanup;
        }
    }, [enabled,url,onResponse]);
    return null;
}
export default _;