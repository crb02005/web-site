$pieces = (cat .\public\internal_links.json | ConvertFrom-Json)
$pieces | % {
    
    $path = ($_.replace("{{site.url}}","./public"))
    
    if($path -like "./public/#*"){
        $path = ($path.Replace("./public/#/","./public/")+".markdown")
    } 
    # Split between # routing and images
    
    $yn = ( Test-Path -Path $path )
    if($yn){

    } else {
        $path
    }
 }
