const fs = require('fs');
const yaml = require('js-yaml');
// directory path
const dir = './public/carl/blog';
tags = {};
links = [];
internal_links=[];
const addLinks=(text)=>{
    // match []() or ![]() this isn't perfect but it should work well enough
    const matches = text.match(/\[[^)]*\)/g);
    if(matches!==null){
        matches.forEach(m=>{
            let link =m.substring(m.indexOf("(")+1,m.length-1).split(" ")[0];
            if(link.indexOf("{{site.url}}")===0){
                internal_links.push(link);
            } 
            if(link.indexOf("http")===0){
                links.push(link);
            }

        }) 
    
    }
};
// list all files in the directory
fs.readdir(dir, (err, files) => {
    if (err) {
        throw err;
    }

    // files object contains all files names
    // log them on console
    var data = []
    var sitemap = [
        "https://www.burksbrand.com/",
        "https://www.burksbrand.com/#/carl/information",
        "https://www.burksbrand.com/#/carl/blogs",
    ]
    
    files.filter(x=>{
        var y = x.split(".");
        return y[y.length-1]==="markdown";
    }).forEach(f=>{
        const p = "./public/carl/blog/"+f,
        rawDoc = fs.readFileSync(p,"utf-8"),
        doc =rawDoc.split("---");
        addLinks(doc[2]);
        sitemap.push(
            "https://www.burksbrand.com/#/carl/blogs/"+ 
            p.replace(".markdown","").split("/").pop());

        if(doc.length>0){
            let yml = yaml.safeLoad(doc[1]);
            if(yml){
                yml.filename = f;
                if(yml.categories !== undefined && yml.categories !== null){
                    yml.categories.split(",").forEach(x=>{
                        
                        if(tags[x]===undefined){
                                tags[x]=[];
                            }
                            tags[x].push(yml.filename.replace(".markdown",""));

                    });
                }
                if(yml!==undefined){
                    data.push(yml);
                }
                
            }else{
                data.push({"filename":f});
            }
        } else {
            data.push({"filename":f});
        }
    })
    fs.writeFile("./public/carl-blog-index.json",JSON.stringify(
        data.sort((a,b)=>new Date(a.date) - new Date(b.date)).reverse()), (err)=>{
        if(err){
            console.log(err);
        }
    })
    fs.writeFile("./public/sitemap.txt",sitemap.join("\r\n"), (err)=>{
        if(err){
            console.log(err);
        }
    })
    fs.writeFile(
        "./public/tags.json",
        JSON.stringify(tags), (err)=>{
            if(err){
                console.log(err);
            }
        }
        );
    fs.writeFile(
        "./public/links.json",
        JSON.stringify([...new Set(links)].sort()), (err)=>{
            if(err){
                console.log(err);
            }
        }
    );
    fs.writeFile(
        "./public/internal_links.json",
        JSON.stringify([...new Set(internal_links)].sort()), (err)=>{
            if(err){
                console.log(err);
            }
        }
    );

});