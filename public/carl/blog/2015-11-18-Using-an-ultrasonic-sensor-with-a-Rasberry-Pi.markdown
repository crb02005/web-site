---
layout: post
title:  "Using an ultrasonic sensor with a Rasberry Pi"
date: 2015-11-18T15:48:00-08:00  
categories: 
---
I was helping a friend build a robot the other day and he was having difficulty with the ultra sonic sensor. I offered to test it on my Pi which a "B" model. I followed [this guide](http:\images\images\www.bytecreation.com\images\blog\images\2013\images\10\images\13\images\raspberry-pi-ultrasonic-sensor-hc-sr04) and hooked it up using the single 1k resistor. I used a total of 5 DuPont connector wires and put the resistor between two. I had to alter the timeout of the code to a full 1 second to get valid results. Also I had to adjust the GPIO pins used to the ones that I actually used to hook up. It worked out nicely: 

_UPDATE: DON'T PUT ELECTRONICS ON CARPET LIKE THESE PICTURES SHOW_

![]({{site.url}}/static/images/1a4a3b1a-323d-4012-a40a-885279e8232f.jpg) 
![]({{site.url}}/static/images/c096b41d-4ef5-4c89-899a-14ba18538dfa.jpg)
![]({{site.url}}/static/images/4e40ef8d-7ff5-4b64-a36d-0bccb82e2aa7.jpg)
![]({{site.url}}/static/images/51e8464e-2bd6-4896-a5ba-87a60ed22219.jpg)
![]({{site.url}}/static/images/33ff67db-bf39-4ac8-bbd6-18b27fc1af64.jpg)
