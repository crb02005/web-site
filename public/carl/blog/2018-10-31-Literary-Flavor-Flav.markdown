---
layout: post
title:  "Literary Flavor Flav"
date: 2018-10-31T12:49:43-07:00  
categories: html,wordclock,javascript
---
REACTED which has a single file "Word Clock" which is written in JavaScript. 

Here is a working demo:

[WordClock Demo]({{site.url}}/wordclock.html)

For those which just want a picture:

![Word Clock Picture]({{site.url}}/static/images/wordclock.png)
