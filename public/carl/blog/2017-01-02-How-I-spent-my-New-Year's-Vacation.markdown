---
layout: post
title:  "How I spent my New Year's Vacation"
date: 2017-01-02T21:10:00.002-07:00  
categories: arduino,blog,docker,loot
---
## Blog Engine

During the break I had the chance to post a copy of my Python Blog Engine. It is rough but works well enough for my purposes. You can find it under the REDACTED link on the menu or directly as _Super Awesome Blog Maker_ * repo redacted

## Docker Files

I also added a repo for some [Docker files](https://gitlab.com/crb02005/docker-files) _note: repo has been moved to gitlab 2020_

### UbuntuLibAV

This is one that has what is needed for converting audio files. The README has more info.

## Arduino

I got a chance to play around with my Arduinos this weekend. I ordered a Leonardo since the Pro Micro didn't have the pins soldered on and my FIL is putting it back together.

More later...

-- Carl