---
layout: post
title:  "Octodad: Dadliest Catch"
date: 2015-06-14T17:29:00.001-07:00  
categories: gaming,octodad
---
I bought this game on Steam as an early father's day present to myself. Steam was running their summer sale and I had looked at the title of this game, but I hadn't watched the trailer once I did I knew I had to have it. It is an amazingly unique hilariously game which is also family friendly. I have added this to the list of games I've beaten. It was totally worth the price.