---
layout: post
title:  "Trocar Dice"
date: 2020-11-18T16:48:00-06:00  
categories: csharp
---

Using the Nuget Package I built:
[Trocar Console Harness](https://www.nuget.org/packages/Trocar.ConsoleHarness/)

I built a small console app used for rolling dice.

```
Trocar.Dice.exe 1d20
```

It also supports adding "NamedDice" via the appSettings.json

This was mainly a proof of concept application, but I would like to eventually add a better expression parser, add support for variables. 

In the journey to further my skill set, I've obtained the Donald Knuth book series on "The Art of Computer Programming". I plan to spend a goodly amount of time improving my craft. The first book titled "Fundamental Algorithms" looks quite interesting, and the examples are in Assembly so I might have more of that on my site.