---
layout: post
title: "new machine npm yarn and http-server setup"
date: 2019-4-18T21:13:06-06:00
categories: npm,javascript
---

Here is my go to setup for npm, yarn and a simple http server.

```
mkdir npm_user_global
npm config set prefix ~/Documents/source/npm_user_global
npm install npm -g
export PATH="$HOME/Documents/source/npm_user_global/bin:$PATH"
npm install http-server -g
npm install yarn -g
```
