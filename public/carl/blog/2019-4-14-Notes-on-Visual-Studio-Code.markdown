---
layout: post
title: "Visual Studio Code"
date: 2019-4-13T13:15:06-06:00
categories: vscode
---

One complaint I have with products is poor naming. Naming an editor "code" or another generic name like "Sql server" without prefixing it with "visual studio" or "microsoft" lends to poor searchability.

That being said, VS Code is a great editor. Its extensions allow it to be a great IDE for almost any language. It comes with an integrated debugger for .NET Core out of the box. Here are some cool things to do:

Side by side compare:
```
ctrl+\
```

Then swap between them with  
```
ctrl+(1|2|...n number of other screens)

```

Zooming and unzooming the whole interface with the standard:
```
ctrl++
ctrl+-
```

Swapping between open files with:

```
ctrl+tab
```

The swiss army knife of utilities, the command pallete, is available with:
```
ctrl+shift+p
```

The panel section holds several other interesting things:

* Problems
* Output
* Debug console
* terminal

 It can be toggled with:
```
ctrl+j
```

## Editor Advanced features

word select with:
```
ctrl-d
```

multicursors to make the same changes to multiple places:
```
Alt-Click
```

everyone's favorite markdown preview
```
ctrl+shift+v
```

peek definition of a file:
```
alt+F12
```

## Other cool features:

project workspaces 
