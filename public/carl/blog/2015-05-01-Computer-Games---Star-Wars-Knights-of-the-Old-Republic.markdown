---
layout: post
title:  "Computer Games - Star Wars Knights of the Old Republic"
date: 2015-05-01T12:57:00-07:00  
categories: gaming,kotor
---
I have a serious problem. I've got a backlog of games requiring my attention that I have not started, or started and forgotten where I was, or finish. I'm going to attempt to play through my backlog until I finish the game, main story quest or am completely sick of it. Further I plan on not installing any mods unless if fixes a bug that makes the game unplayable.

I'm going to start with: http://en.wikipedia.org/wiki/Star_Wars:_Knights_of_the_Old_Republic

I plan on documenting my progress here.

Started up and the resolution wasn't good, so I've modified one of my restrictions, by allowing resolution fix mods.
I'm using http://www.flawlesswidescreen.org/

![kotor1]({{site.url}}/static/images/kotor2.jpg)

I'm also planning on not using strategy guides. 


With the widescreen mod the dialog was cut off. 1376x768 works for the dialog, but looks bad.* I had to turn off NVIDA's no scaling options to get the flawless to work.

Possible Spoiler Below







So when you see the Jedi battle on the ship why does it not drop two lightsabers in the remains. Further frustrations. Why can't the combat bot I repaired come with me. Seriously? And where is my third level feat, and why didn't I get an extra feat for playing a human.

Okay, the game won me over with one line:

![kotor2]({{site.url}}/static/images/kotor.jpg)

I've stopped for the night. I'm still on the first world, but the game has a certain charm. I'll probably post more later.
