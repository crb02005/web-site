---
layout: post
title:  "Windows Host to Linux Guest Named Pipe on VirtualBox"
date: 2015-05-05T19:46:00.001-07:00  
categories: 
---


It is fairly simple just open settings:


![host]({{site.url}}/static/images/windows_host.png)


Then you can connect via PUTTY on Windows and cat to or from /dev/ttyS0 and see the results.