---
layout: post
title:  "Watchdog and watchmedo for fun and pep8ing"
date: 2016-12-01T22:53:00.002-07:00  
categories: watchdog
---
If you need to perform an action when a file is changed, or files in a directory are changed just install the python package watchdog. It comes with a utility "watchmedo".

I used it in combination with pep8 to automatically check my code. Here is how I did it:
```
    watchmedo shell-command --patterns="*.py" --ignore-directories --recursive --command='pep8 "${watch_src_path}"'
```
Read more about [watchdog](https://pypi.python.org/pypi/watchdog)