---
layout: post
title:  "CORS with Docker HTTPD"
date: 2017-09-29T16:22:40-07:00  
categories: apache,cors,docker
---
I thought it would be a good idea to host the book site json data, thumbnails, and pdfs in a seperate docker image from the site itself. This way I could spin up all the versions and point at the same files. One problem with this is the same origin problem. To get the server to play nice I had to follow the instructions on Apache:

[Enable CORS](https://enable-cors.org/server_apache.html)

Spoiler just add

    Header set Access-Control-Allow-Origin "*"

to the appropriate node in the conf.

I was using a docker compose file like:

```
    version: '2'
    services:
        web:
            build: .
            image: "httpd_web_server:base"
            ports: 
                - 8080:80
            volumes:
                -  /home/carl/Documents/Books:/usr/local/apache2/htdocs
```

and a docker file:

```
    FROM httpd
    COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf
```

but where did I get the conf?

I spun up the container in interactive mod and grabbed the original.

I ended up plopping the header tag in node starting with

After that things worked nicely.

I should be posting an AngularIO version of the booksite to the repo soon.