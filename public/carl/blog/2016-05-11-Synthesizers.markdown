---
layout: post
title:  "Synthesizers"
date: 2016-05-11T20:00:00-07:00  
categories: music,electronics
---
After watching:  

[http://www.idreamofwires.org/](http://www.idreamofwires.org/)  

I was sold on the idea of obtaining a synthesizer. I had done a little research and realized it was going to be an expensive hobby. I decided I was going to get the cheapest thing possible:  

[http://www.thesynthesizersympathizer.com/2015/03/buying-your-first-analog-synthesizer.html](http://www.thesynthesizersympathizer.com/2015/03/buying-your-first-analog-synthesizer.html)  

The [MicroBrute](https://www.arturia.com/products/hardware-synths/microbrute). I found one for less than retail from an auction site.  

I've been watching videos about synthesizers and found a tutorial:  

[https://www.youtube.com/watch?v=LBY-AtQwP_M](https://www.youtube.com/watch?v=LBY-AtQwP_M)  

I'm pretty sure I need to find a EuroRack power module and begin collecting.  

Here are some interesting modules I found:  

[http://www.analoguehaven.com/mfb/seq01/](http://www.analoguehaven.com/mfb/seq01/)  

[https://www.youtube.com/watch?time_continue=273&v=xhNljvO0GCc](https://www.youtube.com/watch?time_continue=273&v=xhNljvO0GCc)
