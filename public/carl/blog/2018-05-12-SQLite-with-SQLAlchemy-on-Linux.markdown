---
layout: post
title:  "SQLite with SQLAlchemy on Linux"
date: 2018-05-12T19:59:48-07:00  
categories:  sqlite,sqlalchemy
---
Recently I reformatted my laptop and popped on a copy of Linux. For a little bit I had been playing around with Windows 10. While it was great to have the Windows Linux Subsystem it just wasn't the same as using Linux as my primary OS. 

When I switched I pulled my REDACTED down from REDACTED and tried to run it, to my surprise it worked just fine on Windows 10. 

I got an error:

“SQLite objects created in a thread can only be used in that same thread”

After some research I found out that the issue was with thread pooling. 


[http://docs.sqlalchemy.org/en/latest/dialects/sqlite.html#threading-pooling-behavior](http://docs.sqlalchemy.org/en/latest/dialects/sqlite.html#threading-pooling-behavior)

I'm a little curious as to why it only complained in Linux, but for now toggling the flag in my call to create engine fixed it.
