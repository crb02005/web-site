---
layout: post
title:  "Docker Tech Talk for Central Arkansas JavaScript"
date: 2018-09-04T20:19:02-07:00  
categories: javascript,docker
---
For my loyal blog readers I have gone ahead and published my slides and example demo folders:

[https://gitlab.com/crb02005/tech-talks/tree/master/JavascriptConway/Docker](https://gitlab.com/crb02005/tech-talks/tree/master/JavascriptConway/Docker)
