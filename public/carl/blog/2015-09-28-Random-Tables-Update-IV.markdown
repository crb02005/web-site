---
layout: post
title:  "Random Tables Update IV"
date: 2015-09-28T16:26:00-07:00  
categories:
---
I was able to connect the Azure site to Google Domains and left it running for a week or two and it worked fine. I have since shut it down the Azure application and removed the Storage files from the cloud. The repos are available on GitHub per my previous post. Development has stopped for the common library and the Web Portal until bugs are reported. I may update the data packs at a later date, but have begun to work on other projects until demand increases.
