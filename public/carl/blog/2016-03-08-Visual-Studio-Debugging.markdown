---
layout: post
title:  "Visual Studio Debugging"
date: 2016-03-08T19:12:00-08:00  
categories: vscode
---
If you are a VB.NET developer for any length of time you probably will need to debug code. One of the tools I use is the Immediate Window. CTRL-ALT-I will open it. If you use the ? shortcut to print the value of a variable with multiple lines you probably get something like "Blah"+vbnewline+"someotherfield", but with the string formatter ",nq" you can get the following:

Blah
someotherfield


I hope this helps.
