---
layout: post
title:  "SQLite Recap"
date: 2018-02-15T16:46:38-07:00  
categories: presentation
---
Tuesday, I gave a presentation for the [Little Rock .NET User Group](http://lrdnug.org/SQLite-the-little-DB-that-could-Also-Kanban-framework/) it was my first time presenting on SQLite. First, I had more content than I needed for the time slot. I thought I might be the only speaker so I prepared more content than I needed. If I had to do it over again I would have put the code example first. I didn’t make it to my powershell example, which I thought would be useful to a lot of developers. Most people like to see code rather learning facts, trivia and "what you can do" with a technology.

The next thing I realized was from watching Paul Gower’s presentation "Intro to Kanban". Sometimes less is more, simple design, and effective use of color can bring a lot to a presentation. This wasn’t his first tech presentation, so his had more polish. I could tell that his was passionate about his choice of topic, Kanban. He plugged [Huge.io](http://www.huge.io/) very effectively and I would be willing to give it a try for my team if it was an option.

At the end of the presentations they gave away prizes and I won a subscription to JetBrains. I selected [Resharper Ultimate + Rider](https://www.jetbrains.com/store/?fromMenu#edition=commercial). I installed it and used some of the tools to clean up my presentation example. After additional tweaks I plan on placing it in a repo. I will leave the one I linked in my previous blog for posterity. I was impressed with the tool and think it would be a worth while addition to a dev team’s toolbelt.

SQLite is a timely topic. I got my copy of [SDTimes](https://sdtimes.com/) and they had an article about SQLite in the Feb Print issue, but it is linked online [here](https://sdtimes.com/data/apps-database-stinks-orm/).

I was going to record the presentation but I must have not pushed the record button hard enough, in any case I plan to redo the presentation and record it for YouTube in a three part series. With that goal in mind, I watched some YouTube tutorials for Inkscape for Logo creation. I’ve used Inkscape but wanted to improve my skill set so I watched: [this awesome video by Nick](https://www.youtube.com/embed/hmaYeZ5iDPo)


I made a few tweaks and I haven’t finalized my logo, but here is what I’ve come up with:

![Carl Burks Developer]({{site.url}}/static/images/logo.png "Logo")
