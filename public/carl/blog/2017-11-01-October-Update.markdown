---
layout: post
title:  "October Update"
date: 2017-11-01T19:45:48-07:00  
categories: 
---
I have lots of really good reasons for my lack of updates so here is a mega update.

## Pepper Sauce

I friend at work was nice enough to bring me some peppers from his garden. We made a great pepper sauce with vinegar.

![Pepper Sauce]({{site.url}}/static/images/pepper_sauce.jpg)

Pepper Sauce



## Lemon Penny Battery

We got inspired from various YouTube channels, namely, _King of Random. I had been trying to do at least one electronic project a week, so I did one the kids could enjoy and help me with.

![Lemon Battery]({{site.url}}/static/images/lemon_battery.jpg)

Lemon Battery



![Lemon Battery Measured]({{site.url}}/static/images/lemon_battery_2.jpg)

Lemon Battery Measured



Supplies:

*   Scissors
*   Pennies after 1986
*   Tape or a container for the battery
*   An acid _lemon juice, vinegar, or try something_
*   Cardboard
*   sandpaper or a file
*   a bowl for the acid

Instructions

*   File down one side of the pennies
*   align the pennies so all the copper sides face the same way
*   Cut strips of cardboard into squares
*   Alternate pennies and cardboard soaked in acid
*   _Measure the cell_
*   repeat until desired voltage
*   wrap tape around it leaving the top and bottom exposed

## Puppy

The family got a puppy.

![Family Dog]({{site.url}}/static/images/puppy.jpg)

Family Dog



## Makerspace

Many sleep interrupted nights later, I attended a maker space training:

![Maker Badge]({{site.url}}/static/images/make_badge.jpg)

Maker Badge



I made a Hex Battle board:

![Hex Battle Board]({{site.url}}/static/images/hex_board.jpg)

Hex Battle Board



Here is the

[Hex Map Python script](https://gitlab.com/crb02005/rpg-util/blob/master/python3.5/console/hex_map.py) _move to gitlab 2020_

I wrote to make it.

![Name Plate]({{site.url}}/static/images/name_plate.jpg)

Name Plate



I told my mom I'd make her a PreAmp I made a nicer case than my Radio shack special: ![Enclosure for another PreAmp]({{site.url}}/static/images/mud_sizzle_enclosure.jpg)

## Super Secret Special Project

I've been working on a super secret special project. I hope to have some updates after it is no longer a secret.

## New Equipment

I got a new to me HP-54601B ocilloscope.


![My First Ocilloscope]({{site.url}}/static/images/ocilloscope.jpg)

My First Ocilloscope

