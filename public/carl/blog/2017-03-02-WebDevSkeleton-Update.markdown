---
layout: post
title:  "WebDevSkeleton Update"
date: 2017-03-02T17:13:10-07:00  
categories: framework,nodejs,typescript
---
# WebDevSkeleton Update

I've added a few things to my Web Dev Skeleton framework I've been building.

*   Redis
*   Typescript Spa Routing (This needs some route parameter improvements)
*   MariaDB
*   SQLAlchemy
*   Mustache (haven't implemented it in the router, but it is included now)

I took out LoDash because I wasn't using it yet.

Where am I going from here?

*   Issues
    *   git issues
    *   Add Web Sockets
        *   [gevent-websocket](https://pypi.python.org/pypi/gevent-websocket)
        *   [Client code example](https://bitbucket.org/noppo/gevent-websocket/src/0df192940acd288e8a8f6d2dd30329a3381c90f1/examples/wamp/wamp_example.html?at=default&fileviewer=file-view-default)
    *   Add Service Process
*   Documentation
    *   Need to learn a bit more about UML so I'll probably order [UML Distilled](http://www.sphinx-doc.org/en/stable/)
    *   [Sphinx](http://www.sphinx-doc.org/en/stable/)
*   Tests
    *   Client side [Jasmine](https://jasmine.github.io/)
    *   [unittest](https://docs.python.org/3/library/unittest.html#module-unittest) Python tests for the services
*   Business Intelligence Solution
    *   [CloverETL](http://www.cloveretl.com/)
    *   [Pentaho](http://www.pentaho.com/)
    *   [Talend](https://www.talend.com/)
    *   [Hive](https://hive.apache.org/)
*   Backups

What am I going to do once I get the framework setup the way I'd like it?

Here are some of my ideas:

*   Shared Dice Roller (I always build a dice roller)
*   TIS inspired game
*   ???