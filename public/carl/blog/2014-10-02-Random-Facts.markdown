---
layout: post
title:  "Random Facts"
date: 2014-10-02T16:33:00.002-07:00  
categories: gaming,rpg,pen and paper
---
If you are a game designer for a complex game which involves pseudo physics and need to know roughly how much Beluga Whale weighs I found a cool website that tells you exactly that:  
[http://thewebsiteofeverything.com/animals/mammals/adult-weight.html](http://thewebsiteofeverything.com/animals/mammals/adult-weight.html)  

You can go to the root of the site and read about all sorts of plants and animals. Great for any world designer.  

-Carl Burks