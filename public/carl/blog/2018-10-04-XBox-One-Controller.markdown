---
layout: post
title:  "XBox One Controller"
date: 2018-10-04T22:20:45-07:00  
categories: electronics
---
When you have three boys, and a dog things are likely to get broken. Sometimes it is something cheap, sometimes it is something expensive, and sometimes it is something cheap which is an integral piece to something expensive. This is the case with the Xbox one controllers. The designers of the Xbox one controller picked cheap brittle plastic for the analog directional sticks on a $50 wireless controller. If you have to repair one of these having a desolder tool is really helpful to take out the old piece. 

There is a kit on amazon for less than 10 USD  which has two of the replacement pieces. I followed a YouTube video and I was able use the once broken controller again.


![Xbox repair]({{site.url}}/static/images/xboxrepair.gif)
