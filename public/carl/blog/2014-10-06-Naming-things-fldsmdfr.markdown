---
layout: post
title:  "Naming things fldsmdfr"
date: 2014-10-06T17:49:00.002-07:00  
categories: 
---
Sometimes the hardest part of creating something new is giving it a name. The name of something can greatly contribute to its success. If you have seen "Cloudy with a Chance of Meatballs" you have watched the plight of engineers illustrated in a humorous fashion. For developing software a badly named variable might confuse support developers. A badly named application might not be purchased. A clear concise variable is a jewel.  

Now for a few words on vowels.  

Example:  
getSampleWidget  

Example 2:  
getSmplWdgt  

Which would you like to read?  What are you saving? Are you using a language that has an unreasonable limit on variable names?  

Further Reading:  

StackOverflow - maximum length:    
[http://stackoverflow.com/questions/425988/maximum-method-name-length](http://stackoverflow.com/questions/425988/maximum-method-name-length)  

Wikipedia - naming conventions:  
[http://en.wikipedia.org/wiki/Naming_convention_%28programming%29](http://en.wikipedia.org/wiki/Naming_convention_%28programming%29)  

-Carl Burks 