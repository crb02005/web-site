---
layout: post
title:  "Music Install Notes"
date: 2017-04-23T22:43:13-07:00  
categories: jackd,linux,music
---
So the following is a bit different than my usual blog. This more of series of notes from me getting a linux audio computer setup. Since I was installing as I went some directions may be countermanded in further parts of the notes.

low latency kernel

```
    sudo apt install linux-lowlatency

    sudo apt install jackd jack-tools qjackctl fftw3 pulseaudio-module-jack  
```

dbus-python Add some limits for JACK by editing the limits.conf

[/etc/security/limits.conf](https://linux.die.net/man/5/limits.conf)

```
    # caps the real time priority to 99 
    @audio   -  rtprio     99
    # gives it unlimited memory locking
    @audio   -  memlock    unlimited
    # sets the allowance for running with modified scheduling priority
    @audio   -  nice      -19
```

Add yourself to the audio group

```
    sudo usermod -a -G audio yourusernamehere
```

edit the /etc/pulse/default.pa

```
    load-module module-jack-sink
    load-module module-jack-source
```

Time to retry

```
    sudo apt-get remove --purge alsa-base pulseaudio
    sudo apt-get install alsa-base pulseaudio
```

Fire up the backend with _this will change with your hardware_

```
    jackd -R -P4 -dalsa -r44100 -p512 -n4 -D -Chw:0 -Phw:1
```

So the problem was I was trying to use the hardware 0 instead of hardware 1

```
    -Phw:1
```

Time to add [Ardour](http://ardour.org/), it is a [DAW Digital Audio Workstation](https://en.wikipedia.org/wiki/Digital_audio_workstation)

```
    sudo apt install ardour
```

Then [Audacity](http://www.audacityteam.org/) "...free, open source, cross-platform audio software for multi-track recording and editing."

```
    sudo apt install audacity
```

Why not a drum machine [Hydrogen](http://www.hydrogen-music.org/hcms/)

```
    sudo apt install hydrogen hydrogen-drumkits hydrogen-drumkits-effects 
```

qjackctl doesn't look as cool as it could so why not Gladish GUI ladish

```
    sudo apt install gladish
```

Got a guitar? Soft effects [Rakarrack](http://rakarrack.sourceforge.net/)

```
    sudo apt install rakarrack
```

Comfortable with reading? Like the shell? No install would be complete without:

*   [ffmpeg](http://ffmpeg.org/)
*   [sox](http://sox.sourceforge.net/Main/HomePage)

```
    sudo apt install sox ffmeg
```

All done? Not quite yet you need some MIDI

You need something to make the MIDI data turn into audio:

qsynth front end for FluidSynth

```
    sudo apt install qsynth
```

Need to test out your new qsynth then try jack-keyboard:

```
    sudo apt install jack-keyboard
```

Time to connect the MIDI to ALSA.

```
    sudo apt install a2jmidid
```

Don't forget to launch it.

No sound? Whelp you forgot to install a soundfont.

You can search the interwebs for ones.

After you get that working don't forget you can run it into the same pedal board.

Looks like you forgot everyone's favorite media player [VLC](http://www.videolan.org/vlc/) not not that one [CMUS](https://cmus.github.io/)

Get them both:

```
    sudo apt install cmus vlc
```