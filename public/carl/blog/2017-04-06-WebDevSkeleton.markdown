---
layout: post
title:  "WebDevSkeleton"
date: 2017-04-06T21:21:12-07:00  
categories: layers,rabbitmq,stomp
---
Once again I find myself looking at the stack involved inmy Web Development Skeleton and I believe I can tighten it up once more.

[RabbitMQ Web STOMP Plugin](https://www.rabbitmq.com/web-stomp.html)

There is certainly something to say for reading the manual end to end before fiddling around.

[STOMP](http://stomp.github.io/)

_STOMP is the Simple (or Streaming) Text Orientated Messaging Protocol._

This seems like the thing to use.

Before:

*   Docker
*   Grunt
*   HTML
*   Javascript
*   CSS
*   SASS
*   TypeScript
*   Flask
*   Flask WebSocket
*   RabbitMQ
*   Python
*   SQLAlchemy
*   Redis
*   MySQL

The next point is Apache Storm, a distributed stream processor. To get this working with Python the co-founder of Parse.ly has taken care of this with streamparse.

After:

*   Docker
*   Grunt
*   HTML
*   Javascript
*   CSS
*   SASS
*   TypeScript
*   RabbitMQ STOMP
*   RabbitMQ
*   Storm
*   streamparse
*   Python
*   SQLAlchemy
*   Redis
*   MySQL

Instead of handrolling the processing from RabbitMQ in Python I'm going to pickup a library for doing it.