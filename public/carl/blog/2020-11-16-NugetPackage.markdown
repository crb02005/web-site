---
layout: post
title:  "Creating a Nuget Package"
date: 2020-11-12T16:48:00-06:00  
categories: csharp,nuget
---

I have been working on a game in my spare time. It is a glorious old school revival of BBS doors, with a theme of Space Trading, and plans for landable planets with some of the features of a MUD. This is all well and good, but I've been keep the code private, but I've got a lot of plumbing code which I believe will be useful in my other projects. To that end I will be extracting bits and packaging it up.

Here is the first one:

[Trocar Console Harness](https://www.nuget.org/packages/Trocar.ConsoleHarness/)

## What is it?

It gives you a program and application to override. It also pulls in Microsoft's configuration, and NLog. 

What does this look like?

Your program.cs

```csharp
using System;
using Trocar.ConsoleHarness;

namespace ConsoleApp1
{
    class Program: Trocar.ConsoleHarness.TrocarProgram<SuperApp>
    {
        static void Main(string[] args)
        {
            TrocarProgram<SuperApp>.Main(args);
        }
    }
}

```

Your Application:

```csharp
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class SuperApp: Trocar.ConsoleHarness.TrocarApplication
    {
        protected override void onConfigureServices(string[] args, IServiceCollection sc)
        {
            base.onConfigureServices(args, sc);
            sc.AddScoped<Foo, Foo>();
        }
        protected override void onStart(ServiceProvider sp)
        {
            base.onStart(sp);
            sp.GetService<Foo>();
        }
    }
}

```

Then some class like Foo can inject a logger:

```csharp
namespace ConsoleApp1
{
    public class Foo
    {
        public Foo(ILogger<Foo> logger)
        {
            logger.LogInformation("Foo Foo Foo");
            Console.WriteLine("Foo was here");
        }
    }
}
```

Make an appSetting.json file:


```json
{
  "NLog": {
    "throwConfigExceptions": true,
    "rules": [
      {
        "logger": "*",
        "minLevel": "Information",
        "writeTo": "logconsole"
      },
      {
        "logger": "*",
        "minLevel": "Information",
        "writeTo": "logFile"
      }
    ],
    "targets": {
      "logfile": {
        "type": "File",
        "fileName": "C:\\logs\\superApp.txt",
        "deleteOldFileOnStartup": true
      },
      "logconsole": {
        "type": "Console"
      }
    }
  }
}
```

Want more info:
[NLog.Extensions.Logging Wiki](https://github.com/NLog/NLog.Extensions.Logging/wiki/NLog-configuration-with-appsettings.json)

AppSettings normally works out of the box in a web project, but with this package you get similar functionality from a console app.

I should be adding more repos which make use of this package that are not directly related to my super secret game project. In additional I will be adding more nuget packages for useful common functionality.

So what does it take to make a Nuget Package?

1. You need an icon
2. You need a repo
3. You need to package it

The icon was a simple matter of firing up GIMP and making a 128x128 png.
I used gitlab to make the repo.
For packing it I looked at the properties of the project and updated the entries in package tab. Then I ran "Package" from Build > Pack. After that I uploaded it to Nuget. 


