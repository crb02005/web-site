---
layout: post
title:  "Minecraft: The Castle that commands and command blocks built"
date: 2015-11-22T17:35:00-08:00  
categories: minecraft,gaming
---
My middle son was playing Minecraft and wanted a command block. He didn't really know what they were or what they did but he wanted one. I did a little research and found you have to use a command to get a command block.

/give YOUR_PLAYER_NAME_HERE command_block COUNT_DEFAULTS_TO_ONE

This gives the player listed the number of command blocks specified. If no number was specified it defaults to one. 

If you wanted 47 diamonds you would do something like:

/give YOUR_PLAYER_NAME_HERE diamond 47

Once you have mastered that you can move on to the next phase.

Targeting the nearest player. Instead of targeting a specific player you can target the nearest with @p 

/give @p command_block

That gives the nearest player a command block. Pretty handy.

If you place a command_block and put a button on it you execute the command with the press of the button. 

I have made a command_button giver:


![]({{site.url}}/static/images/a1356355-6d2d-44a5-954d-2d05c4ebfde1.png)


And the command:
![]({{site.url}}/static/images/10da23fc-ca5e-467a-9060-2f8d4f84e826.png)

*Note Previous Output user name removed.

So this is moderately interesting. You could use a dispenser for this but you would have to keep filling it up. This is a close enough to infinite item spawner.

You can also fill an area with an item. If you fill it with air you can clear a flat space to build a base. This is useful if you want a flat area to build on, but like the surrounding terrain, versus the super flat world.

Fill works thusly:

/fill X Y Z X Y Z ITEM_TO_FILL

Why did I put X Y Z twice? It is a range. This range can be a single block if the numbers are the same or a column, or row, or a cube.

To build my castle I cleared a bit of terrain. To find the X Y Z press "F3" on your keyboard. It shows the position of you in XYZ, and the block in Block.

![]({{site.url}}/static/images/a77e14df-061f-45f7-ab42-1cfd553a88b0.png)

To hide the debug window press "F3" again. Through a bit of trial and error you will soon be an expert in three dimensional coordinates. 

This page has a pretty comprehensive list of items:

http://www.minecraftinfo.com/IDList.htm

An important item to remember is "air" that lets you clear a space. I cleared a big cube to start. Then I added walls. The only bit that wasn't added was the crenellations.

![]({{site.url}}/static/images/988e2495-9ad3-480c-a2ed-a5c09f71a378.png)


Using this fill command you can make gates open and close as well as drawbridges.

![]({{site.url}}/static/images/1a3a19a9-623d-4399-af9a-ab202cdb3433.gif)

You can connect the blocks together for multiple effects using redstone wire. If you want a delay use a redstone repeater. 

![]({{site.url}}/static/images/ce7dbd18-f47b-4aef-9572-b033059f7465.gif)

Here is my close gate and remove the draw bridge.
![]({{site.url}}/static/images/edec73b1-0576-4c5e-9a4e-a088ebea493a.png)

![]({{site.url}}/static/images/d3406c30-a6e1-4423-8218-f69f46782e64.png)

![]({{site.url}}/static/images/31c13861-c424-4002-afa0-e5b3dfb36f6d.png)

![]({{site.url}}/static/images/e2e67e6e-2a4c-49d5-95c9-e513394ff834.png)

![]({{site.url}}/static/images/1e1f0e4f-7f22-4f85-8b1d-284163a633b3.png)

You can even make a convertible castle:

![]({{site.url}}/static/images/00f30836-6973-4f73-b2c8-885d6221d754.gif)


![]({{site.url}}/static/images/c1e84996-cc4f-4f3d-ae33-58c14930c766.png)

![]({{site.url}}/static/images/b4c48cdd-f51f-4497-b91b-11d1ebfaa882.gif)


![]({{site.url}}/static/images/37c142e4-1ae4-444e-a0ff-d18908a02dd8.png)


Okay that was fun but what about making some monsters?

/summon CREATURE_NAME X Y Z


![]({{site.url}}/static/images/ddac440f-120a-4269-a926-842b8bbb3b5e.png)