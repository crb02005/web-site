---
layout: post
title:  "Random Tables Update III "
date: 2015-09-09T17:18:00-07:00  
categories: 
---

I have split the repo into three parts: RandomTablesCommon - portable library imported as a submodule into the other repos. 
RandomTablesCommon - REDACTED
RandomTablesUniversal - Windows 10 Universal app. It imports RandomTablesCommon as a submodule. REDACTED
RandomTables - Web Application - REDACTED

It imports RandomTablesCommon as a submodule. REDACTED Since I split the modules I went ahead and got a domain name. I'll announce it once I have the site up. I am working on making the Web App work on the Azure platform. This means I had to add yet another string provider. I haven't committed my changes which add Azure features. I am still developing that piece. Azure has blob storage, and many other cool features. 

Here is an article I've been reading about the topic: [https://azure.microsoft.com/en-us/documentation/articles/storage-dotnet-how-to-use-blobs/](https://azure.microsoft.com/en-us/documentation/articles/storage-dotnet-how-to-use-blobs/) 

Here is a NuGet package: [http://www.nuget.org/packages/WindowsAzure.Storage](http://www.nuget.org/packages/WindowsAzure.Storage) I had a chat with some friends of mine Monday and I might have some content contributors after I get the Web port on the web. After I get things stabilized I might shift focus to community building. Adding creative common content will make this product really useful. Now time for a small bit about BOM. Byte Order Mark (BOM) is optionally included on UTF-8 files. This goes unnoticed until your JSON parser stops working. With my latest development for Random Tables I started yet another implementation of a string provider. The Azure Blob string provider uses a memory stream to read the file. The Windows 10, and System.IO.readAllText seem to chomp the BOM and it worked flawlessly. Reading from the memory stream it is a bit more manual. BOM's are easy to forget until they "Byte" you. [http://www.w3.org/International/questions/qa-byte-order-mark.en.php](http://www.w3.org/International/questions/qa-byte-order-mark.en.php)
