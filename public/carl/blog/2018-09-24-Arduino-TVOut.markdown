---
layout: post
title:  "Arduino TVOut"
date: 2018-09-24T12:22:32-07:00  
categories: arduino
---
My son wanted to start soldering, so I found a project I thought wouldn't take much so I gathered the parts to connect an Arduino to the television. I had an RCA style connector, the two required resistors 470ohm and 1k, a small board, 3 header pins and some wire _yes I know they are the wrong color_. I let him use the iron and helped him get it up and running. We downloaded the library from the internal library and it didn't work out of the gate, so we found a beta version on github which had the missing fontall.h file and dropped it in the Arduino directory. We loaded up the demo sketch and it worked. Afterwards made it say various things, and had a fun morning getting an electronics, and MCU project under our belts. 

Obligatory pictures:

![TVOut demo 3]({{site.url}}/static/images/tvoutfront.png)

![TVOut demo 2]({{site.url}}/static/images/tvoutboard.png)

![TVOut demo 1]({{site.url}}/static/images/tvoutscreen.png)

Some caveats apply. 

The resolution isn't great. It starts with a default resolution and will let you make it better, but depending on the board you could chew through your available memory trying to make the screen look better.

The color choices are Henry Ford style, as long as it is black _and white_.

What is next for our new TV enabled Arduino? 

There are plenty of applications you might want to hook an Arduino up to a screen. 

* Reinvent pong
* Sensor kiosk
* Morse code reader
* Homemade Karaoke
