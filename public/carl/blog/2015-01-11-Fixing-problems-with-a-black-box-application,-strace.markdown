---
layout: post
title:  "Fixing problems with a black box application, strace"
date: 2015-01-11T12:29:00-08:00  
categories: strace,bash,linux,crayon physics
---
If you don't know about this command you should:  

_**strace  -** trace system calls and signals_  

You can see which files a specific program touches and a lot, lot more.  

Here is a blog that goes into more than sufficient detail about the topic:  
http://chadfowler.com/blog/2014/01/26/the-magic-of-strace/  

I was trying to get Crayon Physics to work on a Linux box and the error messages were less than helpful, but I was able to make forward progress thanks to this program.