---
layout: post
title:  "Steam, Origin, Web API"
date: 2018-05-06T13:38:10-07:00  
categories: wls,webapi,powershell,origin,steam
---
I started this little project because I had some Sims keys on steam and some on Origin and I wanted my game to install them both correctly. So after I manually added those keys I got to thinking what other games do I have that might unlock on Origin. So I went to Steam an registered an API key:

[https://steamcommunity.com/dev](https://steamcommunity.com/dev)

Once I had that I read the docs and came up with a URL that would give me a list of my games:

```
http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={secret_key}&steamid={steamid}&format=json&include_appinfo=1
```

Swap out secret_key for your api key and steamed for your steam id.

My trick for finding my steam id is to open my community profile and view source. It is in the HTML.

Once I downloaded the game list to xml I used PowerShell to XPath the game names out:

```
[xml]$data = gc .\v0001.xml
Select-Xml "//response/games/message/name" $data | %{$_.Node.InnerText} | Sort-Object -Unique > games.txt
```
That game me a list of my steam games, next I needed a list of EA games.

I opened the Wikipedia page for EA games [https://en.wikipedia.org/wiki/List_of_Electronic_Arts_games](https://en.wikipedia.org/wiki/List_of_Electronic_Arts_games)

Next I used the developer tools from the browser to just grab the table. I saved it into its own html file. Next I opened it in a browser and copy pasted it into a spreadsheet.

I deleted the extra columns I didn't need then pasted the one column I wanted into a text file.

After that I tried to compare the files using grep from the Windows Linux Subsystem:

```
grep -f sortedea.txt games.txt
```
It didn't produce any results. Next I looked at the files and realized Windows had put them in an encoding I didn't care for. I popped them open with VS Code and swapped the encoding. Then I compared again. Sadly that didn't work out how I wanted. There was too much discrepancy between the names. I could have done a levenshtein distance comparison, but instead I just put both files into Excel and added a "Source" column then I sorted by name. 

Then I manually compared the games. Well it wasn't quite a complete automated solution, but it was good enough.

Obligatory [xkcd](https://xkcd.com/1319/).
