---
layout: post
title:  "Scaffolding"
date: 2017-02-18T16:35:10-07:00  
categories: gitkraken,repos
---
# Building Project Scaffolding

A few days ago I began building a docker compose for Web Dev Scaffolding. I have moved it from the Docker Files repo to its own.

If you don't feel like rolling your own of things consider:

[Yeoman](http://yeoman.io/)

From their site:

    Yeoman helps you to kickstart new projects, prescribing best practices and tools to help you stay productive.

They have tutorials and guides to getting started with several popular libraries.

I'm not using yeoman right now, but I wanted to link to it for those who might not be aware of what it can do.

In my project I've got the following stack:

*   docker
    *   node
        *   npm
        *   gulp
            *   sass
            *   javascript
                *   jquery
                *   knockoutjs
                *   lodash
            *   typescript _implemented with some consideration_
            *   bootstrap
        *   browsersync _not yet implemented_
    *   Python _server api endpoint_
        *   Flask
        *   SqlAlchemy _not yet implemented_
    *   Redis _caching_
    *   MariaDB _long term storage, queries, and reporting_ _not yet implemented_
    *   RabbitMQ _queing process workflow management_ _not yet implemented_
    *   Celery _considering usage_
    *   Python _work process_ _not yet implemented_

## Considerations:

### Docker

Binding to 0.0.0.0 and making networks

### Typescript

Adding typings seemed to make GitKraken fail to open the repo. I've added them to the ignore for now. The mapings aren't being made.