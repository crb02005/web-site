---
layout: post
title:  "Battery on Xmobar"
date: 2013-10-29T20:20:00-07:00  
categories: linux,xmonad
---
The battery plugin didn't seem to work:
```
pacman -S bc
```
gives me a calculator that I can use to divide the full energy from current energy.

```
echo "scale=2;$(cat /sys/class/power_supply/BAT0/energy_now)/$(cat /sys/class/power_supply/BAT0/energy_full)"| bc -q
```

a simple cat of 
```
/sys/class/power_supply/BAT0/status
```
gives me charging or discharging
Heres what I've got so far:

~/.xmobarrc 

```
{% raw %}
Config {bgColor = "black"
, fgColor = "yellow"
, position = Top
, commands = [    
    Run Com "cat" ["/sys/class/power_supply/BAT0/status"] "battstat" 1000,
    Run Com "battpercentage" [] "battcharge" 1000,
    Run StdinReader
    ]
, sepChar = "%"
, alignSep = "}{"
, template = "%StdinReader%}{%battcharge% %battstat% %date%"
}
{% endraw %}
```