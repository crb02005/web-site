---
layout: post
title:  "Counting calories with a few commands"
date: 2014-02-11T19:14:00.002-08:00  
categories: linux
---
Lets say you like to use the Linux shell for totaling up your calories. Maybe you've got a file something like this:  

590<tab>soft beef tacos  
290<tab>fish sandwich  
380<tab>fries  

You could manually calculate these or... you could use some nifty Linux programs. First lets get those numbers away from the rest of the file.  

cat 20140211.calories | cut -f 1  

gives you:  

590
290
380  

cut uses <tab>as the default delimiter and we want the first field.  

Now you've got the numbers but they are not in a usable math expression so you can use "paste"  

cat 20140211.calories | cut -f 1 | paste -s -d+  

gives you:  

590+290+380  

now you just need to total it all up with "bc"  

[ExplainShell.com](http://explainshell.com/) can give a complete break down:  

[cut -f 1 | paste -s -d+ | bc](http://explainshell.com/explain?cmd=cut+-f+1+|+paste+-s+-d%2B+|+bc)
