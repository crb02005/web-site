---
layout: post
title:  "Apache on Ubuntu inside of VirtualBox Headless"
date: 2016-07-07T19:01:00.002-07:00  
categories: mariadb,mysql,ubuntu,virtualbox
---
# Ubuntu LAMP Headless

1.  Install PUTTY or SSH utility (optional)
2.  Install VirtualBox
3.  [Download latest server ISO from Ubuntu.](http://www.ubuntu.com/)
4.  Provision the Virtual Machine to match or exceed the recommendations for minimum system specifications
5.  Attach the ISO and install
    *   use entire disk no LVM
    *   install updates automatically
    *   standard system utilities
    *   LAMP server
        1.  Set a MySQL password
    *   OpenSSH (optional)
    *   GRUB on boot is fine for dedicated VM
6.  Add port forwarding to your NAT
    *   map a port for SSH
    *   map a port for Web Traffic
7.  Install Guest Additions
    1.  sudo mount -t iso9660 -o ro /dev/cdrom /media/cdrom
    2.  cd /media/cdrom
    3.  sudo apt-get -y install gcc make
    4.  sudo ./VBoxLinuxAdditions.run
    5.  sudo umount /media/cdrom
    6.  sudo /sbin/shutdown -r now
8.  start headless virtualbox instance
    *   ssh with PuTTY or another tool
9.  Add Shared Directory for web
    1.  create script to automap in /etc/init.d Sample Script:  

            #!/bin/bashsudo mount -t vboxsf PublicWeb /var/www/html

    2.  Make script executable  

                sudo chmod +x /etc/init.d/mountVBoxDirectories.sh

    3.  Update RC to run at start  

                sudo update-rc.d mountVBoxDirectories.sh defaults