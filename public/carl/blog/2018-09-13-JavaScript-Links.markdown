---
layout: post
title:  "JavaScript Links"
date: 2018-09-13T12:04:47-07:00  
categories: javascript
---
# JavaScript Resources
## Package Managers
* [APM atom package manager](https://atom.io/packages)
* [NPM](https://www.npmjs.com/)

## Bundlers
* [Webpack](https://webpack.js.org/) 

## Taskrunners 
* [Grunt](https://gruntjs.com/)
* [Gulp](https://gulpjs.com/)

## Other Things
* [NodeJS](https://nodejs.org/en/)
* [Electron](https://electronjs.org/)
* [JSLint](https://www.jslint.com/)
* [Mozilla Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* [JSFiddle](https://jsfiddle.net/)

## Binding Frameworks 
* [Vue](https://vuejs.org/)
* [React](https://reactjs.org/)
* [Angular](https://angular.io/) 

## Legacy Binding Frameworks
* [AngularJs](https://angularjs.org/)
* [KnockoutJS](http://knockoutjs.com/) 

## Compiled JavaScript 
* [TypeScript](https://www.typescriptlang.org) 
* [Dart](https://www.dartlang.org/) 
* [Babel](https://www.babeljs.io) 
* [CoffeeScript](https://www.coffeescript.org) 
* [emscripten](https://www.emscripten.org) 

## Editors 

### Free 
* [Atom](https://atom.io/)
* [Code](https://code.visualstudio.com/)
* [Notepad++](https://github.com/notepad-plus-plus)
* [vim](https://www.vim.org/) 
* [emacs](https://www.gnu.org/software/emacs/) 

### Non-Free 
* [Sublime](https://www.sublimetext.com/)
* [UltraEdit](https://www.ultraedit.com/)