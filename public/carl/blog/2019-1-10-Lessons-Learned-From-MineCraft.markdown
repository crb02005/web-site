---
layout: post
title:  "Lessons learned from MineCraft"
date: 2019-1-10T12:35:06-06:00  
categories: minecraft
---

I've been on a MineCraft binge lately. It might have something to do with my three sons who are in love with MineCraft. We've been watching Grian, MumboJumbo and the HermitCraft season six. As a result I've setup a bedrock server for the kids. After they had a little fun in survival mode they also wanted to play creative in their own private realm. We didn't really want to shut one down and have the other one running, and I didn't want to setup two boxes. Here is what I ended up doing.

19132 is a very special port for MineCraft bedrock and it really doesn't like it when you bind to another port, at least for iPads, and XBox One. When you use that port it shows up in the "Friends" tab. If you don't the next tab might let you connect, but I wanted it to work as easily for the users as I could. I looked around and didn't see much in the way of help for running two servers on the same box at the same time.

I had the idea to double up on IP address and route port 19134 to 19132 for one IP. Here is what I did to make that happen:

I had to update this file to allow ip_forwarding

```
/proc/sys/net/ipv4/ip_forward
```

Setting it to _1_ lets the ip address forward.

Adding the address was as simple as:

```
sudo ip addr add 192.168.1.1/24 broadcast 192.168.1.255 dev <MYINTERFACEHERE>
```

Then it was time to route:

```
sudo iptables -A PREROUTING -t nat -d 192.168.1.1 -p tcp --dport 19132 -j DNAT --to 192.168.1.2:19134
sudo iptables -A PREROUTING -t nat -d 192.168.1.1 -p tcp --dport 19133 -j DNAT --to 192.168.1.2:19135
sudo iptables -A POSTROUTING -t nat -d 192.168.1.2 -p tcp --dport 19134 -j SNAT --to 192.168.1.1:19132
sudo iptables -A POSTROUTING -t nat -d 192.168.1.2 -p tcp --dport 19135 -j SNAT --to 192.168.1.1:19133
sudo iptables -A PREROUTING -t nat -d 192.168.1.1 -p udp --dport 19132 -j DNAT --to 192.168.1.2:19134
sudo iptables -A PREROUTING -t nat -d 192.168.1.1 -p udp --dport 19133 -j DNAT --to 192.168.1.2:19135
sudo iptables -A POSTROUTING -t nat -d 192.168.1.2 -p udp --dport 19134 -j SNAT --to 192.168.1.1:19132
sudo iptables -A POSTROUTING -t nat -d 192.168.1.2 -p udp --dport 19135 -j SNAT --to 192.168.1.1:19133
```
Wow after all that it showed up. Then later it went away, but I realized iPad's you can force the IP and port, XBox has no such luck with the security permisions set to disallow multiplayer, although local lan games pop up so it is survival only for the XBox then.

So I was able to find this information from asking around on Reddit. Some users were more friendly than others. One user tried to delete his help after I asked a question about it. The documentation I got from using the manual command:

```
man ip
```

Didn't have easy to follow walk throughs and without doing some long research, trial and error I would have been working on the problem for a while, but thankfully we live in the age of internet forums which can provide answers and explainations rather than the one way communication of a manual.

One lesson learned is to always document the process along the way. Who knows when someone might remove their information? I've setup a local wiki for myself so I was able to keep track of notes along the way.

## The MinKrift Server

One of my children likes to change words around sometimes when he finds the usual name boring, one time he called MineCraft MinKrift and the name stuck. When it came time to pick the name for the Survival server we named it: "MinKrift". 

The children and I have been watching the 6th season of the HermitCraft YouTube channel and one of the features was a merchant district. As a result tons of little shops were added to our server selling all sorts of things.

I created a small shop called "GnomeMart" this provoked jealousy among the merchants guild and led to its defacement. 

### Diamonds aren't forever

In the HermitCraft videos the shops sold things for diamonds. That price point is a bit above what my children wanted to pay. We dropped the price down to iron bars. Sometimes even posting a sign someone will "trade" for items not on the sign displaying exchange prices. Even then sales were not worth the effort.

Eventually I handed off the shop to my eldest.

I had found a village. It didn't take long for all the villagers to be eaten, and after the last one passed I built a rail from the merchant district to the village. I built a small base there and improved the farm. One of my children decided my storage chests were community property and all my diamonds got spent on diamond armor. Another child claimed a horse I tamed and placed in a stable. I left the farm to the third child who hadn't helped himself and went looking for greener pastures.

I found them to the south of town and setup another base. This time I put my "valuables" in a hidden chest. When the elderly pass away and you find money tucked in matteresses and coffee tins there is a reason.

Lessons learned:

* know your market
* protect your asset
* perform a SWAT analysis before opening a shop

### Redstone


#### Innovate where it is cheapest

On the survival server it causes time to mine resources, soon you will discover building takes time and effort and building a redstone contraption takes even longer. It is best I have found to build and experiment on the Creative server then once I have honed my design then build it in survival. This provides another lesson on the "one to throw away". Most people don't get things right the first time and if you get something working you will probably have several revisions before you get one you are happy with. In code this is much like the red, green, refactor. 

Build it. Refine it. Use it. 

#### Bedrock is not Java

So we watched a lot of Mumbo's videos and when we tried to play along at home and build some of his cool creations we hit a wall. Bedrock doesn't work the same. This didn't stop us from watching though. For aspiring inventors it is amazing to see the work. Also it secretly teaches them about electronics. 1 bit memory _flip flop_, AND, NAND, XOR gates, latches, it is quite interesting to see a world with creative tools like these. For the most part we can find replacements to achieve a similar effect, but it would be nice if things worked more closely.
