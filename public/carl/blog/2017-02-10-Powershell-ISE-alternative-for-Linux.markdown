---
layout: post
title:  "Powershell ISE alternative for Linux"
date: 2017-02-10T19:42:12-07:00  
categories: bash,vscode,powershell
---
I switch back and forth between Linux and Windows a lot. At work Windows is the only available OS, but at home I have a choice, when I am not gaming I choose Linux. I've been using bash a while, but with Windows it is nice to have some of the features of Powershell. One feature is the Powershell ISE. It features autocomplete and lets you have a scratch pad to do your work in. If you are on Linux and already use Visual Studio Code for other purposes why not consider using the Integrated Terminal?

_Ctrl-`_ Toggles the terminal on and off.

Additional commands available from [Microsoft](https://code.visualstudio.com/docs/editor/integrated-terminal)

This is one command you probably will want to bind:

    workbench.action.terminal.runSelectedText

File -> Preferences -> Keyboard Shortcuts

Edit your _keybindings.json_

    // Place your key bindings in this file to overwrite the defaults
    [
        {
            "key": "ctrl+shift+r", "command":"workbench.action.terminal.runSelectedText", "when": "editorFocus"
        }
    ]

When I press the Ctrl+Shift-R keyboard chord it runs in the terminal.