---
layout: post
title:  "Mermaids, Markdown, and UML"
date: 2018-09-27T23:04:11-07:00
scripts: 
 - mermaid
categories: pandoc,markdown,uml,mermaid,diagram
---
A friend at work turned me on to a product called mermaid. It has an option to integrate with pandoc.

See [https://github.com/mermaidjs/mermaid.cli](https://github.com/mermaidjs/mermaid.cli)

```
pandoc.exe -t html -F mermaid-filter.cmd -o test.html .\test.md
```

It can render uml diagrams from a markup language.

Here is an example of what it the code looks like:

```
~~~{.mermaid format=svg filename="Article.svg" loc="images"}
sequenceDiagram
    ClientApp->>Flask: Http POST [title,tags,author,content-type,created,excerpt,content] admin_article
    Flask->>DataFacade: AddArticle
    DataFacade->>SqlAlchemy: Create Article info
    SqlAlchemy->>SqlLite: Create Article info, Create Author info
    SqlLite-->>SqlAlchemy: Complete
    SqlAlchemy-->>DataFacade: Complete
    DataFacade->>SqlAlchemy: Get Article Info
    SqlAlchemy->>SqlLite: Get Article info
    SqlLite->>SqlAlchemy: Article info
    SqlAlchemy-->>DataFacade: Article info
    DataFacade-->>Flask: Article info
    Flask-->>ClientApp: Article info
    ClientApp->>ClientApp: Article post complete
~~~
```

Here is what it renders out to:
[diagram-1.svg]({{site.url}}/static/images/diagram-1.svg)
~~~mermaid
sequenceDiagram
    ClientApp->>Flask: Http POST [title,tags,author,content-type,created,excerpt,content] admin_article
    Flask->>DataFacade: AddArticle
    DataFacade->>SqlAlchemy: Create Article info
    SqlAlchemy->>SqlLite: Create Article info, Create Author info
    SqlLite-->>SqlAlchemy: Complete
    SqlAlchemy-->>DataFacade: Complete
    DataFacade->>SqlAlchemy: Get Article Info
    SqlAlchemy->>SqlLite: Get Article info
    SqlLite->>SqlAlchemy: Article info
    SqlAlchemy-->>DataFacade: Article info
    DataFacade-->>Flask: Article info
    Flask-->>ClientApp: Article info
    ClientApp->>ClientApp: Article post complete
~~~

As a fan of documentation this makes modeling your code fairly painless, it also makes the source of the diagrams diff-able since they are text files.
