---
layout: post
title:  "Little Rock Tech Fest"
date: 2016-10-25T19:40:00.002-07:00  
categories: tech-talk
---
Thursday and Friday were the Little Rock Tech Fest. Several of my co-workers and I got to attend. It was a two day technology conference.

[Here is the link.](http://www.lrtechfest.com/)

They had move it upstairs this year. Previously it was in the basement and did not charge for tickets. This year the early bird special was $75\. What do you get for your ticket price?

*   Two solid days of various speakers
*   Two provided lunches. Day one was noodles. Day two we had sandwiches.
*   A techie Tee Shirt

Here are some of the courses I attended:

*   Dr Iterate or: How I learned to Stop Worrying and Love Higher Order Functions - Timi Roberts
*   Why Azure Functions Should be Your New Best Friend - Bryan Soltis
*   From Zero to Production Docker in 10 Minutes - Jeff Hicks
*   Hololens: Demonstration and Workshop - Christopher Steven
*   Building performant web applications - Jonathan Fontanez
*   Building a Highly Scalable Web Environment on AWS - Joseph Yancey
*   Ready for Redis - Clayton Hall
*   My Journey to Continuous Delivery - Ryan Rousseau
*   Toolaholics Anonymous - A. Tate Barber
*   "This Code Sucks" and other tales of developer hubris - Russell Anderson
*   Can C# run in Docker - Don Schenck

If that seems a lot to take in, well it was.

Here are some URLs I made notes about:

[Fantasty Land](https://github.com/fantasyland/fantasy-land)

[kubernetes](http://kubernetes.io) Used for managing docker container

[Performance tools](http://perf.rocks/tools/)

[Building a 12 factor app](https://12factor.net)

[Mozilla Timeline](https://developer.mozilla.org/en-US/docs/Web/API/User_Timing_API)

[Utility for testing](https://www.getpostman.com)

[Overlay](https://www.vuforia.com)

[C# example of docker microservices](https://github.com/DonSchenck/SweetVirginia?files=1)

Meeting with other developers and having an exchange of ideas I believe pulls developers of the rut. It is easy to become fixed in ones own way of doing things and familiar technologies. Exposing your mind to new ideas and new ways of tackling problems provides opportunities for growth.