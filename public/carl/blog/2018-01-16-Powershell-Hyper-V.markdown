---
layout: post
title:  "Powershell Hyper-V"
date: 2018-01-16T21:04:11-07:00  
categories: docker,powershell
---
VirtualBox doesn’t like Hyper-V. The current docker for windows does.

With Powershell you can enable Hyper-V
```
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V –All
```
or disable it.
```
    Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
```
Don’t forget to reboot between.

If you want to pick each time on startup, you can use [these directions](https://www.hanselman.com/blog/SwitchEasilyBetweenVirtualBoxAndHyperVWithABCDEditBootEntryInWindows81.aspx) from Scott Hanselman.