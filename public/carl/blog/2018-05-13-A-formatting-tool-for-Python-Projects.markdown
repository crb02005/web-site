---
layout: post
title:  "A formatting tool for Python Projects"
date: 2018-05-13T17:11:59-07:00  
categories: python,yapf
---
I was looking at [Pika](https://pypi.org/project/pika/) a project that I've used in the past, but I looked at the project submit requirements under contributing and it required not only Test coverage, but also yapf. 

What is Yapf? It formats your code. It allows a consistent style for your project.
  
[https://pypi.org/project/yapf/](https://pypi.org/project/yapf/) 

I'm putting it in my editor instead of build process at the moment since most my projects are solo at the moment.

Atom package:
[https://atom.io/packages/python-yapf](https://atom.io/packages/python-yapf)
