---
layout: post
title:  "Holiday Loot"
date: 2016-12-26T18:33:00.002-07:00  
categories: loot
---
## Epic Loot Drops

*   [XBox One S Minecraft edition](http://www.xbox.com/en-US/xbox-one/consoles/minecraft-favorites-500gb) _for the boys, traded in a Drone for it_
*   [Raspberry PI 3 B](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) _from gift cards_
*   [Arduino Micro Pro](https://www.sparkfun.com/products/12640) _from gift cards_
*   Premium Nut Mix
*   Subway 10 gift card
*   [Teenage Engineering P-28 Robot](https://teenageengineering.com/store)
*   Essential oil atomizer
*   Rockstar Energy drinks

There are lots of potential projects for the items.

1.  Perhaps an [Xbox One app or game](http://www.xbox.com/en-us/Developers/id)
2.  Listener/Worker for the Raspberry PI 3 for the Paper Picker
3.  Virtualized Keyboard for the Micro potential working in tandum with the PI
4.  Some amazing songs made with the P-28

I've blocked off my schedule for a weekly blog post on Monday's and at least thirty minutes a day for working on music, and a day a week to work with the boys on Robots.