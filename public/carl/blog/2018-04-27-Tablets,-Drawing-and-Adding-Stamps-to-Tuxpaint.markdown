---
layout: post
title:  "Tablets, Drawing and Adding Stamps to Tuxpaint"
date: 2018-04-27T18:35:38-07:00  
categories: gimp,tuxpaint,imagemagick
---
A while back I was in the market for a drawing tablet. I didn't want to pay for the new iPad or the Wacom Cintiq, I found a deal on an older device the X200. I put Linux on it and a cheap SSD and I was off to the races. I installed GIMP, TuxPaint and Krita. 

My boys loved using it on our Texas trip. I wanted to add some custom stamps to TuxPaint. 

I looked up how to do it:

[http://tuxpaint.org/docs/html/EXTENDING.html](http://tuxpaint.org/docs/html/EXTENDING.html)

I added one manually and while the process worked it was far from quick. I found an image I wanted to add then made sure I made it have a transparent layer and put it in a directory.

I wanted to be able to add multiple images at once. So if I looked for a way to split up images. I found a GIMP plugin that was exactly what I wanted.

[http://registry.gimp.org/node/22177](http://registry.gimp.org/node/22177)

That divides the image up into other images.

Then I use imagemagick's convert to get rid of the background with this little shell script:

```
for fn in `ls *.png`; do
   convert $fn -fuzz 10% -transparent white "a_$fn";
done;
rm IM*.png;
```
