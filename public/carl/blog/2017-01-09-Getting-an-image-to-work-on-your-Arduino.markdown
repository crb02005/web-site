---
layout: post
title:  "Getting an image to work on your Arduino"
date: 2017-01-09T22:26:00.002-07:00  
categories: arduino,images,c,code,images,xbm
---
# XBM format

After a little more digging it seems Imagemagick supports the xbm format. Which looks like this:
```
    #define test_width 16
    #define test_height 16
    static char test_bits[] = {
      0xFB, 0xEC, 0x7F, 0xEF, 0x7F, 0xEF, 0x8F, 0xF7, 0x3F, 0xFB, 0x4F, 0xF8, 
      0x1F, 0xF8, 0x9F, 0xF4, 0x6F, 0xC2, 0x77, 0xC6, 0x67, 0xEE, 0x77, 0x9C, 
      0xBF, 0x7D, 0xBF, 0x1E, 0x00, 0x00, 0x00, 0x00, };
```
Looks like code right? Well it should. How did I get this to work?

First I just made a png using paint. Then I fired up my docker container:

_Ubuntu Imagemagick Dockerfile_ *repo redacted

I used a local volume and ran:

```
    convert test.png test.xbm
```

just like that I had the file. I opened it in Visual Studio Code and it looked just fine. A little copy and paste to the sketch and I was off to the races.

After a I'm able to get home to my Arduino I'll try and take a picture and put it up.

I also found something pretty cool online:

ESP8266 it is a dev board with built in WiFi for under $10.

*   Carl