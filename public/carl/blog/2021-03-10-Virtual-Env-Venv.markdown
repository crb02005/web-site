---
layout: post
title:  "Python Virtual Environments the old and the new"
date: 2021-3-10T20:45:00-06:00  
categories: python,venv,Virtualenv
---

In the past there was virtualenv, it worked, and you just used it if you didn't want an install singleton environment for python.
Now you have choices:

## [Virtualenv](https://virtualenv.pypa.io/en/latest/) 

* A package
* kitchen sink

## [venv](https://docs.python.org/3/library/venv.html)

* Core to Python 3.3+
* just the basics

More on the war of standards read on [StackOverflow](https://stackoverflow.com/questions/41573587/what-is-the-difference-between-venv-pyvenv-pyenv-virtualenv-virtualenvwrappe)
