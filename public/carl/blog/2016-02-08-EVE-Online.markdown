---
layout: post
title:  "EVE Online"
date: 2016-02-08T15:56:00-08:00  
categories: eve,gaming
---
I recently started playing a popular space game. EVE Online. 

If you've never heard of the game, I would suggest going to YouTube and look at a few videos. 

[Here is a trailer.](https://www.youtube.com/embed/uqoxRcP5kbo?feature=player_embedded)

I had joined a corp and they wanted to get a list of all their player's assets. I created a PowerShell Script specifically for that purpose. 

This one requires setting up your API key:

[Skill Script](https://gist.github.com/anonymous/0bc8bd3da21cd217fb70)

This one shouldn't need any special permissions:

[Sector Script](https://gist.github.com/anonymous/998410deed0c0efb7e51)

The script above lists the solar systems along with some interest data. Kills, Warps, and Corporation Ownership. 

Note: 

To view all just use:
$solarSystems.Values

Since it is PowerShell you can harness the Pipeline to find quite spaces and put them in a CSV file:

```
$solarSystems.Values | %{if($_.ShipJumps -lt 3 -and $_.ShipKills -eq 0){$_}} | export-csv "your file path.csv"
```

If you decide you want to try it sign up for a free trial below:


[Click on my Buddy link and sign up for a free trial](https://secure.eveonline.com/trial/?invc=967f06ed-54cd-4294-8db7-500eed4154b5&action=buddy)