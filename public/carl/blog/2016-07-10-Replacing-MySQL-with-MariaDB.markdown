---
layout: post
title:  "Replacing MySQL with MariaDB"
date: 2016-07-10T07:26:00.002-07:00  
categories: mysql,mariadb,sql
---

# Get MariaDB SQL Up and Running

If you read the previous article you will see that we installed the LAMP stack as is. We are going to be removing the MySQL component today and replacing it with MariaDB.  

## Remove MySQL

1.  Start the server in headless mode
    *   right click the context menu
    *   choose start headless
2.  SSH into it
3.  See what is installed  

        dpkg --get-selections | grep -v deinstall >
         installedFiles.txt

4.  Open the file  

        ...
            mysql-client-5.7                                install
            mysql-client-core-5.7                           install
            mysql-common                                    install
            mysql-server                                    install
            mysql-server-5.7                                install
            mysql-server-core-5.7                           install
            mysql-client-5.7                                install
            mysql-client-core-5.7                           install
            mysql-common                                    install
        ...

5.  Remove these with apt-get remove
    *   For the lazy and uncaring  

               sudo apt-get remove  mysql* 
               sudo apt-get autoremove

6.  remove /etc/mysql
    *   For the trusting and lazy (seriously be careful with any rm commands)  

                sudo rm -Rf /etc/mysql
                sudo rm -Rf /var/lib/mysql

    *   You might find more like:
        *   /var/lib/mysql-5.7
        *   /var/lib/mysql-files
        *   /var/lib/mysql-keyrings

When that doesn't work  

    sudo service mysql stop  #or mysqld
    sudo killall -9 mysql
    sudo killall -9 mysqld
    sudo apt-get remove --purge mysql-server mysql-client mysql-common
    sudo apt-get autoremove
    sudo apt-get autoclean
    sudo deluser mysql
    sudo rm -rf /var/lib/mysql
    sudo apt-get purge mysql-server-core-5.5
    sudo apt-get purge mysql-client-core-5.5
    sudo rm -rf /var/log/mysql
    sudo rm -rf /etc/mysql

http://stackoverflow.com/questions/10853004/removing-mysql-5-5-completely  

## Install MariaDB

    sudo apt-get install --force-reinstall true mariadb-server
    sudo mysql_secure_installation

## Grant localhost

https://dev.mysql.com/doc/refman/5.5/en/adding-users.html  

## Fix the bind address

File:  

    /etc/mysql/mariadb.conf.d/50-server.cnf

Allow any _do not_ do this in production or on an internet accessible machine:  

    bind-address:0.0.0.0

## VirtualBox

*   map a port to connect
*   if you want it locked to your host you could put a host IP of 127.0.0.1