---
layout: post
title:  "Using Docker provide access to books for Kindle"
date: 2017-02-06T21:23:28-07:00  
categories: httpd,kindle
---
Getting books to Kindle from Ubuntu Linux.

One would expect plugging in the Kindle to work out of the box on Ubuntu considering the special relationship Amazon has with Ubuntu. This is not the case in my experience.

Rather than solve this through the typical Linux make a driver option I thought I would attack this problem from a different angle. This Kindle has experimental Wifi so...

I thought perhaps I could run docker and server up my book collection and download them via the Kindle.

The first problem, some of my ebooks are not in a Kindle readable format.

With pandoc this was simple enough:

```
    ls *.epub | sed 's/.*/pandoc & -o &/g' | sed s/epub$/mobi/g
```

I just used the Docker file from my repo:

REDACTED

and changed the volume to the folder with my books.

next I changed the bound ip from local host to one on my LAN and allowed the Kindle to connect through the Firewall.

![View From Kindle]({{site.url}}/static/images/2efec30a-ece4-11e6-91e2-5ce0c5e937e8.jpg)

View From Kindle


Then it is a simple matter to download.

*   Carl Burks