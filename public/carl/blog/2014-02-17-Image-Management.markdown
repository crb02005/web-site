---
layout: post
title:  "Image Management"
date: 2014-02-17T15:34:00.001-08:00  
categories: linux
---
Depending on your camera most of the file names do not add value the real information is in the exif tags on the pictures.

Sometimes you just want to name the pictures by taken date: 

```
exiv2 -r '%Y%m%d-%H%M%S' rename *.JPG
```
