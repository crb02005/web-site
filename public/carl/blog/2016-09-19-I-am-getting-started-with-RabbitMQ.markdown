---
layout: post
title:  "I am getting started with RabbitMQ"
date: 2016-09-19T18:55:00.002-07:00  
categories: rabbitmq,docker
---
## Rabbit MQ

Getting a docker image first go to:

[https://hub.docker.com/_/rabbitmq/](https://hub.docker.com/_/rabbitmq/)

As I'm still learning docker I followed the directions, but I ended up combining the commands:

    docker run -d --hostname my-rabbit --name some-rabbit -e RABBITMQ_ERLANG_COOKIE='secretcookiegoeshere' -e RABBITMQ_DEFAULT_USER="usergoeshere" -e RABBITMQ_DEFAULT_PASS="passwordgoeshere" -p 5672:5672 -p 8080:15672  rabbitmq:3-management

I also mapped the queue port so I could call it from the host.

Then I followed the directions here: [rabbitmq](https://www.rabbitmq.com/tutorials/tutorial-one-python.html)

### Listener:

```
    import pika

    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',credentials=pika.PlainCredentials("usergoeshere","passwordgoeshere", erase_on_connect=False)))
    channel = connection.channel()
    channel.queue_declare(queue='hello')
    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)

    channel.basic_consume(callback,
                          queue='hello',
                          no_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
```

### Sender:

```
    import pika

    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',credentials=pika.PlainCredentials("usergoeshere","passwordgoeshere", erase_on_connect=False)))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    channel.basic_publish(exchange='',
                          routing_key='hello',
                          body='Hello World!')
    print(" [x] Sent 'Hello World!'")

    connection.close()
```

It worked.

I'm going to go into the rest of the tutorials on rabbitMQ's site and then start playing with Celery before implementing the previous post's query, download, and convert tasks.