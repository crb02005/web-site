---
layout: post
title:  "Reddit DL has been renamed to Pernicious Paper Picker"
date: 2016-11-10T17:43:00.002-07:00  
categories: paperpicker
---
Once again the desire to code has returned and I have taken the steps to do a refactor of my RedditDL codebase. I have renamed to to PerniciousPaperPicker. This name change should now reflect the choice to have more input sources than simple the Reddit PRAW. I have stumbled upon the:

[News API](https://newsapi.org)

This will allow me to query multiple sources of information.

Here are my current thoughts of development items for the Pernicious Paper Picker:

*   Testing
*   Implement multiple input sources
*   Clean up the text from sources
*   Smartly handle non-HTML content such as pictures and other media
*   User Interface for viewing and requesting content
*   Docker File
*   Dedicated hardware for each listener
*   Kiosk for the content

Carl Burks
