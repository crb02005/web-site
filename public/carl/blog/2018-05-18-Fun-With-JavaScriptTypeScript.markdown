---
layout: post
title:  "Fun With JavaScript/TypeScript"
date: 2018-05-18T22:20:15-07:00  
categories: typescript,javascript
---
I started a repo to keep my talks:

[https://gitlab.com/crb02005/tech-talks](https://gitlab.com/crb02005/tech-talks)

In the FunWithES2018andTS folder I've got a small project which has TypeScript.

I'm targeting ES2018 with TypeScript so I can do some interesting things:

*    the for of native syntax
*   typescript template strings
*   iterators
*   range feature clone
