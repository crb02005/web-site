---
layout: post
title: "Where is the mouse? (everyone wants to know)"
date: 2019-05-22T07:50:06-05:00
categories: linux, ubuntu
---

How to get mouse coordinates from Linux.

Install xdotool:

```
sudo apt-get install xdotool
```

This will do a rolling track:
```
watch xdotool getmouselocation
```

Get Mouse X
```
xdotool getmouselocation | cut -f 1 -d ' ' | cut -f 2 -d ':'
```

Get Mouse Y
```
xdotool getmouselocation | cut -f 2 -d ' ' | cut -f 2 -d ':'
```
