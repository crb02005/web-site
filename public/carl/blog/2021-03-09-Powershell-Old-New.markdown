---
layout: post
title:  "Powershell the old and the new"
date: 2021-3-9T20:47:00-06:00  
categories: windows,powershell,signing
---

You are on your own with everything, and none of this is a recomendation, best practice or how to, it is simply notes on this well traveled process.

Scott does an amazing job getting this working, but I've got my own notes on this, so check him out before reading. His article might solve your needs:

[Signing Powershell Scripts by Hanselman](https://www.hanselman.com/blog/signing-powershell-scripts)

The location has changed since the article. 

## Finding the makecert util
```powershell
cd C:\
ls -Recurse makecert.exe
```


Let's say you find it at:
```powershell
C:\Program Files (x86)\Windows Kits\10\bin\10.0.17763.0\x64
```

This is great except: [makecert is deprecated](https://docs.microsoft.com/en-us/windows/win32/seccrypto/makecert)

# Make Cert - The old way

You might need to add this to Admin and regular
```powershell
$env:Path +=";C:\Program Files (x86)\Windows Kits\10\bin\10.0.17763.0\x64"
```

Admin
```powershell
makecert -n "CN=PowerShell Local Certificate Root" -a sha1 -eku 1.3.6.1.5.5.7.3.3 -r -sv root.pvk root.cer -ss Root -sr localMachine
```

You now have a root cert
```powershell
makecert -pe -n "CN=PowerShell User" -ss MY -a sha1 -eku 1.3.6.1.5.5.7.3.3 -iv root.pvk -ic root.cer
```

Now you can make a personal cert
```powershell
makecert -pe -n "CN=PowerShell User" -ss MY -a sha1 -eku 1.3.6.1.5.5.7.3.3 -iv root.pvk -ic root.cer
```

Now time to validate!
```powershell
Get-ChildItem cert:\CurrentUser\My -codesign
```

This should return one entry, if it returns multiple the next line would might need to be changed, if you have multiple it might mean you've been playing with certs already.


```powershell
Set-AuthenticodeSignature works.ps1 @(Get-ChildItem cert:\CurrentUser\My -codesign)[0]
```

Signed but not trusted

```powershell
./works.ps1
```

Will give you:

```
works.ps1 is published by CN=PowerShell User and is not trusted on   your system. Only run scripts from trusted publishers.                                                                  [V] Never run  [D] Do not run  [R] Run once  [A] Always run  [?] Help (default is "D"):  
```

Pressing A gets your cert to be added as a trusted publisher which helps you, but to help others you need to export it.

```powershell
certmgr
```

A dialog opens up. Navigate to:

Trusted Root Certification Authorities -> Certificates -> Powershell Local Certificate Root

Right Click and All Tasks -> Export -> DER 

Consider:
```
c:\PS_Public.cer
```

You can then hand out your cert for people to import. 


## Importing a trusted cert on a new machine

Admin
```powershell
certmgr
```

Find the Trusted Root Certification Authorities -> Certificates

Right click and choose import.

Bam! other people can run your script.

It just takes them being willing to install your cert. That is a pretty high barrier to entry.

## New Self Signed - The New way

[new-selfsignedcertificate](https://docs.microsoft.com/en-us/powershell/module/pkiclient/new-selfsignedcertificate?view=win10-ps)

It contains many examples.

-KeyUsage is important

ADMIN
```powershell
New-SelfSignedCertificate -CertStoreLocation cert:\currentuser\my -Subject "CN=PowerShell Local Certificate Root" -KeyAlgorithm RSA -KeyLength 2048 -Provider "Microsoft Enhanced RSA and AES Cryptographic Provider" -KeyExportPolicy Exportable -KeyUsage DigitalSignature -Type CodeSigningCert
```


Trying to sign with the result will not work and will give you an unknown error.

_using 1 because 0 was the one we made with makecert_

```powershell
Set-AuthenticodeSignature .\works-new-way.ps1 @(Get-ChildItem cert:\CurrentUser\My -CodeSigningCert)[1]
```

```
Status:UnknownError
```

How do you fix it?


Exporting your cert to import it into the right places

```powershell
$passwd = ConvertTo-SecureString -String "YOURPASSWORDHERE" -Force -AsPlainText 
@(Get-ChildItem cert:\CurrentUser\My -CodeSigningCert)[1] | Export-PfxCertificate -FilePath "new-way-cert.pfx" -password $passwd 
```

_Don't use YOURPASSWORDHERE_


Validate the cert:

```powershell
certutil.exe new-way-cert.pfx
```

Type in the password.

Signature should show the cert is good.

Lets import it:


ADMIN
```powershell
@("cert:\LocalMachine\My","cert:\LocalMachine\Root","cert:\LocalMachine\TrustedPublisher")|%{
  $certStoreLocation = $_
  Import-PfxCertificate -FilePath "./new-way-cert.pfx" -CertStoreLocation $certStoreLocation -Password $passwd 
}
```
if it doesn't work, make sure you are:

1. Admin
2. setting the $passwd variable correctly in admin mode

Finally:

```powershell
Get-AuthenticodeSignature .\works-new-way.ps1 
```

Valid.

Change your powershell file and don't sign it again.
```powershell
Get-AuthenticodeSignature .\works-new-way.ps1 
```

HashMismatch

Try to run it:

works-new-way.ps1 cannot be loaded. The
contents of file C:\Users\crb02\Documents\source\windows-certs\works-new-way.ps1 might have been changed by an
unauthorized user or process, because the hash of the file does not match the hash stored in the digital signature.
The script cannot run on the specified system. For more information, run Get-Help about_Signing..

You can revert and it should run.

```powershell
Get-ChildItem *.ps1 | Set-AuthenticodeSignature -Certificate @(Get-ChildItem cert:\CurrentUser\My -CodeSigningCert)[1]
```

Don't run self signed in production.

### What is the experience like?

Write some code, instead of hitting F5 you sign, and repeat. Sure you can make a custom vs code task, but the out of the box experience is lacking.

Functionality, Usability, Security where do you want the dot?

### Final Thoughts

The old way works if you can find the tool.
The new way works too.

Security makes things safer, and often makes the dev process more painful, but decreases the risk.
