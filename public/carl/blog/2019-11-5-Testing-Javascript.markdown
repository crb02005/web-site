---
layout: post
title:  "Testing Javascript"
date: 2019-11-5T18:15:00-06:00  
categories: javascript
---

Next month at our Central Arkansas Javascript group we plan on talking about Testing. I'll be updating my

GITHUB LINK REDACTED

repo and adding a talk about testing.

A survey of testing.

Why Test? You want to make sure your code works. You want to make sure the product owner is happy with the product. You want to make sure you didn't break something with your new code. You want to let others in your codebase and don't want them to break the world. You care about quality. All these reasons are good, but quality of life is the most important reason. Your codebase is where you spend your time. You want to make your job as easy as possible and tests let you do that. Tests improve development time, you can write code and not fully spin up the entire app if you want to test one thing. With the right test you only have to test part of your code.

## What kind of tests are there?

* Unit
* Integration
* System
* Sanity
* Smoke
* Interface
* Regression
* Beta Testing
* Coded UI

### Unit Tests

Individual units or components are tested. 

In jest you could do something like this:

* [Jest React Tutorial - https://jestjs.io/docs/en/tutorial-react](https://jestjs.io/docs/en/tutorial-react)

In C# you could do something like this:

* [C# Unit Test Basics - https://docs.microsoft.com/en-us/visualstudio/test/unit-test-basics?view=vs-2019](https://docs.microsoft.com/en-us/visualstudio/test/unit-test-basics?view=vs-2019)
* [Walkthrough for creating and running unit tests - https://docs.microsoft.com/en-us/visualstudio/test/walkthrough-creating-and-running-unit-tests-for-managed-code?view=vs-2019](https://docs.microsoft.com/en-us/visualstudio/test/walkthrough-creating-and-running-unit-tests-for-managed-code?view=vs-2019)

### Integration

You are combining things to test. You are finding fault with the interactions between the units.

### System

You are testing the whole application, not just a couple of combinations. You are evaluating functional and non-functional requirements.

### Sanity

Sanity Testing is the "show stopper" initial regression testing. These are the mission critical parts of the app you test before doing a full smoke test. If X doesn't work I shouldn't test any further.

### Smoke

You powered on the device and didn't let out the magic blue smoke. When I was attending my Ham radio's tech nights they often talked about letting the smoke out. If you wired up something wrong the device would destroy a component and smoke would come out and you'd have some repairs to do. In the software world you are testing the major functionality of your application.

### Interface

This is a type of integration testing which validates the contract you've defined. 

### Regression

With this type of testing you are confirming changings to the application doesn't break the existing features.

### Beta Testing
Handing the product out to a small population of users to try out the new features.

### Coded UI
Tests which operate on the user interface and exercise the application the same way the user would. 

## Red, Green, Refactor

Basically this is write a failing test. 

Write code to make the test pass.

Rewrite the code to make it better.

How do you check if your code works?

## TDD

Test Driven Development. See Red, Green, Refactor.

You can't write untestable code if you right your test first.

## Shift Left

You move the testing to the front rather than the end. "Test early and often" - Larry Smith, Dr. Dobbs  "Shift-Left Testing".

## Jest

"Jest is a delightful JavaScript Testing Framework with a focus on simplicity."

[https://jestjs.io/](https://jestjs.io/)

### Mocks

Image: There will be blood 

Text: There will be Mocks

[https://jestjs.io/docs/en/mock-functions](https://jestjs.io/docs/en/mock-functions)

[https://jestjs.io/docs/en/timer-mocks](https://jestjs.io/docs/en/timer-mocks)

[https://jestjs.io/docs/en/manual-mocks](https://jestjs.io/docs/en/manual-mocks)

[https://jestjs.io/docs/en/es6-class-mocks](https://jestjs.io/docs/en/es6-class-mocks)


## Enzyme

"Enzyme is a JavaScript Testing utility for React that makes it easier to test your React Components' output. You can also manipulate, traverse, and in some ways simulate runtime given the output."

[https://airbnb.io/enzyme/](https://airbnb.io/enzyme/)


### Shallow

Renders the component and only direct descendents.

### Mount

Renders the component and all descendents.


## Other things to be aware of

"Jasmine is a behavior-driven development framework for testing JavaScript code. It does not depend on any other JavaScript frameworks. It does not require a DOM. And it has a clean, obvious syntax so that you can easily write tests."

[https://jasmine.github.io/](https://jasmine.github.io/)


## Image Macros! Now in Text!

Image: Borat thumbs up!

Text-Top: The tests were broken

Text-Bottom: So I disabled them

---

Image: Roll Safe

Text-Top: Your code can't fail unit tests

Text-Bottom: If you don't make any unit tests

---

Image: Grumpy Cat

Text-Top: No Tests?

Text-Bottom: No Code Review

---


