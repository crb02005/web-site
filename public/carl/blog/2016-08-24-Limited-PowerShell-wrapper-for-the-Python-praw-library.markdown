---
layout: post
title:  "Limited PowerShell wrapper for the Python praw library"
date: 2016-08-24T17:38:00.002-07:00  
categories: praw,reddit
---
Wrapping the python praw library with a command line interface then wrapping that with PowerShell was a rather simple task. Like Ian says in the movie Jurassic Park:

"Your scientists were so preoccupied with whether they could, they didn't stop to think if they should."

Sometimes it is fun just to code something. It gives you time to experiment with new technologies. I had not played with the praw library before and this gave me a chance to tinker with it.

I've got a bigger project in mind, but playing with the components gives me a chance to figure out problems before working on the large scale project. Here is what I am thinking:

Worker Tasks

*   querying reddit.com/r/news and posting to message queue
*   download new links from message queue
*   converting html page to PDF
*   processing keywords from page and adding indices

I went ahead and added two files to my [PowerShellFunctions Repo](https://gitlab.com/crb02005/power-shell-functions)

Both are located under:

PS-Python-Functions

_lswpraw.py_ a limited shell wrapper around the Python Reddit Api Wrapper

_lspraw.ps1_ a PowerShell function calling the Python file lswpraw.py which in turn calls the Python Reddit Wrapper

It would be simple enough to call the Get-RedditSubmissions and for each on linksubmissions and calling pandoc to download the html file as a pdf, but we want something a little more complicated than that....

Perhaps some docker, rabbitMQ, celery, python scripts, and natural language parsing.