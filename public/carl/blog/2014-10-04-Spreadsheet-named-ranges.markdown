---
layout: post
title:  "Spreadsheet named ranges"
date: 2014-10-04T09:19:00.001-07:00  
categories: 
---
A friend of mine had never seen a named range in a spreadsheet so I thought I would do a little public service announcement. Named ranges are a great feature I like to use in my spreadsheets to make reading and writing formulas easier. Instead of reference a cell by the column letter and the row number a named range lets you assign a name to the cell(s).  You could just have looked them up on Google and found the Wikipedia article:  
[http://en.wikipedia.org/wiki/Spreadsheet#Named_cells](http://en.wikipedia.org/wiki/Spreadsheet#Named_cells)  

Google Docs supports this feature:  
[https://support.google.com/docs/answer/63175?hl=en](https://support.google.com/docs/answer/63175?hl=en)  

Apache Open Office supports this feature:  
[https://wiki.openoffice.org/wiki/Documentation/OOo3_User_Guides/Calc_Guide/Associating_a_range_to_a_name](https://wiki.openoffice.org/wiki/Documentation/OOo3_User_Guides/Calc_Guide/Associating_a_range_to_a_name)  

Lotus 1-2-3 which recently IBM ended support for the product, was first for named ranges.  

A lesser known spreadsheet program called Excel also supports them.