---
layout: post
title:  "Random Tables: Update"
date: 2015-09-02T18:32:00-07:00  
categories: gaming,pen and paper,gurps
---
After rereading the Steve Jackson Games online policy located here: I am going to have to consider how I will be able to provide content from GURPS. The first part which prevents me from using their content is right here "...Post (or make available for download) forms, charts, tables and text from one of your games..."* The short answer says no, after talking to their director via email it seemed to allow ones that had already made available online, but a clear list of available content does not seem apparent. The second problem with GURPS is the issue with mobile development: "...We currently do not allow "apps" for mobile devices to be created using our content or trademarks..."* this is problematic as the Windows Universal platform is both a mobile, and standard PC program. The final problem which I believe will be universally problematic for development of any third party data packs is: ["and is released for free distribution, and not for resale"](http://www.sjgames.com/general/online_policy.html#gameaids) this precludes me from using: [GPL3](http://www.gnu.org/licenses/gpl-faq.html#DoesTheGPLAllowMoney) *http://www.sjgames.com/general/online_policy.html The simplest solution is to hold off on adding third party content and continue with my in house data pack, but I think putting a separate GitHub repo with each data pack licensed under:

https://creativecommons.org/licenses/by-nc-nd/3.0/

would satisfy my third problem.

As to my second problem I could restrict the mobile version from loading specific data packs from JSON.

Then I am left to my first problem, which I might inquire again and see if they keep a list of web published table content.