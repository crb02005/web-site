---
layout: post
title:  "Calendar In Powershell"
date: 2016-03-12T18:11:00.003-08:00  
categories: powershell
---
*EDIT switched the source control provider 10/30/2020

I've added a new Repo on gitlab for making a calendar in PowerShell, much like the Linux command _cal_. I have omitted multi-language support and have opted to only implement the core functionality of displaying a calendar rather than also including the date of Easter.

[https://gitlab.com/crb02005/PowerShellFunctions/blob/master/calendar.ps1](https://gitlab.com/crb02005/PowerShellFunctions/blob/master/calendar.ps1)