---
layout: post
title:  "JavaScript Meetup"
date: 2018-05-16T17:44:58-07:00  
categories: javascript,caj
---
Recently I signed up for a JavaScript user group and the original organizer had life events take him to another part of the world, so the call was open for organizers. I pick up the mantle at least for now to get the group up and running. Apparently meetup charges organizers. I guess they have to make money some way.

Here is the link: [https://www.meetup.com/javascript-conway/events/250811140/](https://www.meetup.com/javascript-conway/events/250811140/)
