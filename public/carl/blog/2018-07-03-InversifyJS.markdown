---
layout: post
title:  "InversifyJS"
date: 2018-07-03T21:40:14-07:00  
categories: javascript,ioc,typescript
---
I like to play around with new libraries in my spare time, and I was looking for something to handle IoC in JavaScript without writing my own. I checked with the interwebs and they suggested [InversifyJS](http://inversify.io/). So I followed the directions and installed it with node. I got it up and running with a minimum of fuss.


My tsconfig.json has two very important "compilerOptions":

```
        "experimentalDecorators":true,
        "emitDecoratorMetadata": true
```


These get rid of the node warnings and let the decorators work.

In my project I was able to inject a data provider so I could switch between the node "fs" to read local json files and an $.ajax request to a webservice for data.
