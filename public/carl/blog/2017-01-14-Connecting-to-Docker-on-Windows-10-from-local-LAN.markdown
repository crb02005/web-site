---
layout: post
title:  "Connecting to Docker on Windows 10 from local LAN"
date: 2017-01-14T12:37:00.002-07:00  
categories: docker,firewall
---
[Information about Firewalls](https://technet.microsoft.com/en-us/library/dd421709(v=ws.10).aspx)

Create an inbound firewall rule to allow traffic, probably only want it to work on private networks.

Hit the Windows _meta_ Key

Type Windows Firewall

Click Advanced Settings

Click Inbound Rules

Under actions click New Rule

Pick Port

Follow wizard

    Enable-NetFirewallRule -DisplayName "Custom HTTP Allow"

Run your Dockercompose

    Docker-compose up

Turn off the rule when you are done:

    Disable-NetFirewallRule -DisplayName "Custom HTTP Allow"

EDIT: -name was changed to DisplayName