---
layout: post
title:  "Using a CDN"
date: 2021-3-17T20:45:00-06:00  
categories: javascript
---

When you rely on someone else's hardware you are at risk. They might not serve what you want them to server, but how can you mitigate the risk?

With a standard javascript include you might be surprised if your CDN changes the payload. This is possible with a standard script tag:

```html
<script src="https://unpkg.com/mermaid@8.8.0/dist/mermaid.min.js"><script>
```

But you can make sure the file matches what it should with an integrity check such as:

```html
<script src="https://unpkg.com/mermaid@8.8.0/dist/mermaid.min.js" integrity="sha384-OBYc88+eQm2E+Vw9J6jK9Z9rY4rcY+Mq5KlRpOzFTiHZV0Misu7O5AKmBOWGKk8j" crossorigin="anonymous"></script>
```

But how do you get the hash?

[SRIHash](https://www.srihash.org/#app)

You can also do it manually as listed on their website

```bash
openssl dgst -sha384 -binary mermaid.min.js | openssl base64 -A
```

[Mozilla lists another option](https://developer.mozilla.org/en-US/docs/Web/Security/Subresource_Integrity)

```bash
shasum -b -a 384 FILENAME.js | awk '{ print $1 }' | xxd -r -p | base64
```

Or write your own and with do it with powershell 
```powershell
$hash = (Get-FileHash -Algorithm SHA384  .\mermaid.min.js).Hash
$bytes=[byte[]]::new($hash.Length/2)
0..($bytes.Length-1)|%{
    $bytes[$_]= [Convert]::ToByte($hash.Substring(($_*2),2),16)
}
[Convert]::ToBase64String($bytes)
```

Get-FileHash gives you a hex string and you have to convert it. First we make a byte array the size of half the string length, then we use the powershell range function to create a pipeline of numbers that we use the for each then we assign into the byte array by converting two characters into a byte. Couldn't be simpler right?

Then you have to have a browser that is smart enough to respect the integrity flag. The Mozilla link shows compatibility.

Where did SRI come from: [https://www.w3.org/TR/SRI/](https://www.w3.org/TR/SRI/)

Is this a new idea? No, in 2005 Gervase "Gerv" Markham had an idea for [link fingerprints](http://www.gerv.net/security/link-fingerprints/). [older link for link fingerprints](http://blog.gerv.net/2005/03/link_fingerprin_1/)

Nothing new is under the sun, the ZMODEM file transfer protocol, by Chuck Forsberg, had a CRC checksum when downloading. While it was not using cryptography the idea is similar.

[Want to learn more about Chuck?](https://archive.org/details/20030530-bbs-forsberg)

There are a lot of BBS related videos on that site, and preserving history is important to future design and development.

Back to the checksum, in this case we are looking at SHA-2 variant, sha384. SHA-1 came from the NSA, an if you know your movies, Sneakers would tell you, "Too many secrets". In a future post I might cover more details about hashs.

