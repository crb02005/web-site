---
layout: post
title:  "Ham Radio"
date: 2013-09-19T20:07:00.001-07:00  
categories: 
---
Made contact with the Faulkner County Amateur Radio Club at Ecofest. I spoke with one of their members who pointed out several websites to get started. I order a small portable and while I am waiting for it to be shipped I've been studying for the exam. Hopefully before it arrives I will have my technician license. I've thought a lot about getting into Amateur radio, and finally decided to take the plunge. I'm really excited. I've been boring my wife with bands, frequencies, and technical schematics while I am trying to get ready for the test.
