---
layout: post
title:  "Continuing the Downloader project"
date: 2016-09-30T17:38:00.002-07:00  
categories: python
---

Playing with mysql for python2.7 on windows 64bit is awful. You will probably get the following error:

Cannot open include file: 'config-win.h'

After you search online you'll come across the [stackoverflow article](http://stackoverflow.com/questions/1972259/cannot-open-include-file-config-win-h-no-such-file-or-directory-while-inst) which encourages manual manipulation of the setup_windows.py. Then you might install the MySQL Connector for C for some reason. It didn't help me. Also there is a MySQL Connector for Python which also doesn't seem to work. You'll find others suggesting not to use the 64 bit version and use the 32 bit version instead. I kept looking. You can find some sites linking to edu sites with binaries. I'm not really one to be keen on installing secret binaries. Someone else suggests running vcvarsall.bat from command line it is located somewhere on your machine if you've got visual studio. After no readily apparent solution presented itself, and not having a hard requirement on MySQL I simply pivoted and switched technologies. It was a simple change to my SQLAlchemy python script to change to a SQLite database and I was off to the races. Previously I had used:

[Heidi SQL](http://www.heidisql.com/)

To connect to MySQL and MariaDB and it worked well enough.

I found [DB Browser For SQLite](http://sqlitebrowser.org/) to be good enough to view my database on SQLite.

I've switched [computers](https://www.amazon.com/Acer-Aspire-15-6-inch-i7-5500U-Windows/dp/B019KPKPYG) this week so I haven't updated this project as frequently as I would like, but I hope to put a project up on GitHub sometime soon. Currently I'm playing with [SQLAlchemy](http://www.sqlalchemy.org/). Having written my own data layer ORM thing in PHP.*Link redacted* I thought I would see what has already been coded. I wouldn't recommend SuperGreatDataLayer, but I would say SQLAlchemy is great from the experience I have had with it thus far. As I said earlier it was a simple change to Python script to switch SQL technologies with SQLAlchemy. Just look at [create_engine](http://docs.sqlalchemy.org/en/latest/core/engines.html)

Once I've got the persistence taken care of it will be time to connect things up with the message queue. If someone has a spare copy of [Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions](https://www.amazon.com/o/asin/0321200683/ref=nosim/enterpriseint-20) I'd like to give it a read. I might drop by the local library tomorrow and see if they can request it.