---
layout: post
title:  "How to renumber BASIC programs"
date: 2019-10-3T18:15:00-06:00  
categories: javascript, basic
---

Do your programs look like this:

```
10 ?"COOL PROGRAM CODE"
11 ?"HACKY LOGIC YOU FORGOT ABOUT AND HAD TO WEDGE IN"
15 ?"REST OF YOUR CODE"
```

Well here is a tool for you:

[https://github.com/tilleul/basic_renumber/blob/master/renumber.js](https://github.com/tilleul/basic_renumber/blob/master/renumber.js)

It lets you run it with node and supply the bas file as a parameter and you get this:

```
10 ?"COOL PROGRAM CODE"
20 ?"HACKY LOGIC YOU FORGOT ABOUT AND HAD TO WEDGE IN"
30 ?"REST OF YOUR CODE"
```
