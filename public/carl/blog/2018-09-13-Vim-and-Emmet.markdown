---
layout: post
title:  "Vim and Emmet"
date: 2018-09-13T12:16:58-07:00  
categories: vscode,vim
---
I was playing around with vim the other day and I really liked the powerful command ability to insert several elements consider:

Vim Code
```
3i<li>
</li>
```

Output Vim Code
```
<li>
</li>
<li>
</li>
<li>
</li>
```

This baked in support is quite powerful, but what does the modern development analog? 
[Emmet](https://code.visualstudio.com/blogs/2017/08/07/emmet-2.0)  Actions from visual studio code can do something really similar if not more powerful.

Consider the following [example](https://docs.emmet.io/abbreviations/) from Emmet's documentation.

I'm not sure if it would be worth ditching the vim extension to vs code yet, but it is quiet promising.

## Want to learn more?

[Emmet Syntax](https://docs.emmet.io/abbreviations/syntax/)

[Vim vscode extension](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim)

[Vim manual page](http://linuxcommand.org/lc3_man_pages/vim1.html)
