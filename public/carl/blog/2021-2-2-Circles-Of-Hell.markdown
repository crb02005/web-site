---
layout: post
title:  "Circles of Hell"
date: 2021-1-28T20:30:00-06:00  
categories: rant
---

A brief note on history, Bill Moggridge, a british designer was credited with designing the first laptop.

It was a 320x200-pixel weighing in at 10lbs, and made it to space.

I thought it was fitting to give a brief nod to this laptop and its designer. He was a known for his design work, and is the antithesis of what we are discussing. It is now time to learn about Dante before we delve into the circles of hell.

Dante,a florentine poet, who is probably best known for his _Inferno_ defined several circles:

* Limbo
* Lust
* Gluttony
* Greed
* Wrath
* Heresy
* Violence
* Fraud
* Treachery

For the purposes of technology there is a circle of hell for:

* OEM's who don't give you a choice of OS's
* Gaming laptop manufacturers who make the laptop burn your lap
* People who add extra plastic to make USB not Universal _looking at Apple Keyboards from the 2000s_
* People who lock laptop wireless cards with a whitelist _looking at you Lenovo_
* People who make function keys default to Media Keys _looking at most laptop manufacturers and resellers_
* People who lock out BIOS/UEFI settings for machines that they sell to people _looking at a lot of OEMs_
* People who default Media Keys and Lock the settings, lock the BIOS/UEFI Remove the Mobility settings for changing it, and provide no option on the keyboard for changing it _looking at you Gateway_
