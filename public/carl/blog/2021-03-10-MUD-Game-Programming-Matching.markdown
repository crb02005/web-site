---
layout: post
title:  "MUD Game Programming - Matching"
date: 2021-3-10T20:45:00-06:00  
categories: sqlite,mud
---

I have a book that has served as reference, inspiration for some time. It was written by Ron Penton. You should totally find a copy and buy it. It has lots of great ideas, and examples.

 On chapter 8 there is a section about Entities. It goes into detail about pattern matching, full and partial and I thought, well why not do it in "SQL".

Since I presented to the Little Rock Dot Net User Group a few years back about SQLite I have been a fan. My first thought was full match can be easily solved with a where = clause.

Consider the following format for an entity table:

* id-text _really a guid_
* name-text _collation NOCASE_

This is simple enough example for our problem.

## Full Match

```sqlite
select
    id,
    name
from
    entity
where name = ?
```

That was easy enough right?

## Partial match

Well this is a bit more work, but not difficult.

```sqlite
select
    id,
    name,
    name=? full_match,
    name like '%'||replace(?,' ','%')||'%' is_match
from
    entity
where
    name = ? or name like '%'||replace(?,' ','%')||'%'
order by
    name=? desc, name like '%'||replace(?,' ','%')||'%' desc
```

What we are are doing is finding full matches first, then we are taking our search parameter and replacing spaces with the SQLite wild card match and prepending and appending with the same wildcard.

Lets see how a few searches work.

```
entity: rusty sword	
search: rusty sword
full_match: 1
partial_like_expression: %rusty%sword%
partial_match: 1

entity: rusty sword
search: ru sw
full_match:0
partial_like_expression: %ru%sw%
partial_match:1

entity:jeweled sword
search:sw
full_match:0
partial_like_expression:%sw%
partial_match:1

entity: rusty sword
search:sw
full_match:0
partial_like_expression:%sw%
partial_match:1

entity:jeweled sword
search:rusty sword
full_match:0
partial_like_expression:%rusty%sword%
partial_match:0

entity:jeweled sword
search:ru sw
full_match:0
partial_like_expression:%ru%sw%
partial_match:0
```

It should be fairly simple to find items/players/etc with this.

