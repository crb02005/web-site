---
layout: post
title:  "Dice Roller"
date: 2017-11-16T23:05:51-07:00  
categories: electron,vue
---
Electron

I always like to start out with a dice roller when I'm playing with new tech.

Some new techs I've been playing with:

*   [Electron](https://electronjs.org/)
*   [WebPackJS](https://webpack.js.org/)

I tried this one with my comparison of Angular2, React, and Vue * [VueJS](https://vuejs.org/)

I use this one in quite a few projects:

*   [TypeScript](https://www.typescriptlang.org/)


![Dice Roller]({{site.url}}/static/images/DiceRoller.png)

Dice Roller




![Dice Roller - Advanced]({{site.url}}/static/images/DiceRoller_2.png)

Dice Roller - Advanced

I started here and everything worked out just fine: [Electron-WebPack](https://github.com/electron-userland/electron-webpack)

Well that isn't entirely true. First I tried Electron and then manually adding everything, and using Gulp rather than Webpack but soon I realized it was folly. I've got that saved off, but I don't know if I'll source control it.

Next up I'm going to focus on getting it to package properly for Apple, Windows, and Linux.