---
layout: post
title:  "Xmonad - Random Background at Interval"
date: 2014-01-25T06:51:00.003-08:00  
categories: xmonad,linux
---
Make a file randombackgroundatinterval:

```
#!/bin/bash
while x=0;do find $1 | grep 'jpg\|png' | shuf | head -n 1 | xargs -i xloadimage -border black -onroot -fullscreen '{}'; sleep $2; done
```

run it with the top directory for images and how often you want them to change.
example:

```
./randombackgroundatinterval /home/<YOUR USER NAME>/Pictures 3
```

Would pick a random jpg or png from the /home/<YOUR USER NAME>/Pictures directory, and its subs and display a new one every three seconds.
