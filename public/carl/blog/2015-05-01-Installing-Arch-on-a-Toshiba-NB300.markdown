---
layout: post
title:  "Installing Arch on a Toshiba NB300"
date: 2015-05-01T12:58:00-07:00  
categories: arch,toshiba
---  
A few considerations:  

Backed up any files you want to keep?  
Power connected?  
Access to Internet?  

Start reading here:  

https://wiki.archlinux.org/index.php/beginners%27_guide  
https://wiki.archlinux.org/index.php/laptop  
https://wiki.archlinux.org/index.php/Security  

Have a solid state drive? (I hope you have a really small star screw driver.)  

https://wiki.archlinux.org/index.php/Solid_State_Drives  

<pre>/proc/sys/vm/swappiness</pre>

Find which parts you have and read up on them.  

http://www.toshiba.eu/discontinued-products/toshiba-nb300-100/  

I've found it  is helpful to create a folder with all my system information as some manufacturers remove documentation.  

Your going to need some method of getting the OS on the computer.  

USB/SD Drive or Optical are common options, with a Net Book unless you have an external optical drive the USB/SD Drive are probably the best options.  

https://wiki.archlinux.org/index.php/beginners%27_guide#USB_and_optical_drives  

I opted for the USB Drive option.  This destroys the contents of the drive and you might have to take some steps to start using it for regular storage again. Since I was writing the system from Windows I used the USBWriter program.  

https://wiki.archlinux.org/index.php/USB_flash_installation_media#Using_USBwriter