---
layout: post
title: "A new start"
date: 2019-4-13T13:15:06-06:00
categories: ubuntu
---

I had been dual booting for a while and I wanted to start over with a single OS installed on my laptop.

I figured I'd stick with Ubuntu until I found a compeling reason to swap.

I already had a thumb drive with 18.10 so I didn't need to set that up. I backed up my files to a USB drive and rebooted with the thumbdrive.

The computer has two drives in it an M.2 and an SSD. The M.2 is only 128GB and the SSD is 1TB so I did the following setup:

The m.2 is sda. The ssd is sdb.

```
/sda/sda1 /boot ext4    1 GB
/sda/sda2 swap          32 GB
/sda/sda3 /             Remaining

/sdb/sdb1 /home         Full TB

```


I installed spotify with:

```
snap install spotify
```

Next I got virtualbox installed:

```
sudo apt-get install virtualbox
```

 I got windows up and running in a VM and was able to connect to the family minecraft server so that was the first priority.

I then got Visual Studio code from their website:
[https://code.visualstudio.com/](https://code.visualstudio.com/)

Next up was copying over my blog, so I could document my next steps.



I installed steam with:

```
sudo apt-get install steam
```

I copied over my book library and installed calibre with:

```
sudo apt-get install calibre
```

Then I decided I wanted to try to run android apps.

I installed anbox

```
sudo apt-get install anbox
```

It didn't open so I loaded the Kernel mods:

```

sudo add-apt-repository ppa:morphis/anbox-support
sudo apt update
sudo apt install linux-headers-generic anbox-modules-dkms
sudo modprobe ashmem_linux
sudo modprobe binder_linux
```

The android image was still missing so.... I downloaded it from:

[https://build.anbox.io/android-images/](https://build.anbox.io/android-images/)

I copied the image to the right location:

```
sudo cp ~/Downloads/android_amd64.img /var/lib/anbox/android.img
```

removed the anbox and installed the snap
```
sudo apt-get remove anbox
snap install --devmode --beta anbox
sudo apt install curl wget lzip unzip squashfs-tools
wget https://raw.githubusercontent.com/geeks-r-us/anbox-playstore-installer/master/install-playstore.sh
```

Looked at the script with code first.

then ran:

```
chmod +x install-playstore.sh
```

Opened anbox and added full permissions.

```
anbox launch --package=org.anbox.appmgr --component=org.anbox.appmgr.AppViewActivity
```

Then I installed Skype for Business in AnBox.
I also installed Minecraft inside AnBox.

It opened both to a white screen. 

I swapped the driver to the NVIDIA one which was appropriate for my laptop.

Still white screen.

I came here:

[https://mcpelauncher.readthedocs.io/en/latest/getting_started.html](https://mcpelauncher.readthedocs.io/en/latest/getting_started.html)

Running it I get:
```
 Error initializing NSS with a persistent database
```

I hadn't installed chrome so that was next.

That wasn't the problem, but I was missing "nss".

```
sudo apt-get install libnss3 libnss3-tools
```

ran 
```
sudo apt-get dist-upgrade
```

still didn't work

added apt-file to search
```
sudo apt install apt-file
sudo apt-get install libnss3-dbg libnss3-dev
sudo apt-get install libnss3
sudo apt-get update
sudo apt-get purge libappstream3
sudo apt-get install libnss3
sudo apt-get update
```


```
sudo ln -s /usr/lib/x86_64-linux-gnu/nss/ /usr/lib/nss
```

```

LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/nss/
LD_PRELOAD=/usr/lib/x86_64-linux-gnu/nss/
```

I linked my ubuntu account and launched it from the gnome menu and it opened.
I had to pick a version before it would download.

https://mcpelauncher.readthedocs.io/en/latest/troubleshooting.html#no-audio

Then it was time for some styling:

background for terminal to green on black

Then for more shells:

```
snap install powershell --classic
sudo apt-get install xonsh
```

Just for fun I swapped the vscode terminal to powershell:

```
"terminal.integrated.shell.linux": "/snap/bin/powershell"
```

Then some gitkraken:

[https://www.gitkraken.com/](https://www.gitkraken.com/)

Edit:

To get my steam controller recognized by steam I had to:

```
sudo apt-get steam-devices
```

Edit:

because of breaking changes in gnome on ubuntu I've replaced the terminal:

```
sudo apt-get install terminator 
```

then because of 
[https://bugs.launchpad.net/terminator/+bug/1668245](https://bugs.launchpad.net/terminator/+bug/1668245)

I had to

```
mkdir -p ~/.config/terminator
touch ~/.config/terminator/config
```

I also installed GIMP
```
sudo apt-get install gimp
```

I also added lm-sensors

```
sudo apt-get install lm-sensors
sudo apt-get install sensors-applet
sudo apt-get install psensor
```

I also added cmus for music playing:
```
sudo apt-get install cmus
```

Edit:
I needed to manipulate a spreadsheet
```
sudo apt-get install libreoffice-calc
```

Edit:
I needed the voices on skyrim to work. I installed winecfg

```
sudo apt-get install wine
```

To Edit the winecfg:
```
WINEPREFIX=~/.steam/steam/steamapps/compatdata/<GAMEID>/pfx/ 
```

swapping out the GAMEID with the right one. In my case it was: 
```
WINEPREFIX=~/.steam/steam/steamapps/compatdata/489830/pfx/ winecfg
```

* devenum

using 

* xaudio2_6
* xaudio2_7

crashes

Edit:

needed some python stuff I was looking at [https://github.com/peterbraden/genetic-lisa/blob/master/python/genetic-images.py](https://github.com/peterbraden/genetic-lisa/blob/master/python/genetic-images.py)

```
sudo apt-get install python-pip libjpeg-dev
pip install -U numpy
pip install -U scipy
sudo apt-get install libjpeg-dev
pip install --no-cache-dir -I pillow==2.9.0
```
