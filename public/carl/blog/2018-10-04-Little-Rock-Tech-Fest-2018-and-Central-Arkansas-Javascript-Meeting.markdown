---
layout: post
title:  "Little Rock Tech Fest 2018 and Central Arkansas Javascript Meeting"
date: 2018-10-04T22:28:14-07:00  
categories: conferences,caj
---
## SWAG

![LRTech Fest 2018 Swag]({{site.url}}/static/images/lrtf2018_swag.png)

Items in the picture:

* T-Shirt
* two code magazines
* Starting Agile book
* Tervis Tumbler
* Eyeglass wipe
* Code promo
* Process Illustration Tokens
* Various Stickers
* LRTF notebook
* drawstring bag
* fidget cube
* tiny frisbee
* bottle cap opener
* pen
* web cam privacy slider

I have been going to every year Tech fest since 2013 with one exception. In 2017 I had a scheduling conflict with work, but this year I was once again able to attend:
 
[Little Rock Tech Fest 2018](http://www.lrtechfest.com/)

I was able to get an early bird ticket for 100 USD which I am hoping to get reimbursed. I parked in the wrong lot and it cost 12 USD.  

## Morning 

This morning I attended the [CSS Bloat talk](http://www.lrtechfest.com/speakers/2018/hoclement) which was really good. 

The talk slide deck is linked [here](https://bit.ly/cssbloat).

He gave several patterns for dealing with the problem and showed GitLab's custom solution.

He also showed the tool [storybook](https://storybook.js.org/)

Next I attended [Installing Skynet to Production - Stitching Together Microservices, AI and Kubernetes in the Cloud](http://www.lrtechfest.com/speakers/2018/kingchris)

Some of the important bits:

[pytorch](https://pytorch.org/)
[tenserflow](https://www.tensorflow.org/)
[google's guide to machine learning](https://developers.google.com/machine-learning/guides/rules-of-ml/)

Production application checklist

* NOC monitoring
* connection pooling
* async logging
* built in health checks
* ci/cd pipeline
* Automated quality checks
* Scalability
* Test Automation
* Load Test Results
* Secret management
* Endpoint security protection
* log aggregation
* Document API
* Service Discovery
* Externalized Config
* Dynamic security scan
* on call support

Next I went to the [MySQL 8 talk](http://www.lrtechfest.com/speakers/2018/stokesdavid)

Some highlights:

[Unofficial MySQL Guide](http://www.unofficialmysqlguide.com/)
[Release notes](https://dev.mysql.com/doc/refman/8.0/en/mysql-nutshell.html)

My take away from a developer at a Microsoft shop is that MySQL has been investing in JSON, document stores, built in api and histograms.  

SQL 2018 also has some [JSON support](https://docs.microsoft.com/en-us/sql/relational-databases/json/json-data-sql-server?view=sql-server-2017)

Lunch was provided _Tacos for life_ I got the vegetarian option which was a red tofu taco which was pretty good. 

## Afternoon

Next up was just what was needed after lunch.

An [interactive session](http://www.lrtechfest.com/speakers/2018/sheadmark)

this was also one of my favorites. Rethinking flow is really important to how fast things can be done through the entire organization.

Another coworker and I had a nice long conversation with the speaker afterwards.

The final course of the day was a by [Neal David](http://www.lrtechfest.com/speakers/2018/nealdavid)

Here are some thoughts from that item:

[https://reverentgeek.com/](https://reverentgeek.com/)

Node development makes developers happy. 

[Use Node to talk to SQL with seriate](https://www.npmjs.com/package/seriate)

[Strangulation Pattern for cleaning up an app](https://www.martinfowler.com/bliki/StranglerApplication.html)

Edge.js gives you .NET with Node.

## Central Arkansas JavaScript

After tech fest ended for the day I moved another training venue. [Central Arkansas JavaScript](https://www.centralarkansasjavascript.com/)

Some take items from this session:

[https://serverless.com](https://serverless.com)
[https://github.com/fnproject/fn](https://github.com/fnproject/fn)

It was really great to have a small group talk with [Tristan Sokol](http://www.lrtechfest.com/speakers/2018/sokoltristan)
