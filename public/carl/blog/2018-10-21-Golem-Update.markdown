---
layout: post
title:  "Golem Update"
date: 2018-10-21T19:06:46-07:00  
categories: csharp
---
I consolidated several portions of the application into a single console app making use of the [Command Line Utils package](https://www.nuget.org/packages/Microsoft.Extensions.CommandLineUtils/) This is similar to the python argparse, but has some fiddly little quirks. For example it requires setting the help for each command.

The salt utility is now just an option of the command line app.
I've made the previous console app just an "interactive" mode of the app.
Options now include:

* inspect - inspect request information
* interactive - run in interactive mode
* list - list requests
* salt - generate a salt
* serialized - executes a json serialized request
* usercommand - execute a service as a user

In addition I added a convention loader for the commands, and processors.
