---
layout: post
title:  "Minecraft 1.8.x functions behavior packs and image renderer"
date: 2018-12-19T20:19:06-06:00  
categories: minecraft,python
---

I made a python3.7 file which outputs a minecraft bedrock function.

Currently it only renders black and white, but I hope to add more colors soon.




Here is the python script:


```
from PIL import Image
import codecs


im = Image.open('derp.png') # Can be many different formats.
pix = im.load()
x,y = im.size

file = codecs.open("C:\\Users\\YOURUSERGOESHERE\\AppData\\Local\\Packages\\Microsoft.MinecraftUWP_8wekyb3d8bbwe\\LocalState\\games\\com.mojang\\behavior_packs\\YOURBEHAVIORPACKGOESHERE\\functions\\derp2.mcfunction", "w", "utf-8")
file.write(u'\ufeff')
# Brute implementation
for currentX in range(0,x):
    for currentY in range(0,y):  # Get the width and hight of the image for iterating over
        if((255,255,255) == pix[currentX,currentY]):  # Get the RGBA Value of the a pixel of an image
            file.write(f"fill ~{x-currentX} ~{y-currentY} ~ ~{x-currentX} ~{y-currentY} ~ wool\n")
        else:  # Get the RGBA Value of the a pixel of an image
            file.write(f"fill ~{x-currentX} ~{y-currentY} ~ ~{x-currentX} ~{y-currentY} ~ coal_block\n")


file.close()
```

NOTE:
The tilde _the squiggle_ ~ is the current player position so the script is filling near the player.


Script outputs to UTF-8 because it doesn't work otherwise.

If it doesn't find PIL you might have to pip install "image"

Swap out the YOURUSERGOESHERE for your user.
Swap out the derp.png with the file you want to render. 
Swap out the YOURBEHAVIORPACKGOESHERE with a behavior pack.

X pixels * Y pixels should be less than 10,000 because minecraft has a 10,000 line function limitation.
I didn't optimize it to render more than one block at a time.

In your behavior pack folder you can add a pack_icon.png 128x128.

You'll also need a pack_manifest.json.

```
{
    "format_version": 1,
    "header": {
      "uuid": "YOURGUIDGOESHERE",
      "name": "YOURNAMEGOESHERE",
      "version": [0, 0, 2],
      "description": "YOURDESCRIPTIONGOESHERE",
      "min_engine_version": [1, 8, 0]
    },
    "modules": [
      {
        "description": "YOURDESCRIPTIONGOESHERE",
        "type": "data",
        "uuid": "YOURGUIDGOESHERE",
        "version": [0, 0, 2],
        "min_engine_version": [1, 8, 0]
      }
    ]
  }
```

Then you should be able to open Minecraft and load up the behavior pack in a new world.

```
/function derp2 
```

After you run it you can rerun the python script and overwrite the function then call:

```
/reload
```

Which will update the function and you can re-execute:

```
/function derp2
```

to render another image.

Links:

* [https://minecraft.gamepedia.com/Function_(Bedrock)](https://minecraft.gamepedia.com/Function_(Bedrock))
* [https://minecraft.gamepedia.com/Tutorials/Creating_behavior_packs](https://minecraft.gamepedia.com/Tutorials/Creating_behavior_packs)

# Screen Cap

![Screen Cap]({{site.url}}/static/images/minecraft_render.png)