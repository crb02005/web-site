---
layout: post
title:  "Happy Hanukkah"
date: 2017-12-12T18:40:49-07:00  
categories: arduino
---
## LED Menorah


![LED Menorah]({{site.url}}/static/images/c538ec08-483b-4733-b953-5322f3656636.jpg "LED Menorah")


For the past few weeks I’ve been very busy with my day job and haven’t had much time to update things, work on projects, play games, or build music equipment but I thought I would take a little break and update my blog. My mom wanted me to build her an LED Menorah. I took an Arduino Micro and then soldered up some LEDs and a tac switch. I only built the one and my mom has it, but I’ve made a [repo with the code](https://gitlab.com/crb02005/menorah-led). _moved to gitlab 2020_

## Signal Generator

I picked up a Signal Generator kit and put it together:


![Signal Generator]({{site.url}}/static/images/ab6f681f-fd2b-4141-a3a1-9d3c5e760bcf.jpg "Signal Generator")




![Waveform]({{site.url}}/static/images/cf6d51a3-62cf-40f8-b862-be3fda1ec7a5.jpg "Wavform on scope")


Then I accidently destroyed it when I built the power supply backwards.

I’ve ordered a replacement capacitor, but I think I’ll need a new IC.

## Steam


![Steam specials]({{site.url}}/static/images/fde98c9e-6afc-426c-a39c-951157b70982.jpg "Steam Devices")


I took advantage of the Steam specials and picked up these.

I did find time to hook them up and I was really happy with them. The controller seemed a bit odd at first but was quite playable.