---
layout: post
title:  "TI-83 Stat Calc"
date: 2015-05-01T12:57:00.002-07:00  
categories: math,python,numpy
---
Using a TI-83 I used to use "Stat Calc" to determine a formula for a series of points.  

I can now use Python via numpy:  

import numpy  

x = [0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10]  
y = [1,   6,  17,  34,  57,  86, 121, 162, 209, 262, 321]  
coeffs = numpy.polyfit(x,y,deg=2)  
print numpy.poly1d(coeffs)  
print numpy.polyval(numpy.poly1d(coeffs), 10)  

For other languages see:  
http://rosettacode.org/wiki/Polynomial_regression