---
layout: post
title:  "Python Loading JSON files into normalized Sqlite3 Tables"
date: 2021-3-12T20:45:00-06:00  
categories: python,sqlite3
---

Building tables, populating them with data is often a job for Visual Studio Database Projects if you are in a Microsoft world. In a Sqlite3, Python world you might have other tools, but I thought I could make one that was simple enough for my limited needs. 

Here is how I have my directory structure:

```
import\
        mob\
            orc-like-creature.json
            were-wolf.json
        item\
            sword.json
        shop\
            bob-weapon-shoppe.json
        room\
            the-pit.json
            the-town.json
            the-shopping-center.json
```

The script loops the import directory looking for tables in the database which match the subfolders, then each of json files are loaded and the columns which match the json dictionary entries are loaded, and if the table contains a field called data the json string is loaded into it.

```python
import sqlite3
import os
import json

schema_sql = "select name,type,pk from PRAGMA_TABLE_INFO(?)"

def load_table_from_folder(cur, import_folder, table):
    
    cur.execute(schema_sql,(table,))
    column_names =[]
    pk=""
    print(f"\treading schema for table {table}")
    for row in cur.fetchall():
        column_names.append(row[0])
        if row[2] == 1:
            pk=row[0]

    if len(column_names) < 1:
        print(f"\ttable {table} does not exist.")
        return False

    parameter_expression=",".join(["?"]*len(column_names))
    insert_into_table = 'INSERT or REPLACE INTO '+table+' VALUES ('+parameter_expression+')' 

    data_to_load = []
    file_to_load = ""

    for x in os.listdir(f"{import_folder}\{table}"):
        file_to_load = f"{import_folder}\{table}\{x}"
        print(f"\tconsidering {file_to_load}")
        if x.endswith(".json"):
            print(f"\t\tloading {file_to_load}")
            with open(file_to_load, 'r') as file:
                data = json.load(file)
            data_to_load=[]
            for column_name in column_names:
                if column_name == "data":
                    data_to_load.append(json.dumps(data))
                elif(column_name in data):
                    data_to_load.append(data[column_name])
                else:
                    data_to_load.append(None)
            print(f"\t\t\tinsert or replacing '{table}' '{data[pk]}'")
            cur.execute(insert_into_table, data_to_load)

    return True

def load_to_database(connection_string, import_folder):
    con = sqlite3.connect(connection_string)
    cur = con.cursor()
    
    for directory in os.listdir(import_folder):
        print(f"Attempting to import from {import_folder}\{directory}...")
        if os.path.isdir(f".\{import_folder}\{directory}") and load_table_from_folder(cur, import_folder, directory):
            con.commit()
            print(f"\t{import_folder}\{directory} finished.")
        else:
            print(f"\t{import_folder}\{directory} is not suitable for import.")

    con.close()

load_to_database('db.sqlite',"import")
```
