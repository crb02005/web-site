---
layout: post
title:  "Spelling For the Command Line"
date: 2016-08-23T17:38:00.002-07:00  
categories: aspell,pandoc,spelling
---
If you haven't heard of spell, ispell, or aspell you probably haven't looked for a command line spell checker. The great folks at [GNU Aspell](http://aspell.net/) have provided a free spell check utility. If you are a windows user and can't be bothered to build from source the way [Stallman](http://www.stallman.org/) intended:

[Win32 Port](http://aspell.net/win32/)

```
    aspell -c somefile.txt
```

Running this against a file will give you an error:

```    
    aspell : Error: No word lists can be found for the language "en_US".
```

Then you realize you forgot to install the Precompiled dictionaries.

Then you realize you are running it in PowerShell ISE:

```  
initscr(): LINES=1 COLS=1: too small.
```

Then you find the -a command which lets you pipe from standard in and then errors to standard out.

```
cat .\content.html | aspell -a
```

Great.... Well those tags don't work that well.

pandoc to the rescue:

```
pandoc .\content.html -t plain -o - | aspell -a | ?{$_.StartsWith("&")}
```

Okay I added that last "StartsWith"" to give me just the errors.

With a little PowerShell-Fu you can turn those into a collection:

```
    pandoc .\content.html -t plain -o - | aspell -a | ?{$_.StartsWith("&")} | %{

    $pieces =  $_.split(":")
    $parts = $pieces[0].Split(" ");
    $possibleIncorrectWord = $parts[1];
    $lineNumber = $parts[2];
    $column = $parts[3];
    $choices = ($pieces[1..($pieces.Length-1)] -join ":").Split(",") #| %{$_.trim()}

    @{"possibleIncorrectWord"=$possibleIncorrectWord;"line"=$lineNumber;"column"=$column;"replacements"=$choices}
    }
```

You could do some replacements or manually edit, but you've got a list of what is potentially wrong.