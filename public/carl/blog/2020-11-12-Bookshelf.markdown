---
layout: post
title:  "Adding shelving to a multipurpose room"
date: 2020-11-12T16:48:00-06:00  
categories: woodworking
---

Yesterday I helped clean up my mom's garage, they had leftover wood from a shelving project for my now deceased father's library. I thought it would be a nice use of the wood to add some shelves to my own office. 

Requirements:

* should be sturdy enough to hold a small TV
* should match the theme of the room

I looked at the lumber and the space in the room, and came up with this:

![Shelf]({{site.url}}/static/images/shelf-revised.png)

I used deck screws _6 x 1-1/4"_ which I got from ACE hardware.

I had started with large L brackets but it didn't look good, and was overkill.

![Shelf]({{site.url}}/static/images/shelf-real.png)
