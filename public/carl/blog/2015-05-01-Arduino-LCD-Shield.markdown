---
layout: post
title:  "Arduino LCD Shield"
date: 2015-05-01T17:30:00.001-07:00  
categories: 
---
Replacement Resistors

A while back my LCD Shield had an incident in my backpack and the blue contrast adjustment snapped off. I couldn’t find it, it must have walked off. I was set to order a new one.  Ordered a new one from China, but I found the old broken screen and didn’t have a replacement part, but I did have resistors. I found the perfect  a usable setting:

![shield]({{site.url}}/static/images/LCDShield1.jpg)

Fixed Contrast Replacement
![shield2]({{site.url}}/static/images/LCDShield2.jpg)


I guess I’ll have a spare once my other one gets in, but I was happy I made this one work again.

IT’S ALIVE!
