---
layout: post
title:  "Humble Book Bundle"
date: 2016-08-21T18:58:00.002-07:00  
categories: ebook,eff,humblebundle
---
If you like to code and like to read I've got some great news for you:

## [The Joy of Coding Book Bundle](https://www.humblebundle.com/books/joy-of-coding-book-bundle)

16 books about programming isn't a bad deal for about a dollar a piece. Best thing is you will support one of the noblest of causes: [The EFF](https://www.eff.org/)

Most of them come in epub, pdf, and mobi, but a few do not:

*   Hacker Bundle 2016 PDF Sampler (PDF only)
*   If Hemingway wrote JavaScript (no MOBI)