---
layout: post
title:  "End Of WingIDE usage"
date: 2017-08-03T19:53:45-07:00  
categories: wingide
---

A while back I bought a copy of WingIDE on Steam. It worked great and I was happy with it. Sadly Python3.6 is not supported by version 5.1 of the IDE. I don't really want to contribute $40 to an IDE I use in my spare time. For now I'm going to make Visual Studio Code work for my Python coding while I investigate other options.
