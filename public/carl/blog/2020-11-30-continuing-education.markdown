---
layout: post
title:  "continuing education"
date: 2020-11-21T20:48:00-06:00  
categories: meta,Kaizen
---

# The 7th habit from Covey: "Sharpen the Saw; Growth"

I strive to constantly improve my skills. 

Here are some research materials I've recently been reviewing:


* Microservices Architectural Design Patterns Playbook by Rag Dhiman - a course available on Pluralsight.
* Art of computer programming Vol I by Donald E. Knuth
* The Simple Path to Wealth by JL Collins
* Bob Vila's workshop the ultimate illustrated handbook to the home work shop
* Making desks and bookcases by Nick Engler
* 25 essential projects for your workshop best of Popular Woodworking magazine
 
