---
layout: post
title:  "Starving Pinnipeds"
date: 2018-10-29T18:36:06-07:00  
categories: javascript,gaming
---
A while back I started sowing the seeds of knowledge for my children to learn software development. I tried Python, and a few other things with them, but I thought I'd make a simple game in JavaScript and migrate it through development styles to show them a few things about how software evolves.

I used [Aseprite](https://www.aseprite.org/) to make a simple image for the game and walked them through how layers can form an animation. 

![Screen shot]({{site.url}}/static/images/pinniped.gif)

I've created the repo:

REDACTED

I started them off learning about functions and few other things, and then showed them how people coded before jQuery. Then the stages of using selectors to wire up a page, then finally a bit of VueJS.
 
![Screen shot]({{site.url}}/static/images/9c316194-7d7a-41c8-bc23-fb16eb18a198.png)

I let them implement a feature _autocollector_ which collects coins from the pinniped and look at fixing bugs.

I don't think I'll be spending much more time on the repo, but they might be adding features at a later date.
