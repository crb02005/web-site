---
layout: post
title:  "Tools for Development on Android"
date: 2017-06-21T22:01:49-07:00  
categories: android,termux
---
## termux

About ten days ago I posted on reddit about development on android. Not apk development but actually writting code on an Android phone or tablet. I didn't get any good feedback at first until /u/jaumegreen suggested termux. It was exactly what i needed.

Ctrl is needed to work on consoles.

On my phone volume down emulates that key.


Next I had to share a storage space to allow files to be exposed.

```

cd /storage/emulated/0/
mkdir WorkingDir
```

and then I was off to the races.

```
made on nano via termux
```