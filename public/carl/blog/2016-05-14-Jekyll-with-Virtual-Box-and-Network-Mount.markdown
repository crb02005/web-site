---
layout: post
title:  "Jekyll with Virtual Box and Network Mount"
date: 2016-05-14T12:39:00.001-07:00  
categories: jekyll,virtual box
---

It appears when you have your files for Jekyll stored on the host machine on VirtualBox the files get cached and you receive a stale copy. 

A simple solution is to "touch" the file.

```
#!/bin/bash
echo "Touching all the things..."
find . -exec touch {} \;
echo "Finished Touching..."
jekyll serve
```