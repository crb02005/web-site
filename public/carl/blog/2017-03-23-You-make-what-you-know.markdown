---
layout: post
title:  "You make what you know"
date: 2017-03-23T19:23:57-07:00  
categories: diy,expectations
---
My oldest son came to me after we moved in and asked to build a project. We had completed the pegboard and adding a reinforcement to an heirloom wicker chair. I acquiesced and began looking at his instructions. He wanted some wheels, a platform, and a place for me to push him around. It didn't have a specification on the size of wheels. No weight requirements. No deadlines on when to ship the product. I didn't even have to have a daily stand up meeting to discuss the project status, but somehow he and I put together the cart. I found casters which were acceptable for his weight, and considering I managed a warehouse for a bit I build a utility cart: ![]({{site.url}}/static/images/pushcart.jpg)

It isn't much to look at but it can haul, and it has a full turn radius as all the wheels are omni-directional. There are many cart styles I could have built, but I ended up building something familiar. When building a quick project without specifications and needing a working product building something familiar is a safe bet. If I had unique requirements or a demanding customer, a new tool I wanted to try, or new design schematic, then I might be tempted to build something else.

When constructing an item in the real world the cost of failure results both in a loss of time and materials, but with software the barrier is lower for materials. Time is always a cost.

*   Carl