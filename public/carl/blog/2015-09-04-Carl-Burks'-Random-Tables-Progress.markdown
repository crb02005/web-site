---
layout: post
title:  "Carl Burks' Random Tables Progress"
date: 2015-09-04T08:09:00-07:00  
categories: 
---
I've made a bit more progress on my application.  

I am working on adding a feature to support multiple data directories in the web version. This will keep things organized a little better and will make it easier to drop in content packs. I also registered a domain for this application and have researched hosting. After I finish adding the feature I plan to fork the code base and create a repo for each major part:  

Portable Library  
Windows 10 Port  
Web Port  

Further I will add a repo for the Creative Commons Share-Alike Attribution Non-commercial data pack.  

Next feature will be to add logic for custom attribution display messages for the Web Port and corresponding parsing instructions to the Portable Library.  

After these are developed I will hopefully setup the web facing application.  

I will then split my time for this project between adding features, adding tables to the CC repo, adding content to the publisher packs which have given permission.  

As a side note, Bing seems to hate blogger.com searching for my name, "Carl Burks", doesn't show any blogger.com links and shows a bunch of spammy unrelated content. I'm trying an experiment by adding my name to post title to see if that improves things.
