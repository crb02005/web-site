---
layout: post
title:  "Fixing the time in Windows after booting into another OS"
date: 2017-01-16T10:43:00.002-07:00  
categories: windows
---
If you booting in multiple operating systems sometimes the date gets out of sync.

If you want to update the time you can use the command:
```
    W32tm /resync /force
```
[W32tm info from TechNet](https://technet.microsoft.com/en-us/library/bb491016.aspx)

This will force a resync.

If you want to make it into a powershell function for easy access:
```
    function Update-WindowsTime(){
        W32tm /resync /force
    }
```
[Sometimes you code the same thing and forget about it.](http://www.burksbrand.com/2016-04-13T14-22-00.001-07-00--PowerShellFunctions.html)

I found this article from my blog using a special search in the search engine.
```
    site:burksbrand.com time
```
Using site colon it forces the results to only include from the host listed.

I tried this on both Bing and Google and it worked.

Now to the real issue why does the time get "out of sync".

Windows attempts to sync time with the local time zone. GMT was made for a reason.

This is suppose to fix the real issue:

_be careful_

*   Win + R
*   regedit
*   Press enter

*   Navigate to HKEY_LOCAL_MACHINE

*   Add a DWORD of RealTimeIsUniversal set to 1

or

*   Create a new file named time.reg

    Windows Registry Editor Version 5.00

    [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation]
    "RealTimeIsUniversal"=dword:00000001

Then run it.

_Don't just run random Registry settings/file on your computer without researching what they do_

_Update 2017-01-21_

Registry doesn't fix it.

_Update 2019-4-13_

Here is currently my fix: `net start w32time W32tm /resync /force`