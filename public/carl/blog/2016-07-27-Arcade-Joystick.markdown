---
layout: post
title:  "Arcade Joystick"
date: 2016-07-27T21:34:00.002-07:00  
categories: arcade,diy,gaming,joystick
---
This idea has been a long time in the cooker. I've been wanting to put together an Arcade style joystick since my Commodore days. I finally just did it:

I ordered one of these from amazon:
"Easyget Zero Delay Pc Arcade Game Joystick Cabinet DIY Parts Kit for Mame Jamma & Fighting Games Support All Windows Systems - Color Black Kit"
It came today:

![]({{site.url}}/static/images/c24d4fd2-dcec-4279-a6f6-a5532e3c8f34.jpg)

The unboxing.
![]({{site.url}}/static/images/de41e5a7-8497-43b3-a105-4e78f4043843.jpg)

It came in a bunch plastic bags.
![]({{site.url}}/static/images/c48a878a-0f7e-4e63-9e76-09b332ab33a6.jpg)

Here are the parts fanned out.
![]({{site.url}}/static/images/39ef91a3-cb93-4a4d-8471-755a2e4d1d9d.jpg)

A blurry one of me testing the joystick without the buttons on my computer before assembled it.
![]({{site.url}}/static/images/2590206b-2da0-48ea-9fa1-7865002a63ed.jpg)


I found these on thingaverse. I printed them, but one of them snapped. I'm reprinting it with a higher fill density.
[thing](http://www.thingiverse.com/thing:723554)


Raspberry Pi Arcade Stick Console

![]({{site.url}}/static/images/8ea46de1-9332-43e9-8b7b-eee7c473ee46.jpg)

Crude markings for drilling.
![]({{site.url}}/static/images/421dda6a-6879-49ec-8d52-070b51008f57.jpg)

The pieces drilled.
![]({{site.url}}/static/images/607cd9ef-6c6d-4349-a5c7-3b1f4245d9e0.jpg)

A trip to Ace hardware and I was ready to sand, paint, and assemble.
![]({{site.url}}/static/images/11a94435-0915-4dff-882b-499262b9a602.jpg)

I thought I would test out some paints on the bottom piece which isn't very visible once assembled.
![]({{site.url}}/static/images/1b7f20bb-4aea-4266-b62b-ccced94601cb.jpg)

Here is the painted version without assembly. 
![]({{site.url}}/static/images/43fc5622-b884-41d6-9363-307f8864a334.jpg)

Mostly assembled. I drilled holes for the joystick but after putting the hardware in I noticed the D-stick bumped one of the edges of the wall. instead I used some wood screws and re-centered stick.
![]({{site.url}}/static/images/efa46069-3782-4ae0-b3ba-a112f16ed004.jpg)

Here we go hooked up. You can see the keyboard underneath. I'm going to add a wrist rest next.
After some advice from my long time friend I stumbled upon the joytokey software to get things working for games which don't support standard joysticks.
![](http://joytokey.net/en/)

The joystick parts from amazon cost me 25 dollars and 10 dollars worth of search history sold to "Bing" for Amazon gift cards.
We had the wood left from a crib to outdoor couch project.
Then I spent about 10 dollars at Ace Hardware.
The risers I 3d printed from a giant orange spool of .175 I ordered for around 13 dollars so I'll go ahead a say about .50 cents worth.
Total out of pocket $35ish. Not bad. 
