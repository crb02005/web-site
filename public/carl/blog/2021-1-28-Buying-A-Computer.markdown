---
layout: post
title:  "Buying a computer"
date: 2021-1-28T20:30:00-06:00  
categories: computer,capitialism,commercialism
---

This week someone asked for my recommendations for new computer. 

I don't really like to give advice on computer tips, for several reasons. 

Here are the places I go when I'm looking at a new computer

## Desktop Options:

* [https://www.reddit.com/r/suggestapc/](https://www.reddit.com/r/suggestapc/)
* [https://www.reddit.com/r/buildapc/](https://www.reddit.com/r/buildapc/)

## Laptop Options:

* [https://www.reddit.com/r/SuggestALaptop/](https://www.reddit.com/r/SuggestALaptop/)


## PC Building links:

* [https://pcpartpicker.com/](https://pcpartpicker.com/)
* [https://www.youtube.com/user/LinusTechTips](https://www.youtube.com/user/LinusTechTips)

They specifically wanted advice for a Minecraft capable machine.

Here are the specs I found:


Recommended Requirements:

CPU: Intel Core i5-4690 3.5GHz / AMD A10-7800 APU 3.5 GHz or equivalent
RAM: 8GB
GPU: GeForce 700 Series or AMD Radeon Rx 200 Series (excluding integrated chipsets) with OpenGL 4.5
HDD: 4GB (SSD is recommended)
OS (recommended 64-bit):
- Windows: Windows 10
- macOS: macOS 10.12 Sierra
- Linux: Any modern distributions from 2014 onwards

Minimum Requirements:

CPU: Intel Core i3-3210 3.2 GHz / AMD A8-7600 APU 3.1 GHz or equivalent
RAM: 4GB
GPU (Integrated): Intel HD Graphics 4000 (Ivy Bridge) or AMD Radeon R5 series (Kaveri line) with OpenGL 4.4*
GPU (Discrete): Nvidia GeForce 400 Series or AMD Radeon HD 7000 series with OpenGL 4.4
HDD: At least 1GB for game core, maps and other files
OS:
- Windows: Windows 7 and up
- macOS: Any 64-bit OS X using 10.9 Maverick or newer
- Linux: Any modern 64-bit distributions from 2014 onwards
Internet connectivity is required for downloading Minecraft files, afterwards offline play is possible.

These specificiations are pretty low by modern computer standards.

As it turns out I believed that the computer they already had was up to the task, but needed to allocate more RAM.
