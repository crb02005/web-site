---
layout: post
title:  "New board with wifi"
date: 2017-01-09T22:26:00.002-07:00  
categories: lua,nodemcu
---
Ordered a NodeMCU from amazon for around $8\. When it arrived I followed the instructions here:

[Arduino on the NodeMCU](https://github.com/esp8266/Arduino#installing-with-boards-manager)

using the Arduino IDE seemed a little more user friendly than a command line flash.

Still haven't found the right board to get it to actually program with.... flashing back to the lua version

[LuaLoader](https://github.com/GeoNomad/LuaLoader/)

Got it connect to my Wifi.

Now time to program some points on a webserver for it to hit.