---
layout: post
title:  "Infrared remote Update"
date: 2017-07-30T19:15:09-07:00  
categories: arduino,ir
---

Spend some time this weekend playing with my new soldering iron. I put together the IR blaster and receiver.

Here's a picture: 


![]({{site.url}}/static/images/d94ad685-3978-4564-884e-558fbccfa159.jpg)


So for the Arduino code I used the IR library:


[IR Library](https://github.com/z3t0/Arduino-IRremote)

I played around with getting the blaster to work without success. The receiver I was able to read several remotes, but not the one for my Changhong tv. I think it might use a frequency that is out of range or perhaps blueooth even.

I hooked the board that I soldered together both the Leonardo and the pro-micro and it worked with both. I wrote a little bit of Python plumbing code to have my computer use the IR receiver:

```
import serial
from subprocess import Popen, PIPE


volume_up_sequence = '''key XF86AudioRaiseVolume
'''
volume_down_sequence = '''key XF86AudioLowerVolume
'''

terminal_sequence = '''keydown Control_L
keydown Alt_L
key t
keyup Control_L
keyup Alt_L
'''

key_sequence = '''key %s
'''

def keypress(sequence):
     p = Popen(['xte'], stdin=PIPE)
     p.communicate(input=sequence)

def quick_kp(key):
     keypress(key_sequence % (key,))

ser = serial.Serial('/dev/ttyACM0', 9600)
def volume_up():
     keypress(volume_up_sequence)
def volume_down():
     keypress(volume_down_sequence)
def open_terminal():
     keypress(terminal_sequence)

handlers = {"E0E0E01F":volume_up,
            "CF2F9DAB":volume_up,
            "B2BBAC69":volume_down,
            "E0E0D02F":volume_down,
            "E0E058A7":open_terminal,
            "E0E020DF":lambda: quick_kp("1"),
            "E0E0A05F":lambda: quick_kp("2"),
            "E0E0609F":lambda: quick_kp("3"),
            "E0E010EF":lambda: quick_kp("4"),
            "E0E0906F":lambda: quick_kp("5"),
            "E0E050AF":lambda: quick_kp("6"),
            "E0E030CF":lambda: quick_kp("7"),
            "E0E0B04F":lambda: quick_kp("8"),
            "E0E0708F":lambda: quick_kp("9"),
            "E0E08877":lambda: quick_kp("0")
            }
while True:
     key = ser.readline().split("\r\n")[0]
     if key in handlers:
          handlers[key]()
     else:
          print(key)</code></pre>
```