---
layout: post
title:  "Start of a Geocache"
date: 2013-10-30T16:42:00.003-07:00  
categories: 
---
One of my favorite sites "Hack a Day" was running a contest to slap their logo on something and possibly win a Trinket by AdaFruit. Since I didn't have an ion beam to win the small category and didn't have access to any giant building light arrays I thought of a creative way to spray paint something.

A while back I had picked up two surplus ammo boxes which are perfect size for a traditional geocache. I took their logo image and made an outline of it so I could print just what I needed to cut it. After printing the outline I sliced it up and left a small strip to keep the eyes in the appropriate places. A few sprays and the logo was on. Here is a link to the article:

[http://hackaday.com/2013/10/26/trinket-contest-update-2/ ](http://hackaday.com/2013/10/26/trinket-contest-update-2/)  
![GeoCache]({{site.url}}/static/images/7f7b14ca-90e3-47c2-a47e-8038cb4fbd7b.JPG) Future Geocache

Now I need to find a place to stash it. I'll probably need a nice label on the top and side and maybe a QR code to scan when it is found, or a log book for the traditional people. It would be neat to have a few electronic projects to put inside. I'm open to ideas.