---
layout: post
title:  "January Update"
date: 2018-01-21T23:49:44-07:00  
categories: powershell,python,minecraft
---
REDACTED

## Code

### Book Site

Bought the latest Humble Python bundle and added it to my library. I wanted to start extracting the ISBNs to get better meta data. I built a regex to extract it.

With Powershell it was pretty easy to find them.

Docs: [Microsoft Powershell Regex Docs](https://social.technet.microsoft.com/wiki/contents/articles/4310.powershell-working-with-regular-expressions-regex.aspx)

It wasn’t right approach though. Letting Python do the heavy lifting was:

[PyPDF2](https://pythonhosted.org/PyPDF2/) to extract the text. [ISBNTools](https://pypi.python.org/pypi/isbntools) to get the ISBN and to find the meta data.

[WordCloud](https://pypi.python.org/pypi/wordcloud)

This required installing the C++ redistributable and dealing with rc.exe missing from the Visual Studio directory:

[StackOverflow Visual Studio can’t build due to rc.exe](https://stackoverflow.com/questions/14372706/visual-studio-cant-build-due-to-rc-exe)

### Text Game Engine

I’ve been tinkering with a game engine. At the moment I’m focusing on text only. I’ve got a Hex Grid renderer that is my current focus on this project.

Other projects I’m playing with:

### Python

*   multiprocessing library
*   numpy
*   scipy
*   matplotlib

# Minecraft

I played a little Minecraft with my boys and I was happy to learn Minecraft for windows added back command blocks. Previously I wrote an article about how to build a castle with command blocks for the Java Edition. If I have time I’d like to revisit that for the new version.
