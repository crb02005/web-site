---
layout: post
title:  "Image Manipulation With Image Magick"
date: 2018-01-16T21:39:15-07:00  
categories: cropping,powershell,resizing
---
Automatically cropping an image with powershell and ImageMagick:
```
    ls | %{magick  convert $_.FullName -flatten -fuzz 1% -trim +repage $_.FullName} 
```
Automatically resizing an image with powershell and ImageMagick:
```
    ls | %{magick  convert $_.FullName -resize 300x300 $_.FullName}
```