---
layout: post
title:  "Arduino Project"
date: 2017-08-19T22:12:08-07:00  
categories: nano,punchcard
---


![]({{site.url}}/static/images/punchcard.jpg)

Punch Card Read with Nano For Scale




Well I've been playing with electronics again. I made a rookie mistake and started soldering before breadboarding. So I had to do some rework, but here is what I ended up with:


Resistors are 10kOhm _RX_.


Photoresistors are those little round things _LDRX_.


Analog input on Arduino _AX_.



![Punch Card Diagram]({{site.url}}/static/images/punchcard.svg)

I soldered up a Nano and hooked this thing up, I cut some 3x5 cards and tested it out:

```
// pin assignments
#define SENSOR_COUNT 8
#define THRESHOLD 300
#define CONNECTION_SPEED 9600
#define LOOP_DELAY = 1000
 // binary values
 int VALUES[8] = {1,2,4,8,16,32,64,128};
 void setup() {
    for(int i=0; i < SENSOR_COUNT; i++){
     pinMode(i, INPUT);
    }
   Serial.begin(CONNECTION_SPEED);
 }
 void loop() {
  int result = 0;
  for(int i=0; i<SENSOR_COUNT;i++){
     if(analogRead(i)>THRESHOLD){
      result += VALUES[i];
     } 
  }
 Serial.println(result);
 delay(LOOP_DELAY);
 }
```

That's about it. I read each resistor and total up the values and send it along. Next steps? Perhaps add a wheel that feeds the cards. Redo the reader a tension connection and a clock.


[A more elegant solution](https://arduining.com/2012/06/10/arduino-punched-card-reader/)
