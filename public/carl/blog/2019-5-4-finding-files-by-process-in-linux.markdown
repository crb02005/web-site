---
layout: post
title: "How To find files by process in linux"
date: 2019-05-4T07:50:06-05:00
categories: supertux2,ubuntu,bash
---

I was moving my son's save game process for supertux to a different computer and noticed:

```
~/.supertux2
```

was missing. So I went to the chat for supertux and tried to find out where they should be. The people online at the time didn't know. My googlefoo left me without a location.

Next I looked up how to find files used by a process in linux. I found pfiles, but that didn't seem to be on Ubuntu by default, I did some more searching and found what I wanted:

```
lsof -a -p <pid>
```

You can get the pid from 
```
ps -A | grep tux
```