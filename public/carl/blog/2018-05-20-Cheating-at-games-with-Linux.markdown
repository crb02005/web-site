---
layout: post
title:  "Cheating at games with Linux"
date: 2018-05-20T21:16:29-07:00  
categories: linux,scanmem,gaming
---
One of my children wanted me to join them playing Starbound. Recently I've pretty much given up on gaming, so I didn't have my old character. First I had to get steam up and running. I'm not sure if it was the multilib or the kernel update, but my Arch's X server didn't want to start up after. I was on a limited time table, so rather than fix the problem I just swapped the OS. With Manjaro the touchpad didn't work out of the box, so I popped Ubuntu onto the machine and I got Steam installed. Shortly later I was able to install scanmem. I fired up Starbound and made a character.

First I need to find the pid for Starbound.

```
ps -A | grep -i starbound
```

_ps - report a snapshot of the current processes_

then I spawned scanmem

```
pid 99999
```
I replaced the 99999 with the actual pid.

Next I collected some pixels in Starbound
I typed in the number that I had in scanmem.
I repeated this process until I had two matches.
I used:

```
set 1000000
```

Instant pixel millionaire.

After that I repeated the process for tech cards etc until my character was back to the level of progress my old character was.

This process works for most games.
