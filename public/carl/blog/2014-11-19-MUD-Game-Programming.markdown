---
layout: post
title:  "MUD Game Programming"
date: 2014-11-19T19:23:00.001-08:00  
categories: mud,gaming
---
I recently obtained a copy of a book I lost a few years ago.  

http://www.amazon.com/Game-Programming-Premier-Press-Development/dp/1592000908  

It is a great niche book which covers building a MUD from start to finish (a MUD is never really finished)  

Now that I am rearmed with this guide book I am going to make a MUD. I'm going to document my process from start to finish*.  

The first choice I need to make is languages. The guides in the book are C++ and eventually plugin support with Python.  

I could follow the book exactly, but the opportunity to enhance the design. I am considering the trade off between native network access and a web interface. I could go with a web framework and use SignalR and make a slick web client and/or make a standard network connection via telnet and or dialup access.   

Here is what I am considering:  

C++ or C# for telnet and dialup connection to the MUD  
C# for the MUD engineC# for the Web Server interface  
Python for plugins  
SignalR to allow enhanced web connection  
WebAPI for native user bot support  

After some consideration this stack will not work well on my RasberryPi which would be a great dedicated machine for a MUD.  

I think I am going to use Node.js for everything.