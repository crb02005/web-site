---
layout: post
title:  "Games from my past"
date: 2017-01-30T20:38:25-07:00  
categories: c64,retro,rpg
---
After watching several episodes of the 8-bit guy on YouTube I was feeling nostagic and went looking for a few games I used to play on the C64C the longer grey Commodore 64.

# Gateway to Apshai

This was my entry into roleplaying games. I had spent days playing this game.

It was a wonderful intro into the world of dungeoncrawling. Fighting rats, snakes, mummy's, searching for keys and weapons. I could play this game for ages.

[youtube - gateway to apshai](https://www.youtube.com/embed/dpAJuFVCBYY);

# Movie Creator a Fisher-Price game from Spinnaker

A wonderful little program for making movies.

Scenes:

*   City
*   Lighthouse and ocean
*   Field, tree, and pond
*   Planet, lander, space
*   Living room
*   Castle
*   Gas station
*   Airport
*   Football Field
*   Apartment
*   Blank green horizon
*   Black
*   Grey

Actors:

*   Man
*   Woman
*   Boy
*   Alien
*   Football Player
*   Dog
*   Car
*   Van
*   Bird
*   Helicopter
*   Plane
*   Tie-Fighter Clone
*   U.F.O.
*   Sailboat
*   Tugboat
*   Hot Air Balloon
*   Kite
*   Pine Tree
*   Fence
*   Cloud
*   House
*   Storefront
*   Business building
*   Church
*   Circle
*   Smaller Circle
*   Square
*   Triangle

Other features include:

*   "Undo" under OOPS!
*   Online help
*   Ability to change colors
*   Small movements for actors
*   Recording up to 8 tracks of actors

[Spinnaker](https://en.wikipedia.org/wiki/Spinnaker_Software)

REDACTED