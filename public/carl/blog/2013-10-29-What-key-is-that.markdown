---
layout: post
title:  "What key is that?"
date: 2013-10-29T17:52:00.002-07:00  
categories: 
---
```
showkey --scancodes
or
showkey --keycodes
or 
xev > somelogfile
```
Might have to install xorg-xev

See:

[https://wiki.archlinux.org/index.php/Extra_Keyboard_Keys](https://wiki.archlinux.org/index.php/Extra_Keyboard_Keys)
