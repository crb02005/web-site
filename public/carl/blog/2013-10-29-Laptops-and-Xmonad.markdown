---
layout: post
title:  "Laptops and Xmonad"
date: 2013-10-29T17:32:00.001-07:00  
categories: linux,arch,urxvt
---
The Asus laptop is gone. A friend at work wanted a touchscreen laptop so we worked out a deal. He bought an SSD and it seems to work for him. As for me I am back on the E430. It might have a few problems with the case, but it seems to work for now. I switched back to playing with Xmonad. I plan on posting a few smaller posts as I discover bits that I need to document for my reference.

Why Carl can't paste: 

If you look at the config from ArchWiki you might use "urxvt" as your terminal. I haven't delved into why they picked that terminal, but I (re)discovered that copy/paste traditional "gnome/windows" ctrl-c ctrl-v doesn't work in urxvt. I've hit this issue before and thought I would document for posterity.

