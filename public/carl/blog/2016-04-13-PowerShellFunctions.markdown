---
layout: post
title:  "PowerShellFunctions"
date: 2016-04-13T14:22:00.001-07:00  
categories: powershell
---

PowerShell Functions:

Recently I made a
[https://gitlab.com/crb02005/power-shell-functions](https://gitlab.com/crb02005/power-shell-functions)
* NOTE The above was on another host and has been moved to gitlab 10/30/2020

PowerShellFunctions

 In a previous post I talked about one of the functions used Display-Calendar. I have since updated it to utilize the appropriate verb show. For a list of appropriate PowerShell verbs look no further than:
 
[Approved Verbs](https://msdn.microsoft.com/en-us/library/ms714428.aspx)






I have recently updated it to include:

*   PS-Windows-Functions
    *   Sync-WindowsTime
*   PS-File-Functions
    *   ConvertFrom-Base64
    *   ConvertTo-Base64
    *   Write-CompressedFiles
*   PS-Calendar-Functions
    *   Show-Calendar
*   PS-Structured-Query-Language-Functions
    *   Get-Sql
    *   Show-DatabasesWithTable
*   PS-GUI-Functions
    *   Set-Clipboard

## PS-Windows-Functions

### Sync-WindowsTime

This starts the Windows Time service and calls w32tm /resync Why might you use such a command? Perhaps you dual boot and the time is off after rebooting. See: [KB2385818](https://support.microsoft.com/en-us/kb/2385818) .

## PS-File-Functions

### ConvertTo-Base64

This takes a input and produces a Base64 string:


```
echo "Look of disapproval" | ConvertTo-Base64
```

Results in: TG9vayBvZiBkaXNhcHByb3ZhbA0K


ConvertFrom-Base64

```
"TG9vayBvZiBkaXNhcHByb3ZhbA0K" | ConvertFrom-Base64
```

Results in: Look of disapproval

Write-CompressedFiles

Takes a directoryPath and writes a zip file in the destinationFile location.

```
Write-CompressedFile $directoryPath $destinationFile</span>
```

I use this to zip up my module directory and save to my cloud:

```
Write-CompressedFile $env:PSModulePath.Split(";")[0] $ExportCompressedModuleFilePath $true
```



PS-Calendar-Functions

[Show-Calendar]({{site.url}}/#/carl/blog/2016-03-12-calendar-in-powershell)


PS-Structured-Query-Language-Functions

Get-Sql

This one is probably one of my favorite functions in the library. Here is an example:
```
Get-Sql "mycoolserver" "mydb" "select * from widgets"
```

You can pipe it to something like:

```
Get-Sql "mycoolserver" "mydb" "select * from widgets" | Out-GridView
```

Or make a query that calls a query:

```
Get-Sql "mycoolserver" "mydb" "select serverName, databaseName from linkedServerTable" | %{Get-Sql $_.serverName $_.databaseName "Select * from Widgets"}
```

Hopefully your database doesn't do that, but if it does it is nice to know PowerShell can handle it.

Show-DatabasesWithTable
Sometimes you need to find a database with a specific table:


DatabasesWithTable "mycoolserver" "importantTableName"

Gives you a list of databases with the appropriate table.

As we learned from the previous example you can chain these via the pipe line so you could run a query for each database.
