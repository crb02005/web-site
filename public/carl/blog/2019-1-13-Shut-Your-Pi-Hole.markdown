---
layout: post
title:  "Shut Your Pi Hole"
date: 2019-1-10T12:35:06-06:00  
categories: advertising
---

After my wife attempted to view a website to get some simple information she was bombarded with so many ads she couldn't read the content. A long time ago she had used Ad Block. While that might still work, for one device, I thought it might be better to see what other options are out there.

# PI Hole

[https://pi-hole.net/](https://pi-hole.net/)

If you have an extra Raspberry Pi around the house and don't know what do it with it, then maybe you could use it as an advertising black hole.

