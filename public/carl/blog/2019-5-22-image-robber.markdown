---
layout: post
title: "Window Selection, Image Capture, and OCR "
date: 2019-05-22T17:50:06-05:00
categories: linux,ubuntu
---

I made a little project which leverages some existing Linux tools for capturing images:

[https://gitlab.com/crb02005/image-robber](https://gitlab.com/crb02005/image-robber) *EDIT* this was moved from github and added to gitlab 10/27/2020

From the README I wrote:

# ImageRobber

This is a set of scripts to help snagging images

## Requirements

### python3.7

if you don't have this installed here: 

```
#!/usr/bin/python3.7
```

you will have a bad time

### xdotool

This gets the mouse location.

### image_magick

used for capturing images

### tesseract

ocring

## What the scripts do?

### install.sh

makes a ~/.imageRobber/settings.json
copies scripts to /usr/bin/

### jread 

Reads a JSON key from file. It only works on a flat dictionary.

```
jread <file> <key>
```

### jwrite

Writes a JSON key to file. It only works on a flat dictionary.

```
jwrite <file> <key> <value>
```

### store_mouse_location

Saves the mouse x/y in settings.json

```
store_mouse_location <position (0|1)>
```

### import_expression_builder

```
import_expression_builder <x0> <x1> <y0> <y1>
```

gets the expression for import

### capture_screen_shot

captures a subset of the screen from settings

```
import -window root -crop <expression from x0 x1 y0 y1 from settings> <date stamp>.<extension from settings>"
```

## How to use

keyboard bind "store_mouse_location 0" to a key command
keyboard bind "store_mouse_location 1" to a key command
keyboard bind "capture_screen_shot" to a key command

Enable OCR on output
keyboard bind "jwrite $HOME/.imageRobber/settings.json "convert to text" 1

Disable OCR on output
keyboard bind "jwrite $HOME/.imageRobber/settings.json "convert to text" 0

