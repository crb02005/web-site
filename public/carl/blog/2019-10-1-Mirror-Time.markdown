---
layout: post
title:  "Mirror Time"
date: 2019-10-1T18:25:00-07:00  
categories: linux
---

It has been a long time since I've posted. There has been a lot to do at my
day job, I've had some family issues, and took a vacation.

I've been setting up an old laptop and when I installed ARCH the mirror list had a dud.

I thought there is probably something that can update the repos. Well, there is:

```
sudo reflector --country US --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

That will need to be re-run at intervals to always choose the latest.
