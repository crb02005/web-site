---
layout: post
title:  "Side Projects"
date: 2017-05-30T20:12:57-07:00  
categories: pickup,radio,guitar
---
## WW II Radio

My parents came to visit this weekend. He brought oldest boys a radio kit. One was a WW II fox hole radio and the other was a crystal radio. I've hooked up a lot of crystal radios and never really gotten a radio signal from any of them, so I started helping them put together the WW II style one. It uses a pencil lead and a razor to make a diode. A tube slightly larger in diameter from a bathroom tissue roll serves as a winding aid for the air coil. Some sand paper across the coating of coil in a line allows for making a connection and tuning. I hooked it up and once again I could only hear a popping noise. I tried numerous things. Clipping the ground to a cold water pipe. Making a long antenna. Nothing seemed to improve it. So I waited for night and tried to repeat. I've built FM tuners which have a power source and they worked. At least I heard something this time.

## Guitar pickup

I ordered the same wire used fender which was much thinner than my first coil. Since my father was in town and we had made one coil for the radio project I had him hold the wire spool as I rewound the coil with the thinner wire. It took several times to get the tension right but soon we fell into a groove where we didn't break the hair thin wire. Once we had finished I hooked up some crocodile clips and held it over the guitar. It sounded as loud as the built in pickup. I unscrewed the case and looked at the prewired pick guard. The guitar I was playing with was an inexpensive Ammooon guitar I had gotten with the intent to fully customize. I had to clip the strings to take the guard off. I realized when I built the coil originally I had made it for the depth of a cigar box not for a routed strat style guitar. The coil was too big. So now I will probably need to make a smaller one or cut out more of the wood.

