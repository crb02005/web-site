---
layout: post
title:  "Golem ECS to ES"
date: 2018-10-14T19:44:22-07:00  
categories: csharp,ddd
---
I've been building a game engine and using it to try out different concepts. 

REDACTED.

One of the concepts I was using was CQRS and what goes really well with CQRS? Event sourcing. In case you haven't heard of event sourcing check out:

[https://speakerdeck.com/robsmallshire/event-sourced-domain-models-in-python](https://speakerdeck.com/robsmallshire/event-sourced-domain-models-in-python)

It is in Python, but the concepts are good. I haven't overhauled everything quite to the pattern but it is getting there. I started out with a traditional database design and then converted it to a Entity Component System which is kind of an object oriented approach to databases, but with Event Sourcing that becomes irrelevant, it is more transactional and accounting centric. Another feature is using events, this serves to decouple. I'm going to be working towards the chart found on slide thirty. The link at the bottom didn't work but there is a pdf version here:

[http://domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf](http://domainlanguage.com/wp-content/uploads/2016/05/DDD_Reference_2015-03.pdf)

I need to strike a better balance between fiddling with the architecture bits, and building out what I really want to build. At the moment I'm spending more time in the architecture. 

It may seem like I'm rolling my own on a few things such as the ECS and ES but I like to do that first before converting it to using an off the shelf component. Being able to roll back the abstraction and fix the parts under the cover is something best done on a hobby project like this instead of a serious project where lives or money hang in the balance.

If you want to seriously look at ECS probably a study of Unity would be the thing to do. For an off the shelf ES system look at [https://eventstore.org/](https://eventstore.org/)

Further reading:

* [https://martinfowler.com/eaaDev/EventSourcing.html](https://martinfowler.com/eaaDev/EventSourcing.html)

Further watching:

* [Event Sourcing https://youtu.be/aweV9FLTZkU](https://youtu.be/aweV9FLTZkU)
* [Event Driven https://youtu.be/STKCRSUsyP0](https://youtu.be/STKCRSUsyP0)
