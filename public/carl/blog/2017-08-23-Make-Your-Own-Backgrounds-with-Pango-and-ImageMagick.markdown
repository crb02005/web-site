---
layout: post
title:  "Make Your Own Backgrounds with Pango and ImageMagick"
date: 2017-08-23T22:00:08-07:00  
categories: pango
---
First install imagemagick

[ImageMagick](https://www.archlinux.org/packages/extra/i686/imagemagick/)

Install your fonts of choice.

Here is one I like:

[Roboto](https://www.archlinux.org/packages/community/any/ttf-roboto/)

You can either type it every time or keep some settings you like in a script like:

createimagefrompango

    convert -font Roboto-Black -size 1920x1080 -background white -gravity Center  pango:"$1" $2

Drop that in your

    /usr/bin/createimagefrompango

Then use:

    createimagefrompango "some text for render" someimage.png

If you've got a file of formated pango:

    createimagefrompango @yourfile someimage.png

## Documentation

[pango](http://www.pygtk.org/docs/pygtk/pango-markup-language.html)

[pango gnome](https://developer.gnome.org/pango/stable/PangoMarkupFormat.html)

Then you have your new image.

![Some Image]({{site.url}}/static/images/pango_image.png)

