---
layout: post
title:  "So many projects so little time"
date: 2017-01-09T22:26:00.002-07:00  
categories: projects
---
*   Arduino Display project
*   Teenage Engineering Synths song programming
*   Raspberry PI cluster
*   Splitting the Paper Picker into sub repos

Lots to do. I had to work late today so I didn't get started writing a blog today until late. So there is that... I'm going to stop worrying about the number of things to do and just pick what is fun to work on. Tonight that is going to be the Arduino project.

Working on the PBM format to make it work for the Arduino display.

*   Carl