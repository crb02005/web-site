---
layout: post
title: "Changing Ubuntu's Lock Background"
date: 2019-4-10T19:45:06-06:00
categories: ubuntu
---



Copy the css file:

```
cp /etc/alternatives/gdm3.css ~/Deskop
```

Edit it
```
code ~/Desktop/gdm3.css
```

Find the "lockDialogGroup"

Change it as needed.

```
background-image: url(file:///storage/theme/cthulhu.svg);
```

Restart.... or 

```
gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell --method org.gnome.Shell.Eval 'Main.loadTheme();'
```
 or

```
ALT-F2
restart
```
