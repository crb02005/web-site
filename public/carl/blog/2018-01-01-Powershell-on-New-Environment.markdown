---
layout: post
title:  "Powershell on New Environment"
date: 2018-01-01T21:10:36-07:00  
categories: powershell,windows
---
Open Powershell ISE

    Win+R

    powershell_ise

Powershell ISE appears.

    cat $profile

If you haven’t setup a powershell profile you’ll get a message like:

    cat : Cannot find path 'C:\Users\yourUserHere\Documents\WindowsPowerShell\Microsoft.PowerShellISE_profile.ps1' because it does not exist.

You will need to create the file:

    echo "" > C:\Users\yourUserHere\Documents\WindowsPowerShell\Microsoft.PowerShellISE_profile.ps1

If you get a message like:

    out-file : Could not find a part of the path 'C:\Users\yourUserHere\Documents\WindowsPowerShell\Microsoft.PowerShellISE_profile.ps1'.

It means the directory doesn’t exist yet.

    New-Item -ItemType Directory -Path 'C:\Users\yourUserHere\Documents\WindowsPowerShell\'

Will create it.

Then creating the profile is as easy as re-running:

    echo "" > C:\Users\yourUserHere\Documents\WindowsPowerShell\Microsoft.PowerShellISE_profile.ps1

It won’t do much, but we can fix that.

[Here]({{site.url}}/#/carl/blog/2016-04-13-PowerShellFunctions) are some functions I’ve found useful.

This will show you were your system is looking for modules:

    echo $env:PSModulePath.Split(";")

This will print out the valid directories

    $env:PSModulePath.Split(";") | %{if(Test-Path $_){echo $_}}

You can do some research on where you want to put them, but I would go ahead and make that missing directory:

    New-Item -ItemType Directory -Path 'C:\Users\yourUserHere\Documents\WindowsPowerShell\Modules'

Unblock the Zip after downloading.

Edit your profile:

    ise $profile

Here is what I have in mine:

    #Paths to Register

    @("C:\Users\yourUserName\AppData\Local\Programs\Python\Python36",
    "C:\Users\yourUserName\AppData\Local\Programs\Python\Python36\Scripts",
    "C:\Program Files (x86)\Pandoc",
    "C:\Program Files (x86)\MiKTeX 2.9\miktex\bin\"
    ) | %{ 
        if($env:Path -notlike ("*;{0}*" -f $_)){
            $env:Path += (";{0}" -f $_)
        }
    }

    function Get-GUID(){
       [System.Guid]::NewGuid().toString()
    }

    function md-to-pdf($infile, $outfile){
        pandoc.exe $infile --latex-engine=xelatex -o $outfile
    }

    Import-Module PS-File-Functions -Force -Verbose
    Import-Module PS-Calendar-Functions -Force -Verbose
    Import-Module PS-Structured-Query-Language-Functions -Force -Verbose
    Import-Module PS-GUI-Functions -Force -Verbose
    Import-Module PS-Python-Functions -Force -Verbose

    $local = New-Object -TypeName PSObject
    $local | Add-Member -MemberType NoteProperty -Name CompressedModuleFilePath -value "C:\Users\yourUserName\Google Drive\Powershell.$env:COMPUTERNAME.zip"
    $local | Add-Member -MemberType NoteProperty -Name ExportProfileFilePath -value "C:\Users\yourUserName\Google Drive\Powershell.profile.$env:COMPUTERNAME.ps1"

    # Create compressed version for desktop
    Write-CompressedFile $env:PSModulePath.Split(";")[0] $local.CompressedModuleFilePath $true

    # Export Profile
    cp $profile $local.ExportProfileFilePath

    # For now set dir to modules to work with
    cd $env:PSModulePath.Split(";")[0]

If you’ve got issues:

    C:\Users\yourUserName\Documents\WindowsPowerShell\Microsoft.PowerShellISE_profile.ps1 cannot be loaded because running scripts is disabled on this system.

You can pick how you want to handle this.

If everything works fine you should see some blue text when you start up:

    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-File-Functions\PS-File-Functions.psd1'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-File-Functions\PS-File-Functions.psm1'.
    VERBOSE: Importing function 'ConvertFrom-Base64'.
    VERBOSE: Importing function 'ConvertTo-Base64'.
    VERBOSE: Importing function 'Write-CompressedFile'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-Calendar-Functions\PS-Calendar-Functions.psd1'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-Calendar-Functions\PS-Calendar-Functions.psm1'.
    VERBOSE: Importing function 'Show-Calendar'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-Structured-Query-Language-Functions\PS-Structured-Query-Language-Functions.psd1'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-Structured-Query-Language-Functions\PS-Structured-Query-Language-Functions.psm1'.
    VERBOSE: Importing function 'Get-Sql'.
    VERBOSE: Importing function 'Show-DatabasesWithTable'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-GUI-Functions\PS-GUI-Functions.psd1'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-GUI-Functions\PS-GUI-Functions.psm1'.
    VERBOSE: Importing function 'Set-Clipboard'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-Python-Functions\PS-Python-Functions.psd1'.
    VERBOSE: Loading module from path 'C:\Users\yourUserName\Documents\WindowsPowerShell\Modules\PS-Python-Functions\PS-Python-Functions.psm1'.
    VERBOSE: Importing function 'ConvertFrom-Yaml'.
    VERBOSE: Importing function 'Get-RedditSubmissions'.