---
layout: post
title:  "Random Table Portable Library, Windows 10 Application, and Web Port in C#"
date: 2015-09-01T21:05:00.002-07:00  
categories: gaming,gurps,pen and paper
---
Yesterday, I checked in a project I've spent a few weeks playing with:  

REDACTED 

This is a utility for rolling random tables with support for connecting dependent tables. If you don't see the purpose you might not have played a pen and paper role playing game. Let's say you were running a space type game and you wanted to see what space craft were near by you might have a table that looks like this:  

Tables - Space Craft Encounters (1d6)  
1-2   No Encounter  
3       Shuttlecraft  
4       Some kind of cargo freighter  
5       Assault Craft  
6       Roll on Unusual Space Craft Encounters  

You would roll a standard six sided die and get a result. This is fairly straight forward roll a one or two and nothing happens. Roll a three and get a shuttlecraft. Roll a four and get a cargo freighter. Roll a five get an assault craft. Roll a six, you would need to look at another table and roll on that. Depending on your game system this can be a somewhat tedious process. It is also difficult to organize multiple publishers of books tables and connect them.  

My software solves many of these problems. Currently only sample JSON files are included, but I am working on creating useful game system independent data-packs for my utility. I have also contacted several publishers and requested permission to create data packs.  

I've contacted AEG, Steve Jackson Games, Fantasy Flights, Troll Lords and Green Ronin. So far I've heard back from AEG and Steve Jackson Games. Both companies were polite and helpful.  

AEG indicated if I was not making any money then they were fine with creation.  

Steve Jackson Games online policy indicates that they will only allow tables already published.  

I plan on adding tables from both provided I have time. I will need to thoroughly research publicly available tables for SJG and that will take some time.  

How can I get this program you might be asking yourself. Currently I have a common library that is used by the Windows 10 Application and the Web application. I've currently got some test coverage. Until bugs are reported I plan on spending time adding content in the form of JSON files. I have a few minor features I'm planning on adding, but the core application I believe covers most cases. The application is written in C#. Given time and interest I might make a port to another language. After some additional testing I might look for hosting and deploy the web app.