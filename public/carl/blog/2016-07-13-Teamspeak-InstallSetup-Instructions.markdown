---
layout: post
title:  "Teamspeak Install/Setup Instructions"
date: 2016-07-13T19:30:00-07:00  
categories: teamspeak
---
# Teamspeak Install/Setup Instructions

## For Ubuntu

1.  Install PUTTY or SSH utility (optional)
2.  Install VirtualBox
3.  [Download latest server ISO from Ubuntu](http://www.ubuntu.com/) .
4.  Provision the Virtual Machine to match or exceed the recommendations for minimum system specifications
5.  Attach the ISO and install
    *   use entire disk no LVM
    *   install updates automatically
    *   standard system utilities
    *   OpenSSH (optional)
    *   GRUB on boot is fine for dedicated VM
6.  Add port forwarding to your NAT
    *   map a port for SSH
    *   map a UDP port for client port 9987
7.  Install Guest Additions
    1.  sudo mount -t iso9660 -o ro /dev/cdrom /media/cdrom
    2.  cd /media/cdrom
    3.  sudo apt-get -y install gcc make
    4.  sudo ./VBoxLinuxAdditions.run
    5.  sudo umount /media/cdrom
    6.  sudo /sbin/shutdown -r now
8.  update os
    *   sudo apt-get update
9.  clone vm so you don't need to install ubuntu again
    *   setup a different host name for each full clone
    *   setup a different port for openSSH so you can boot a few up at once
10.  start headless virtualbox instance
    *   ssh with PuTTY or another tool
11.  install teamspeak-server
    1.  Download from http://www.teamspeak.com/downloads
    2.  Click the copy link
    3.  wget and paste into putty  

            wget pastedurlhere

    4.  decompress the download  

            tar -xvf somedownload.bz2

    5.  start the server  

            ./ts3server_startscript.sh start

    6.  save off that screen that pops up it is important
        *   sign in from the TeamSpeak client
        *   add the network key from the screen that appeared
    7.  set the teamspeak server to startup on server

```
        crontab -e 
        @reboot
        /path/to/decompressedlocation/ts3server_startscript.sh start
```