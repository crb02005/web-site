---
layout: post
title:  "Building console apps"
date: 2018-04-27T19:08:56-07:00  
categories: npyscreen,windows,wsl,linux
---
I read an article a while back on reddit about a python library for building terminal apps.

[http://npyscreen.readthedocs.io/introduction.html](http://npyscreen.readthedocs.io/introduction.html)

It seemed pretty cool and I wanted to try it out. It worked fine on Linux, but I wanted to see if it would work on Windows.

Sadly it uses:

[https://en.wikipedia.org/wiki/Curses_(programming_library)](https://en.wikipedia.org/wiki/Curses_(programming_library))

Which is a great library, but the Python3 library for windows doesn't have it. What is a developer to do?

Well Windows has one thing going for it. It has Linux, well with a subsystem:

[https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux)

Getting it installed is only half the battle:

[https://docs.microsoft.com/en-us/windows/wsl/install-win10](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

You need Linux installed. How ironic is it to install a free OS from a walled garden like the Microsoft Store. Sometimes you can't avoid every problem.

In the words of Dr. Ian Malcom:

_"Life, Uh, finds a way"_

[https://superuser.com/questions/1271682/is-there-a-way-of-installing-windows-subsystem-for-linux-on-win10-v1709-withou](https://superuser.com/questions/1271682/is-there-a-way-of-installing-windows-subsystem-for-linux-on-win10-v1709-withou)

Well after getting Ubuntu installed I was almost ready. I tried Debian, but the user permissions wouldn't access my home directory without running as Admin and I didn't want to do that.

I just had to install python3, pip3 and then pip to install npyscreen. 

It worked just fine after that.
