---
layout: post
title:  "TNC Baofeng APRS Droid"
date: 2014-10-01T16:11:00.002-07:00  
categories: radio,electronics
---
A while back I ordered a Bluetooth TNC from [mobilinkd.com](http://www.mobilinkd.com/) and it arrived with a 3D printed case I had one problem. I forgot to order the cable, for some reason I thought I had one of the cables that I needed, but as it turns out I didn't after searching the internet I finally found what I thought was the right cable. I ordered it and almost a month later it arrived. The cable was too long and didn't work. I should have just ordered the one from mobilinkd for 9.99 and paid the 9.99 in shipping. Some times it doesn't pay to find a "Bargain".

I had recently rejoined the community of smart phone users after a one year hiatus. I got the APRSDroid app. I had to get a passcode and they got back to me pretty fast. I would have loved to try it out, but the cable was wrong. Its going to probably be a month before I order the right cable get it  shipped. Check back in a month or so for an update.
