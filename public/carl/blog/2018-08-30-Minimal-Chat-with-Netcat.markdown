---
layout: post
title:  "Minimal Chat with Netcat"
date: 2018-08-30T09:32:25-07:00  
categories: chat,peer,netcat
---
Recently I was setting up another computer and I wanted to set it up the same as my current machine and I wanted to send a list of my packages installed and didn't want to send and email, Facebook message, or open GoogleDrive, so I did some research. 

## Find the IP address of the computers

```
ip address show | grep -i inet
```

"ip address show"  is a way to find the ip address of a computer, and then piping it to grep and filtering down to strings containing inet cuts down some of the noise.

## Setup the first computer

```
nc -l 55555
```
[Explain shell for this](https://explainshell.com/explain?cmd=nc+-l+55555)

This starts netcat in listen mode for the port 55555. 

## Setup the next computer

```
nc <ipaddress_of_first_computer> 55555
```
This opens netcat to the other computer.

## Testing

Type some information in one of the terminals and press enter and it will show in the other terminal.

## Getting installed packages using pacman

```
pacman -Qqe > pkglist.txt
```

[https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#List_of_installed_packages](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#List_of_installed_packages)

```
pacman -S - < pkglist.txt
```

Will install packages from a list.
