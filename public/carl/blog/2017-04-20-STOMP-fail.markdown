---
layout: post
title:  "STOMP fail"
date: 2017-04-20T20:40:36-07:00  
categories: mq,stomp,websockets
---
About a month or two ago I started a project to make a web development skeleton. A sort of playground/quickstart for my other projects. I had switched to use WebSockets on Python with Flask instead of a standard WebApi. Things were going well. I had the idea to remove the Flask layer and make it talk directly to RabbitMQ. I read up on the STOMP protocol and made an attempt to use the STOMP framework from:

[stomp websocket](https://github.com/jmesnil/stomp-websocket)

I couldn't get it to work and thought maybe the library was at fault. It was written in CoffeeScript and I figured it could use a replacement. I started working on a replacement in TypeScript. Yes, I realize the irony in that. After playing around with multiple settings and ports and configuration options I hit a wall. I could nc to the RabbitMQ server and manually send command and get a reply but in the browser the websockets refused to connect.

Sending this via nc:

    CONNECT

    ^@

gets a reply from the server.

Tried switching from RabbitMQ to ActiveMQ. Same result. Tried installing the queues directly instead of via docker. Same result. I'm going to take a little break from my WebDevSkeleton and stop efforts on STOMP.



Thanks to my good friend at [8charmax](http://www.8charmax.com/) for spending time working on this with me. -Carl Burks