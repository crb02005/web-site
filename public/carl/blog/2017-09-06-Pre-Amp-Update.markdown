---
layout: post
title:  "Pre-Amp Update"
date: 2017-09-06T17:00:58-07:00  
categories: 
---

Last night I opened up the preamp and began to make some changes. Before I started I took an old Kodak printer apart with the intent of harvesting a power plug. It looked like right size for a typical guitar pedal so I took it apart.

![Kodak print board - Bobcat board]({{site.url}}/static/images/kodak_board.jpg)

Kodak print board - Bobcat board



If you look carefully at the board the bottom shows the power plug next to the metal USB plug. Lots of interesting things on this board. Looks like a realtime clock I might harvest later. As it turns out the power plug was the wrong size, but I plan on revisting the board to pull off a few parts. That crystal might be useful as well.

I looked through my parts bin and found some RCA connectors and thought it would be a really good thing to have a clean signal going into the mixer before it routes around the pedal board. Also having an RCA aux in doesn't hurt either. I fired up the drill and put some appropriately sized holes for the power, RCA, and a switch because I intended on making the Darlington pair optional. I plugged soldered the power to a 9v connector backwards so I could plug it into the battery lead and remove it to plug in the battery if needed. I realized then that the power was the wrong size. I left the hole because I plan on adding power at a later date. I need to start checking the components first. I pulled out the old transistor and replaced it with two. I liked how it sounded and decided to make the switch toggle one of the resistors to make the signal louder. After that I swapped the resistor with a fast switching RGB LED after switching it out I tested it and was surprised to hear what sounded like a synthizer, the LED was too noisy and I replaced it with a standard red LED.

![Preamp - Labeled]({{site.url}}/static/images/front_preamp_marked_up.jpg)

Preamp - Labeled



![Preamp]({{site.url}}/static/images/front_preamp.jpg)

Preamp



![Preamp - top]({{site.url}}/static/images/top_preamp.jpg)

Preamp - top



![Preamp - Side Shot]({{site.url}}/static/images/side_shot_preamp.jpg)

Preamp - Side Shot



![Preamp - Other Side Shot]({{site.url}}/static/images/side_shot_2_preamp.jpg)

Preamp - Other Side Shot

