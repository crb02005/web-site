---
layout: post
title:  "npm package"
date: 2020-11-21T19:48:00-06:00  
categories: npm,javascript
---

Recently I published an NPM package. I did this in preparation for a talk I am going to do for the Central Arkanas Javascript virtual meeting.

Here is a preview of my tech talk: [https://gitlab.com/crb02005/tech-talks](https://gitlab.com/crb02005/tech-talks)

Here is a link to my NPM package:

[https://www.npmjs.com/package/trocar-dice-js](https://www.npmjs.com/package/trocar-dice-js)

Okay, so what does it do?

Well after:

```
npm install trocar-dice-js
````

You can import it:

```
import dice from 'trocar-dice';
```

Then you can use it:

```
console.log(dice.d20());
 
console.log(dice.dice(4,6));
```

