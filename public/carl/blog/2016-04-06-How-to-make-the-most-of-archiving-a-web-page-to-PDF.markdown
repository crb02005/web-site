---
layout: post
title:  "How to make the most of archiving a web page to PDF"
date: 2016-04-06T19:05:00.001-07:00  
categories: pdf
---
When you work in a technology industry and like to code your own pet projects there is often overlap. Sometimes when I am at work and I find an interesting to which article I would like to dedicate further research, I email myself a link.   

When I don't have time to fully digest the page, I like to save the content for later. As the web is a dynamic ever changing environment often sites change owners, or newer content takes the place. I like to archive them into PDF.   

Typically the web page is filled with clutter, but some browsers offer a "Reading Mode"  

Internet Explorer 11:  

[https://blogs.msdn.microsoft.com/ie/2014/03/04/introducing-reading-view-in-ie-11/](https://blogs.msdn.microsoft.com/ie/2014/03/04/introducing-reading-view-in-ie-11/)  

Edge:  
[](https://www.blogger.com/goog_1431102083)  
[http://windows.microsoft.com/en-us/windows-10/getstarted-take-your-reading-with-you](http://windows.microsoft.com/en-us/windows-10/getstarted-take-your-reading-with-you)  

Firefox (proposed):  
[](https://www.blogger.com/goog_1431102087)  
[https://wiki.mozilla.org/Reading_Mode](https://wiki.mozilla.org/Reading_Mode)  

Chrome (also in the works)  
[http://lifehacker.com/enable-the-new-hidden-reader-mode-in-chrome-for-andro-1666469700](http://lifehacker.com/enable-the-new-hidden-reader-mode-in-chrome-for-andro-1666469700)  

Turning on reader mode increases the quality of your archive through noise reduction.