---
layout: post
title:  "Docker Links"
date: 2018-09-08T20:11:15-07:00  
categories: docker
---
The day after I posted my Docker slide deck I came across this tutorial from HackADay I thought it was worth posting here:

[https://hackaday.com/2018/09/05/intro-to-docker-why-and-how-to-use-containers-on-any-system/](https://hackaday.com/2018/09/05/intro-to-docker-why-and-how-to-use-containers-on-any-system/)

[Hello World Repo](https://hub.docker.com/_/hello-world/)

I also found some other resources:

Resources from Docker's Site:
[https://www.docker.com/resources](https://www.docker.com/resources)


You might checkout the Docker subreddit:

[https://www.reddit.com/r/docker/](https://www.reddit.com/r/docker/)

Or the Docker Slack Channel:
[https://community.docker.com/registrations/groups/4316](https://community.docker.com/registrations/groups/4316)

### Repos 
### Languages
* [erlang](https://hub.docker.com/_/erlang/)
* [elixir](https://hub.docker.com/_/elixir/)
* [go](https://hub.docker.com/_/golang/)
* [groovy](https://hub.docker.com/_/groovy/)
* [jruby](https://hub.docker.com/_/jruby/)
* [openjdk](https://hub.docker.com/_/openjdk/)
* [perl](https://hub.docker.com/_/perl/)
* [php](https://hub.docker.com/_/php/)
* [python](https://hub.docker.com/_/python/)
* [pypy](https://hub.docker.com/_/pypy/)
* [ruby](https://hub.docker.com/_/ruby/)

### OS

#### Linux
* [AmazonLinux](https://hub.docker.com/_/amazonlinux/)
* [Alpine](https://hub.docker.com/_/alpine/)
* [bash](https://hub.docker.com/_/bash/)
* [BusyBox](https://hub.docker.com/_/busybox/)
* [CentOs](https://hub.docker.com/_/centos/)
* [Debian](https://hub.docker.com/_/debian/)
* [Fedora](https://hub.docker.com/_/fedora/)
* [OpenSUSE](https://hub.docker.com/u/opensuse/)
* [Ubuntu](https://hub.docker.com/_/ubuntu/)

#### Microsoft's Docker Images

[https://hub.docker.com/r/microsoft/](https://hub.docker.com/r/microsoft/)

Mono:

[https://hub.docker.com/_/mono/](https://hub.docker.com/_/mono/)


### Productivity Repos

#### Orchestration
* [consul](https://hub.docker.com/_/consul/)
* [swarm](https://hub.docker.com/_/swarm/)

#### Web Servers, server utilities, and frameworks
* [httpd](https://hub.docker.com/_/httpd/)
* [jetty](https://hub.docker.com/_/jetty/)
* [kong](https://hub.docker.com/_/kong/)
* [nats](https://hub.docker.com/_/nats/)
* [nats streaming](https://hub.docker.com/_/nats-streaming/)
* [nginx](https://hub.docker.com/_/nginx/)
* [node](https://hub.docker.com/_/node/)
* [haproxy](https://hub.docker.com/_/haproxy/)
* [httpd plus php](https://hub.docker.com/_/php/)
* [rethinkdb](https://hub.docker.com/_/rethinkdb/)
* [tomcat](https://hub.docker.com/_/tomcat/)
* [traefik](https://hub.docker.com/_/traefik/)

#### Stream
* [flink](https://hub.docker.com/_/flink/)

#### Data Analytics
* [chronograf](https://hub.docker.com/_/chronograf/)
* [ElasticSearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)
* [Kibana](https://hub.docker.com/_/kibana/)
* [matomo](https://hub.docker.com/_/matomo/)
* [Solr](https://hub.docker.com/_/solr/)


#### Logging
* [logstash](https://www.elastic.co/guide/en/logstash/current/docker.html)
* [sentry](https://hub.docker.com/_/sentry/)

#### Secret Storage
* [vault](https://hub.docker.com/_/vault/)

#### Data Storage Tools
* [adminer](https://hub.docker.com/_/adminer/)
* [telegraf](https://hub.docker.com/_/telegraf/)

#### Data Storage
* [arangodb](https://hub.docker.com/_/arangodb/)
* [cassandra](https://hub.docker.com/_/cassandra/)
* [couchbase](https://hub.docker.com/_/couchbase/)
* [couchdb](https://hub.docker.com/_/couchdb/)
* [cratedb](https://hub.docker.com/_/crate/)
* [influx](https://hub.docker.com/_/influxdb/)
* [memcached](https://hub.docker.com/_/memcached/)
* [mariadb](https://hub.docker.com/_/mariadb/)
* [Mongo](https://hub.docker.com/_/mongo/)
* [MySql](https://hub.docker.com/_/mysql/)
* [neo4j](https://hub.docker.com/_/neo4j/)
* [Percona](https://hub.docker.com/_/percona/)
* [postgres](https://hub.docker.com/_/postgres/)
* [Redis](https://hub.docker.com/_/redis/)
* [zookeeper](https://hub.docker.com/_/zookeeper/)

#### Queues
* [eclipse mosquitto](https://hub.docker.com/_/eclipse-mosquitto/)
* [rabbitmq](https://hub.docker.com/_/rabbitmq/)

#### Build Servers and utilities
* [buildpack](https://hub.docker.com/_/buildpack-deps/)
* [gradle](https://hub.docker.com/_/gradle/)
* [jenkins](https://hub.docker.com/_/jenkins/)
* [sonarqube](https://hub.docker.com/_/sonarqube/)

#### Project Management
* [maven](https://hub.docker.com/_/maven/)
* [redmine](https://hub.docker.com/_/redmine/)

#### Clouds
* [OwnCloud](https://hub.docker.com/r/owncloud/server/)
* [nextcloud](https://hub.docker.com/_/nextcloud/)

#### CMS/Blogging/Etc
* [drupal](https://hub.docker.com/_/drupal/)
* [ghost](https://hub.docker.com/_/ghost/)
* [joomla](https://hub.docker.com/_/joomla/)
* [WordPress](https://hub.docker.com/_/wordpress/)

#### Documentation
* [MediaWiki](https://hub.docker.com/_/mediawiki/)

#### Communication
* [RocketChat](https://hub.docker.com/_/rocket.chat/)
* [TeamSpeak](https://hub.docker.com/_/teamspeak/)
