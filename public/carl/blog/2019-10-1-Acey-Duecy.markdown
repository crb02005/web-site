---
layout: post
title:  "Acey Duecy"
date: 2019-10-1T18:25:00-07:00  
categories: basic, python
---

What I've been fascinated with lately is the "Commander X16" it is a reinvisioning of a Commodore-like computer. There is an emulator for it already:

```
https://snapcraft.io/install/x16emu/arch
```

I found an old computer book which has several basic programs, I thought it might be a fun excerise to show an old program then try to rewrite it in another language.

I found these stickers which I think would help me remember which symbol goes with the keys:

[https://www.4keyboard.com/commodore-keyboard-stickers/585-commodore-commodore-64-non-transparent-keyboard-stickers.html](https://www.4keyboard.com/commodore-keyboard-stickers/585-commodore-commodore-64-non-transparent-keyboard-stickers.html)

After doing some rewriting from the code I ended up with a tighter version of AceyDuecy than what I found in the book.

AceyDuecy for the uninitiated is a game in which you draw two cards. You then bet if the next card is going to be between the two cards. I modified the code from the original program by:

Bill Palmby of Prairie View, Illinois

Here it is:

```
5 ? "MODIFIED FROM ORIGINAL BY CARL BURKS"
10 ? TAB(26);"ACEY DUCEY CARD GAME"
20 ? TAB(15);"CREATIVE COMPUTING MORRISTOWN, NEW JERSEY"
21 FOR I=1 TO 3:?"":NEXT
30 ?"ACEY-DUCEY IS PLAYED IN THE FOLLOWING MANNER"
40 ?"THE DEALER(COMPUTER) DEALS TWO CARDS FACE UP"
50 ?"YOU HAVE AN OPTION TO BET OR NOT TO BET DEPENDING"
60 ?"ON WHETHER OR NOR YOU FEEL THE CARD WILL HAVE"
70 ?"A VALUE BETWEEN THE FIRST TWO"
80 ?"IF YOU DO NOT WANT TO BET, INPUT A 0"
90 DEF FN CARD(A)=INT(13*RND(1))+2
100 N=100
110 Q=100:REM MONEY
120 ?"YOU NOW HAVE ";Q;" DOLLARS":REM ROUND START
130 ?""
260 ?"HERE ARE YOUR NEXT TWO CARDS"
270 A=FN CARD(0)
300 B=FN CARD(0)
330 IF (A+3)>(B) THEN 270:REM NEED DIFFERENT CARDS TO PLAY
335 ?"FIRST CARD"
340 C=A
350 GOTO 370
360 ?"SECOND CARD"
365 C=B
370 IF C<11 THEN 400:REM SKIP SPECIAL FACE CARD LOGIC IF REGULAR CARD
380 IF C>13 THEN 480
370 IF C>12 THEN 460
390 IF C>11 THEN 440
395 IF C=11 THEN 420
400 ? C
410 GOTO 490:REM SHOULD START BETTING
420 ?"JACK"
430 GOTO 490:REM SHOULD START BETTING
440 ?"QUEEN"
450 GOTO 490:REM SHOULD START BETTING
460 ?"KING"
470 GOTO 490:REM SHOULD START BETTING
480 ?"ACE"
490 IF B<>C THEN 360
500 INPUT"WHAT IS YOUR BET";M:REM START BETTING
510 IF M>1 THEN 550:NO BET OR INVALID NEGATIVE BET
520 ?"CHICKEN!!"
530 ?""
540 GOTO 120:REM ROUND START
550 IF M<=Q THEN 580:REM LAST CARD
560 ?"YOU'VE BET TOO MUCH"
570 GOTO 500
580 Q=Q-M
585 C=FN CARD(0)
586 ?"LAST CARD"
590 IF C<11 THEN 640
600 IF C>13 THEN 700
610 IF C>12 THEN 690
620 IF C>11 THEN 680
630 IF C=11 THEN 660
640 ?C
650 GOTO 740:REM CHECKWON
660 ?"JACK"
670 GOTO 740:REM CHECKWON
680 ?"QUEEN"
690 GOTO 740:REM CHECKWON
700 ?"KING"
710 GOTO 740:REM CHECKWON
720 ?"ACE"
740 IF C<A THEN 780:REM LOSE
750 IF C>B THEN 780:REM LOSE
760 Q=Q+(M*2)
765 ?"YOU WIN"
770 GOTO 120
780 ?"YOU LOST" 
790 IF Q>0 GOTO 120
800 ?"YOU ARE OUT OF MONEY."
810 SYS$FFFF:REM CLOSE THE EMU
```

I moved the card selection to a function, looped the first two card print outs and added some REM statements for comments. I'm sure there are a lot of other optimizations which could be made. I'm just getting back into to the swing of things.

I'm able to run it in the emulator:

```
x16emu -bas acey-ducey.bas -run
```

When the program reaches line 810 it closes the emulator.
If I saw that in the documentation I must have missed it, but the repo maintainers were really responsive:

[https://github.com/commanderx16/x16-emulator/issues/151](https://github.com/commanderx16/x16-emulator/issues/151)

This lets me make a shortcut which launches the program then closes it when the game is over.

Here is a python implementation:

```
from random import randint 

print("ACEY DUCEY CARD GAME")

print("""ACEY-DUCEY IS PLAYED IN THE FOLLOWING MANNER
   THE DEALER(COMPUTER) DEALS TWO CARDS FACE UP
   YOU HAVE AN OPTION TO BET OR NOT TO BET DEPENDING
   ON WHETHER OR NOR YOU FEEL THE CARD WILL HAVE
   A VALUE BETWEEN THE FIRST TWO
   IF YOU DO NOT WANT TO BET, INPUT A 0""")

lookup = {11:"JACK",12:"QUEEN",13:"KING",14:"ACE"}

def getCardDescription(card):
    return card if card < 11 else lookup[card] 
def card():
    return randint(2,14)

money = 100

while money > 0:
    print(f"YOU HAVE {money} DOLLARS.")
    cards = sorted([card() for _ in range(1,3)])
    if(cards[0]-cards[1]):
        next
    [print(getCardDescription(_)) for _ in cards]
    bet = money+1
    while bet > money and bet >1:
        bet = int(input("BET?"))
    if bet > 0:
        flop = card()
        print(getCardDescription(flop))
        if flop >= cards[0] and flop <= cards[1]:
            print("WINNER")
            money += bet
        else:
            money -= bet
            print("YOU LOSE!")
    else:
        print("CHICKEN!")
print("YOU ARE OUT OF MONEY")
```

Looks like a version is for sale:

[https://www.kidwaresoftware.com/SmallBasicComputerGames/](https://www.kidwaresoftware.com/SmallBasicComputerGames/)

And Microsoft published a version of it here:

[https://social.technet.microsoft.com/wiki/contents/articles/16398.basic-computer-games-small-basic-edition-acey-ducey.aspx](https://social.technet.microsoft.com/wiki/contents/articles/16398.basic-computer-games-small-basic-edition-acey-ducey.aspx)

It is available here:
[https://archive.org/details/Basic_Computer_Games_1978_David_Ahl/page/n7](https://archive.org/details/Basic_Computer_Games_1978_David_Ahl/page/n7)

So what is to be learned? 

This program is a great example of a minimum viable product. You don't need
the suit of the card so it is entirely irrelevant and thus the code can simply pick a number betwen 2 and 14.

Old code, and old ideas are worth an investigation to provide a lesson.

I'm thinking of making a graphical version on both systems. PyGame for the python version.

It would also be interesting if the odds changed based on how unlikely the card was to be within range.
