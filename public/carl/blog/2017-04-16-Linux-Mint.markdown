---
layout: post
title:  "Linux Mint"
date: 2017-04-16T01:09:00-07:00  
categories: linux,mint
---
Sadly my Ubuntu install was borked after attempting to upgrade to 17\. After this experience I thought it best to look at other options. I previously had used Arch, Fedora, and others. I gave Linux Mint a chance a while, back and once again it was time to give it a whirl. Using a USB drive I booted my old install and grabbed what files I could and copied them to an external drive. After repartitioning it was time install. It worked fairly well and then it was time to load back up a few key applications:

*   GitKraken
*   Visual Studio Code
*   Spotify
*   PcGen
*   Pandoc

After installing curl GitKraken started working. Spotify, and VS Code worked without tweaking. PcGen didn't run from the .sh file but looking at the last line and executing it worked. I had to install pip for Python 3 and update it to get the old blog engine working.