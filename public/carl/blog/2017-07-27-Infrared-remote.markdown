---
layout: post
title:  "Infrared remote"
date: 2017-07-27T19:39:29-07:00  
categories: arduino
---
Lately I've been watching a lot of Ben Heck on Youtube. Also I got a new soldering iron for my birthday. Both of these have contributed to my desire to do more electronic projects. I lose television remotes more often that I care to admit, but soon I hope to remove that problem entirely. I've order a few parts and plan on building an IR capture device to store the codes from my existing remotes. Then I'm going to attach an IR emitter to my computer so I can control things from it.
[Wikipedia's Article on infrared communication](https://en.wikipedia.org/wiki/Infrared#Communications)

[https://blog.arduino.cc/2010/02/11/arduino-all-stars-the-coplete-ir-library-for-arduino/](Blog post about IR Arduino Library)

Probably going to use my Arduino Micro Pro to drive things and control it with PySerial from my laptop.
I'm thinking about building a container which will hang off the laptop opposite the webcam.
I hope to post more later or perhaps start adding embeded YouTube videos of my projects.

Carl