---
layout: post
title:  "Where did all the space go?"
date: 2018-12-27T17:05:06-06:00  
categories: windows,windirstat,du,md5,gaming,ark,kkrieger
---

A long time friend and I used to play ARK: Survival Evolved a lot, and I hadn't played in a while. The game wasn't installed so I downloaded it over the break, installed some mods and then later noticed my hard drive on my laptop was getting full. I wanted to find out exactly where the space went.

On Linux "du" would do the job. There are lots of graphical render tools for du that will show it in nice boxes.

Windows has something like that too, except you have to download it from a third party and like all mystery downloads you need to check the file checksum. 


If you haven't done this before, here is a link to Microsoft's tool:

[https://support.microsoft.com/en-us/help/841290/availability-and-description-of-the-file-checksum-integrity-verifier-u](https://support.microsoft.com/en-us/help/841290/availability-and-description-of-the-file-checksum-integrity-verifier-u)

But they also have another tool baked in now:

CertUtil

You can find a common use example here: 
[https://security.stackexchange.com/questions/189000/how-to-verify-the-checksum-of-a-downloaded-file-pgp-sha-etc](https://security.stackexchange.com/questions/189000/how-to-verify-the-checksum-of-a-downloaded-file-pgp-sha-etc)

Hashing is a nice first step but... Signatures are better:
[https://security.stackexchange.com/questions/31836/why-we-use-gpg-signatures-for-file-verification-instead-of-hash-values](https://security.stackexchange.com/questions/31836/why-we-use-gpg-signatures-for-file-verification-instead-of-hash-values)

After that brief diversion we remember we are looking at checking file sizes. Assuming the hash matches installing the software gives an outdated looking UI, and the program didn't cost so I'm not going to complain too much.

Ark with the server,mods, and maps takes up 235 GB. Why?!? The game looks "okay" if you look at the screen shots on ultra you can take a nice one, but there are only two character models. A male and a female. Digging further in 60GB of maps. Scorched Earth's map _only_ takes 7.8 GB and it seems to be the smallest in file size. The server I'm connecting to has several maps linked by "tribute terminals" which let you change maps. Roughly 10 maps at about 15 GB average that makes up the space. I wondered why the file format takes up so much space. ARK uses the unreal 4 engine and there specification is here:

[https://docs.unrealengine.com/en-us/Engine/Landscape/TechnicalGuide](https://docs.unrealengine.com/en-us/Engine/Landscape/TechnicalGuide)

It looks like they are saving vertices.  This where the space is going. The engine probably needs them in that format, and if our network pipe and drive space can support it, then it isn't a big deal I guess. I was just thinking squeezing the space might be and interesting challenge.

One of my favorite past times is watching YouTube videos. One of them is about a game called Micro Mages. It fits an entire multiplayer game in 40 kilobytes. The game is two dimensional and is custom built rather than using an off the shelf engine. It shows the gambit of optimization.

When I discussed this with a longtime friend he reminded me of another FPS which was released in 2004 and takes only 96kb:

_.kkrieger_

[Watch the game play](//commons.wikimedia.org/wiki/File:.kkrieger_gameplay.webm?embedplayer=yes)

Do better Ark.