---
layout: post
title:  "flashcards"
date: 2017-05-30T19:59:36-07:00  
categories: programming,study,python
---
Sometimes one hobby makes another one more accessible, and sometimes it provides a distraction. In this case I used my WebDevelopmentSkeleton to build a flash card emulator.

Introducing a new repo:

[Study Flash Cards](https://gitlab.com/crb02005/study-flash-cards) _moved to gitlab 2020_
I've used this to improve my knowledge of music by first running a little Python to get a list of music notes and formating it to a JSON file that my new Flash Card program uses:

```
intervals = [0,2,4,5,7,9]

keys = ["A","A#","B","C","C#","D","D#","E","F","F#","G","G#"]*2
roman = ["I","II","III","IV","V","VI"]
minors = ["","m","m","","","m"]

for offset in range(0,12):
   for index in range(5,6):
       print("{\n\"frontText\":\"What is the key for %s%s as the  %s?\",\n\"backText\":\"%s\"\n}," % (keys[offset+intervals[index]],minors[index],roman[index],keys[offset+intervals[0]]))

```

running the above code prints out a partial JSON file.

I've limited the questions just to the 6th which gives you the minor key to solo in. I can change the range to show them all but I've found studying a smaller pool to be more effective.


I'll be able to use this for all my study activities.
